﻿using Auritex.Model;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace ClienteAuritex.Windows
{
    /// <summary>
    /// Lógica de interacción para VentanaTicket.xaml
    /// </summary>
    public partial class VentanaTicket : Window
    {

        private Venta venta;

        public VentanaTicket(VentanaAreaPersonal vap, VentanaCarrito vc, Venta venta)
        {
            InitializeComponent();

            if (vap != null)
            {
                Owner = vap;
            }
            else
            {
                Owner = vc;
            }

            this.venta = venta;
            panelVenta.DataContext = venta;
        }

        private void descargarFacturaVenta_Click(object sender, RoutedEventArgs e)
        {
            string path = SeleccionarPath("Factura_Pedido" + venta.VentaId + ".pdf");
            if (path != null)
            {
                GenerateInvoice(path);

                Process.Start(path);
            }
        }

        private string SeleccionarPath(string nombre)
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer);
            fileDialog.FileName = nombre;
            if (fileDialog.ShowDialog() == true)
            {
                return fileDialog.FileName;
            }
            else
            {
                return null;
            }
        }

        private void GenerateInvoice(string path)
        {
            try
            {
                Document pdfDoc = new Document(PageSize.A4, 30, 30, 30, 30);
                MemoryStream PDFData = new MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream(path, FileMode.Create));

                var titleFont = FontFactory.GetFont("Arial", 14, Font.BOLD);
                var titleMain = FontFactory.GetFont("Arial", 24, Font.BOLD);
                var titleFontBlue = FontFactory.GetFont("Arial", 12, Font.BOLD, BaseColor.RED);
                var boldTableFont = FontFactory.GetFont("Arial", 9, Font.BOLD);
                var bodyFont = FontFactory.GetFont("Arial", 8, Font.NORMAL);
                var EmailFont = FontFactory.GetFont("Arial", 8, Font.NORMAL, BaseColor.BLUE);
                BaseColor TabelHeaderBackGroundColor = BaseColor.LIGHT_GRAY;

                Rectangle pageSize = writer.PageSize;
                pdfDoc.Open();

                #region CABECERA PDF

                PdfPTable headertable = new PdfPTable(3);
                headertable.HorizontalAlignment = 0;
                headertable.WidthPercentage = 100;
                headertable.SetWidths(new float[] { 100f, 320f, 100f });
                headertable.DefaultCell.Border = Rectangle.NO_BORDER;

                {
                    PdfPCell pdfCelllogo = new PdfPCell(new Phrase("Auritex", titleMain));
                    pdfCelllogo.Border = Rectangle.NO_BORDER;
                    pdfCelllogo.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                    pdfCelllogo.BorderWidthBottom = 1f;
                    headertable.AddCell(pdfCelllogo);
                }

                {
                    PdfPCell middlecell = new PdfPCell();
                    middlecell.Border = Rectangle.NO_BORDER;
                    middlecell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                    middlecell.BorderWidthBottom = 1f;
                    headertable.AddCell(middlecell);
                }

                {
                    PdfPTable nested = new PdfPTable(1);
                    nested.SpacingAfter = 15;
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;

                    PdfPCell nextPostCell1 = new PdfPCell(new Phrase("Auritex", titleFont));
                    nextPostCell1.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell1);

                    PdfPCell nextPostCell2 = new PdfPCell(new Phrase("Ourense, 32002", bodyFont));
                    nextPostCell2.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell2);

                    PdfPCell nextPostCell3 = new PdfPCell(new Phrase("988 21 22 23", bodyFont));
                    nextPostCell3.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell3);

                    PdfPCell nextPostCell4 = new PdfPCell(new Phrase("auritex@gmail.com", EmailFont));
                    nextPostCell4.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell4);

                    PdfPCell nesthousing = new PdfPCell(nested);
                    nesthousing.Border = Rectangle.NO_BORDER;
                    nesthousing.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                    nesthousing.BorderWidthBottom = 1f;
                    nesthousing.Rowspan = 5;
                    nesthousing.PaddingBottom = 10f;
                    headertable.AddCell(nesthousing);
                }


                PdfPTable Invoicetable = new PdfPTable(3);
                Invoicetable.SpacingBefore = 20;
                Invoicetable.HorizontalAlignment = 0;
                Invoicetable.WidthPercentage = 100;
                Invoicetable.SetWidths(new float[] { 100f, 320f, 125f });
                Invoicetable.DefaultCell.Border = Rectangle.NO_BORDER;

                {
                    PdfPTable nested = new PdfPTable(1);
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;

                    PdfPCell nextPostCell1 = new PdfPCell(new Phrase("Factura para:", titleFont));
                    nextPostCell1.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell1);

                    nested.AddCell("\n");

                    PdfPCell nextPostCell2 = new PdfPCell(new Phrase(venta.cliente.nombre + " " + venta.cliente.apellidos, bodyFont));
                    nextPostCell2.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell2);

                    PdfPCell nextPostCell3 = new PdfPCell(new Phrase(venta.direccionEnvio + ", " + venta.ciudadEnvio + ", " + venta.codPostalEnvio, bodyFont));
                    nextPostCell3.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell3);

                    PdfPCell nesthousing = new PdfPCell(nested);
                    nesthousing.Border = Rectangle.NO_BORDER;
                    nesthousing.Rowspan = 5;
                    nesthousing.PaddingBottom = 10f;
                    Invoicetable.AddCell(nesthousing);
                }

                {
                    PdfPCell middlecell = new PdfPCell();
                    middlecell.Border = Rectangle.NO_BORDER;
                    Invoicetable.AddCell(middlecell);
                }

                {
                    PdfPTable nested = new PdfPTable(1);
                    nested.DefaultCell.Border = Rectangle.NO_BORDER;

                    PdfPCell nextPostCell1 = new PdfPCell(new Phrase("PEDIDO Nº" + venta.VentaId, titleFontBlue));
                    nextPostCell1.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell1);

                    nested.AddCell("\n");

                    PdfPCell nextPostCell2 = new PdfPCell(new Phrase("Fecha del pedido: " + venta.fecha.ToShortDateString(), bodyFont));
                    nextPostCell2.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell2);

                    PdfPCell nextPostCell3 = new PdfPCell(new Phrase("Plazo devolución: " + venta.fecha.AddDays(30).ToShortDateString(), bodyFont));
                    nextPostCell3.Border = Rectangle.NO_BORDER;
                    nested.AddCell(nextPostCell3);

                    PdfPCell nesthousing = new PdfPCell(nested);
                    nesthousing.Border = Rectangle.NO_BORDER;
                    nesthousing.Rowspan = 5;
                    nesthousing.PaddingBottom = 10f;
                    Invoicetable.AddCell(nesthousing);
                }


                pdfDoc.Add(headertable);
                Invoicetable.PaddingTop = 10f;

                pdfDoc.Add(Invoicetable);

                #endregion

                #region CUERPO PDF

                PdfPTable itemTable = new PdfPTable(6);
                itemTable.SpacingBefore = 25;
                itemTable.HorizontalAlignment = 0;
                itemTable.WidthPercentage = 100;
                itemTable.SetWidths(new float[] { 5, 35, 10, 15, 15, 20 });
                itemTable.SpacingAfter = 40;
                itemTable.DefaultCell.Border = Rectangle.BOX;

                PdfPCell cell1 = new PdfPCell(new Phrase("Nº", boldTableFont));
                cell1.BackgroundColor = TabelHeaderBackGroundColor;
                cell1.HorizontalAlignment = Element.ALIGN_CENTER;
                itemTable.AddCell(cell1);

                PdfPCell cell2 = new PdfPCell(new Phrase("ARTÍCULO", boldTableFont));
                cell2.BackgroundColor = TabelHeaderBackGroundColor;
                cell2.HorizontalAlignment = Element.ALIGN_CENTER;
                itemTable.AddCell(cell2);

                PdfPCell cell3 = new PdfPCell(new Phrase("TALLA", boldTableFont));
                cell3.BackgroundColor = TabelHeaderBackGroundColor;
                cell3.HorizontalAlignment = Element.ALIGN_CENTER;
                itemTable.AddCell(cell3);

                PdfPCell cell4 = new PdfPCell(new Phrase("SEXO", boldTableFont));
                cell4.BackgroundColor = TabelHeaderBackGroundColor;
                cell4.HorizontalAlignment = Element.ALIGN_CENTER;
                itemTable.AddCell(cell4);

                PdfPCell cell5 = new PdfPCell(new Phrase("CANTIDAD", boldTableFont));
                cell5.BackgroundColor = TabelHeaderBackGroundColor;
                cell5.HorizontalAlignment = Element.ALIGN_CENTER;
                itemTable.AddCell(cell5);

                PdfPCell cell6 = new PdfPCell(new Phrase("PRECIO UD.", boldTableFont));
                cell6.BackgroundColor = TabelHeaderBackGroundColor;
                cell6.HorizontalAlignment = Element.ALIGN_CENTER;
                itemTable.AddCell(cell6);

                foreach (DetalleVenta dv in venta.detallesVenta)
                {
                    PdfPCell numberCell = new PdfPCell(new Phrase(dv.DetalleVentaId.ToString(), bodyFont));
                    numberCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    numberCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                    itemTable.AddCell(numberCell);

                    PdfPCell nameCell;

                    if (dv.estado.Equals("VENDIDO"))
                    {
                        nameCell = new PdfPCell(new Phrase(dv.detalleArticulo.articulo.nombre, bodyFont));
                    }
                    else
                    {
                        nameCell = new PdfPCell(new Phrase(dv.detalleArticulo.articulo.nombre + " (DEVUELTO)", bodyFont));
                    }
                    nameCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    nameCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                    itemTable.AddCell(nameCell);

                    PdfPCell tallCell = new PdfPCell(new Phrase(dv.detalleArticulo.talla, bodyFont));
                    tallCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    tallCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                    itemTable.AddCell(tallCell);

                    PdfPCell sexCell = new PdfPCell(new Phrase(dv.detalleArticulo.articulo.sexo, bodyFont));
                    sexCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    sexCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                    itemTable.AddCell(sexCell);

                    PdfPCell quantityCell = new PdfPCell(new Phrase("1 ud.", bodyFont));
                    quantityCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    quantityCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                    itemTable.AddCell(quantityCell);

                    PdfPCell unitPVPCell = new PdfPCell(new Phrase(dv.precio + "€", bodyFont));
                    unitPVPCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    unitPVPCell.PaddingRight = 10;
                    unitPVPCell.Border = Rectangle.LEFT_BORDER | Rectangle.RIGHT_BORDER | Rectangle.BOTTOM_BORDER;
                    itemTable.AddCell(unitPVPCell);
                }

                PdfPCell totalAmtStrCell = new PdfPCell(new Phrase("TOTAL COMPRA", boldTableFont));
                totalAmtStrCell.Colspan = 5;
                totalAmtStrCell.Border = Rectangle.RECTANGLE;
                totalAmtStrCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                totalAmtStrCell.PaddingRight = 10;
                itemTable.AddCell(totalAmtStrCell);

                PdfPCell totalAmtCell = new PdfPCell(new Phrase(venta.totalPrecio + "€", boldTableFont));
                totalAmtCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                totalAmtCell.PaddingRight = 10;
                itemTable.AddCell(totalAmtCell);

                PdfPCell cell = new PdfPCell(new Phrase("*** AVISO: Tienes hasta 30 días desde la fecha de compra para devolver cualquier artículo ***", bodyFont));
                cell.Colspan = 6;
                cell.HorizontalAlignment = Element.ALIGN_CENTER;
                itemTable.AddCell(cell);
                pdfDoc.Add(itemTable);

                #endregion

                #region PIE PDF

                PdfContentByte cb = new PdfContentByte(writer);

                BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, true);
                cb = new PdfContentByte(writer);
                cb = writer.DirectContent;
                cb.BeginText();
                cb.SetFontAndSize(bf, 8);
                cb.SetTextMatrix(pageSize.GetLeft(220), 20);
                cb.ShowText("Factura creada por Auritex | © Copyright 2018");
                cb.EndText();

                cb.MoveTo(40, pdfDoc.PageSize.GetBottom(50));
                cb.LineTo(pdfDoc.PageSize.Width - 40, pdfDoc.PageSize.GetBottom(50));
                cb.Stroke();

                #endregion

                pdfDoc.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show("No se pudo generar la factura en PDF", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                Debug.WriteLine(String.Format("Error al generar la factura PDF - {0}", ex.Message));
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
        }

    }
}
