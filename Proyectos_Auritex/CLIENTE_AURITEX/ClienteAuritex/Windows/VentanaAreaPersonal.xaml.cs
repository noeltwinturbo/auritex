﻿using Auritex.Model;
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace ClienteAuritex.Windows
{
    /// <summary>
    /// Lógica de interacción para VentanaAreaPersonal.xaml
    /// </summary>
    public partial class VentanaAreaPersonal : Window
    {

        public MainWindow mw { get; }
        private int index = 0;

        public VentanaAreaPersonal(MainWindow mw)
        {
            InitializeComponent();

            Owner = mw;
            this.mw = mw;

            cbCiudadCliente.ItemsSource = mw.uof.GeografiaRepository.GetGeografia("PROVINCIA");
            cbCampoBusquedaHistorialPedidos.Text = "Número pedido";

            panelDatosPersonalesCliente.DataContext = mw.cliente;
            lbNombreUsuario.Content = mw.cliente.nombre;
        }

        #region PANEL BOTONES LATERALES

        private void btDatosPersonalesCliente_Click(object sender, RoutedEventArgs e)
        {
            if (index != 0)
            {
                OcultarVisibilidadPaneles();
                btDatosPersonalesCliente.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#606060"));
                btDatosPersonalesCliente.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#333333"));

                panelDatosPersonalesCliente.DataContext = mw.cliente;

                panelDatosPersonalesCliente.Visibility = Visibility.Visible;
                index = 0;
            }
        }

        private void btDatosDireccionEnvioCliente_Click(object sender, RoutedEventArgs e)
        {
            if (index != 1)
            {
                OcultarVisibilidadPaneles();
                btDatosDireccionEnvioCliente.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#606060"));
                btDatosDireccionEnvioCliente.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#333333"));

                panelDatosDireccionCliente.DataContext = mw.cliente;

                panelDatosDireccionCliente.Visibility = Visibility.Visible;
                index = 1;
            }
        }

        private void btDatosHistorialPedidosCliente_Click(object sender, RoutedEventArgs e)
        {
            if (index != 2)
            {
                OcultarVisibilidadPaneles();
                btDatosHistorialPedidosCliente.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#606060"));
                btDatosHistorialPedidosCliente.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#333333"));

                dgHistorialPedidos.ItemsSource = mw.cliente.ventas;
                dgHistorialDetallesPedido.ItemsSource = null;

                panelDatosPedidosCliente.Visibility = Visibility.Visible;
                index = 2;
            }
        }

        private void btDatosAccesoCliente_Click(object sender, RoutedEventArgs e)
        {
            if (index != 3)
            {
                OcultarVisibilidadPaneles();
                btDatosAccesoCliente.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#606060"));
                btDatosAccesoCliente.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#333333"));

                panelDatosAccesoCliente.Visibility = Visibility.Visible;
                index = 3;
            }
        }

        private void OcultarVisibilidadPaneles()
        {
            switch (index)
            {
                case 0:
                    panelDatosPersonalesCliente.DataContext = null;
                    btDatosPersonalesCliente.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#333333"));
                    btDatosPersonalesCliente.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#606060"));
                    panelDatosPersonalesCliente.Visibility = Visibility.Collapsed;
                    break;
                case 1:
                    panelDatosDireccionCliente.DataContext = null;
                    btDatosDireccionEnvioCliente.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#333333"));
                    btDatosDireccionEnvioCliente.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#606060"));
                    panelDatosDireccionCliente.Visibility = Visibility.Collapsed;
                    break;
                case 2:
                    btDatosHistorialPedidosCliente.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#333333"));
                    btDatosHistorialPedidosCliente.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#606060"));
                    panelDatosPedidosCliente.Visibility = Visibility.Collapsed;
                    break;
                case 3:
                    LimpiarPanelDatosAccesoCliente();
                    btDatosAccesoCliente.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom("#333333"));
                    btDatosAccesoCliente.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#606060"));
                    panelDatosAccesoCliente.Visibility = Visibility.Collapsed;
                    break;
            }

            if (index != 2)
            {
                mw.cliente = mw.uof.ClienteRepository.MultiGet(c => c.ClienteId == mw.cliente.ClienteId).FirstOrDefault();
            }
        }

        #endregion

        #region PANEL DATOS PERSONALES

        private void btCancelarDatosPersonales_Click(object sender, RoutedEventArgs e)
        {
            panelDatosPersonalesCliente.DataContext = null;
            panelDatosPersonalesCliente.DataContext = mw.cliente;
        }

        private void btModificarDatosPersonales_Click(object sender, RoutedEventArgs e)
        {
            ModificarDatosPersonales();
        }

        private void ModificarDatosPersonales()
        {
            mw.cliente.nombre = tbNombreCliente.Text;
            mw.cliente.apellidos = tbApellidosCliente.Text;
            mw.cliente.dni = tbDniCliente.Text;
            mw.cliente.telefono = tbTelefonoCliente.Text;
            mw.cliente.fechaNacimiento = Convert.ToDateTime(dpFechaNacCliente.Text);
            if (mw.Validar(mw.cliente))
            {
                mw.uof.ClienteRepository.Modify(mw.cliente);
                MessageBox.Show("Datos personales modificados", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void panelDatosPersonalesCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ModificarDatosPersonales();
            }
        }

        #endregion

        #region PANEL DATOS DIRECCION

        private void btCancelarDatosDireccion_Click(object sender, RoutedEventArgs e)
        {
            panelDatosDireccionCliente.DataContext = null;
            panelDatosDireccionCliente.DataContext = mw.cliente;
        }

        private void btModificarDatosDireccion_Click(object sender, RoutedEventArgs e)
        {
            ModificarDatosDireccion();
        }

        private void ModificarDatosDireccion()
        {
            mw.cliente.ciudad = cbCiudadCliente.Text;
            mw.cliente.codPostal = tbCodPostalCliente.Text;
            mw.cliente.direccion = tbDireccionCliente.Text;
            if (mw.Validar(mw.cliente))
            {
                mw.uof.ClienteRepository.Modify(mw.cliente);
                MessageBox.Show("Datos de la dirección de envío modificados", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void panelDatosDireccionCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ModificarDatosDireccion();
            }
        }

        #endregion

        #region DATOS HISTORIAL PEDIDOS

        private void cbCampoBusquedaHistorialPedidos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCampoBusquedaHistorialPedidos.SelectedItem.Equals("Fecha pedido"))
            {
                tbCampoBusquedaHistorialPedidos.Visibility = Visibility.Collapsed;
                dpBusquedaPedido.Visibility = Visibility.Visible;
            }
            else
            {
                dpBusquedaPedido.Visibility = Visibility.Collapsed;
                tbCampoBusquedaHistorialPedidos.Visibility = Visibility.Visible;
            }
        }

        private void tbCampoBusquedaHistorialPedidos_TextChanged(object sender, TextChangedEventArgs e)
        {
            dgHistorialPedidos.SelectedItem = null;
            dgHistorialDetallesPedido.ItemsSource = null;

            if (!String.IsNullOrEmpty(tbCampoBusquedaHistorialPedidos.Text))
            {
                int idVenta = Convert.ToInt32(tbCampoBusquedaHistorialPedidos.Text);
                dgHistorialPedidos.ItemsSource = mw.uof.VentaRepository.MultiGet(c => c.ClienteId == mw.cliente.ClienteId && c.VentaId == idVenta);
            }
            else
            {
                dgHistorialPedidos.ItemsSource = mw.cliente.ventas;
            }
        }

        private void dpBusquedaPedido_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            dgHistorialPedidos.SelectedItem = null;
            dgHistorialDetallesPedido.ItemsSource = null;

            if (dpBusquedaPedido.SelectedDate != null)
            {
                DateTime fecha = Convert.ToDateTime(dpBusquedaPedido.SelectedDate);
                dgHistorialPedidos.ItemsSource = mw.uof.VentaRepository.MultiGet(c => c.ClienteId == mw.cliente.ClienteId && c.fecha.Day == fecha.Day && c.fecha.Month == fecha.Month && c.fecha.Year == fecha.Year);
            }
            else
            {
                dgHistorialPedidos.ItemsSource = mw.cliente.ventas;
            }
        }

        private void btVerTodosPedidos_Click(object sender, RoutedEventArgs e)
        {
            tbCampoBusquedaHistorialPedidos.Clear();
            dgHistorialPedidos.SelectedItem = null;
            dgHistorialDetallesPedido.ItemsSource = null;
            dgHistorialPedidos.ItemsSource = mw.cliente.ventas;
        }

        private void dgHistorialPedidos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgHistorialPedidos.SelectedIndex >= 0)
            {
                dgHistorialDetallesPedido.ItemsSource = ((Venta)dgHistorialPedidos.SelectedItem).detallesVenta;
            }
        }

        private void dgHistorialPedidos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgHistorialPedidos.SelectedIndex >= 0)
            {
                VentanaTicket vt = new VentanaTicket(this, null, (Venta)dgHistorialPedidos.SelectedItem);
                vt.ShowDialog();
            }
        }

        private void dgHistorialDetallesPedido_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgHistorialDetallesPedido.SelectedIndex >= 0)
            {
                VentanaDetalleArticuloInfo vdai = new VentanaDetalleArticuloInfo((DetalleVenta)dgHistorialDetallesPedido.SelectedItem, this);
                vdai.ShowDialog();
            }
        }

        #endregion

        #region PANEL DATOS ACCESO

        private void btCancelarDatosAcceso_Click(object sender, RoutedEventArgs e)
        {
            LimpiarPanelDatosAccesoCliente();
        }

        private void btModificarDatosAcceso_Click(object sender, RoutedEventArgs e)
        {
            ModificarDatosAcceso();
        }

        private void tEliminarCuentaCliente(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show(String.Format("{0}, ¿estás seguro de que quieres eliminar tu cuenta de Auritex? Recuerda que este cambio no puede ser revertido.", mw.cliente.nombre), "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                mw.cliente.estado = MainWindow.DESHABILITADO;
                mw.uof.ClienteRepository.Modify(mw.cliente);

                mw.SendEmail(mw.cliente.email, String.Format(MainWindow.HTML_CLIENTE_ELIMINADO, mw.cliente.nombre));
                mw.SendEmail(MainWindow.EMAIL_AURITEX, String.Format(MainWindow.HTML_CLIENTE_ELIMINADO_MAESTRO, mw.cliente.ClienteId));

                mw.cliente = null;
                Close();

                MessageBox.Show("Cuenta de usuario eliminada", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void tbEmailClienteNuevo_TextChanged(object sender, TextChangedEventArgs e)
        {
            bool enabled = String.IsNullOrEmpty(tbEmailClienteNuevo.Text);

            pbContraseñaClienteVieja.IsEnabled = enabled;
            pbContraseñaClienteNueva.IsEnabled = enabled;
            pbContraseñaClienteNueva2.IsEnabled = enabled;
        }

        private void pbContraseñaClienteVieja_PasswordChanged(object sender, RoutedEventArgs e)
        {
            bool enabled = String.IsNullOrEmpty(pbContraseñaClienteVieja.Password);

            tbEmailClienteNuevo.IsEnabled = enabled;
            tbEmailClienteNuevo2.IsEnabled = enabled;
            pbContraseñaEmailCliente.IsEnabled = enabled;
        }

        private void ModificarDatosAcceso()
        {
            var dialogResult = MessageBox.Show(String.Format((tbEmailClienteNuevo.IsEnabled) ? "{0}, ¿estás seguro de que quieres modificar el email de tu cuenta?" : "{0}, ¿estás seguro de que quieres modificar la contraseña de tu cuenta?", mw.cliente.nombre), "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                mw.ActualizarContexto();

                if (tbEmailClienteNuevo.IsEnabled)
                {
                    if (!String.IsNullOrEmpty(tbEmailClienteNuevo.Text) 
                        && !String.IsNullOrEmpty(tbEmailClienteNuevo2.Text) 
                        && !String.IsNullOrEmpty(pbContraseñaEmailCliente.Password))
                    {
                        if (tbEmailClienteNuevo.Text.Equals(tbEmailClienteNuevo2.Text) && pbContraseñaEmailCliente.Password.Equals(mw.cliente.contraseña))
                        {
                            if (mw.uof.ClienteRepository.MultiGet(c => c.email.Equals(tbEmailClienteNuevo.Text) && c.estado.Equals(MainWindow.HABILITADO)).Count() == 0)
                            {
                                mw.cliente.email = tbEmailClienteNuevo.Text;
                                if (mw.Validar(mw.cliente))
                                {
                                    mw.uof.ClienteRepository.Modify(mw.cliente);

                                    mw.SendEmail(mw.cliente.email, String.Format(MainWindow.HTML_ACCESO_MODIFICADO, mw.cliente.nombre));

                                    LimpiarPanelDatosAccesoCliente();

                                    MessageBox.Show("Email modificado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Ya existe un usuario registrado con este email", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Alguno de los datos introducidos no son correctos", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Faltan campos por rellenar", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(pbContraseñaClienteNueva.Password) 
                        && !String.IsNullOrEmpty(pbContraseñaClienteNueva2.Password) 
                        && !String.IsNullOrEmpty(pbContraseñaClienteVieja.Password))
                    {
                        if (pbContraseñaClienteNueva.Password.Equals(pbContraseñaClienteNueva2.Password) 
                            && pbContraseñaClienteVieja.Password.Equals(mw.cliente.contraseña))
                        {
                            mw.cliente.contraseña = pbContraseñaClienteNueva.Password;
                            if (mw.Validar(mw.cliente))
                            {
                                mw.uof.ClienteRepository.Modify(mw.cliente);

                                mw.SendEmail(mw.cliente.email, String.Format(MainWindow.HTML_ACCESO_MODIFICADO, mw.cliente.nombre));

                                LimpiarPanelDatosAccesoCliente();

                                MessageBox.Show("Contraseña modificada", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Alguno de los datos introducidos no son correctos", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Faltan campos por rellenar", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
        }

        private void LimpiarPanelDatosAccesoCliente()
        {
            tbEmailClienteNuevo.Clear();
            tbEmailClienteNuevo2.Clear();
            pbContraseñaEmailCliente.Clear();
            pbContraseñaClienteNueva.Clear();
            pbContraseñaClienteNueva2.Clear();
            pbContraseñaClienteVieja.Clear();
        }

        private void panelDatosAccesoCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ModificarDatosAcceso();
            }
        }

        #endregion

        #region METODOS VENTANA

        private void tb_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int ascci = Convert.ToInt32(Convert.ToChar(e.Text));
            if (ascci >= 48 && ascci <= 57)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            try
            {
                base.OnMouseLeftButtonDown(e);
                DragMove();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error moviendo la ventana - {0}", ex.Message);
            }
        }

        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btCerrarSesion_Click(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show(String.Format("{0}, ¿estás seguro de que quieres cerrar sesión?", mw.cliente.nombre), "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                mw.cliente = null;
                Close();
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
        }

        #endregion

    }
}
