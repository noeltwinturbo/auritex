﻿using Auritex.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ClienteAuritex.Windows
{
    /// <summary>
    /// Lógica de interacción para VentanaCarrito.xaml
    /// </summary>
    public partial class VentanaCarrito : Window
    {

        public MainWindow mw { get; }
        private Venta venta;

        public VentanaCarrito(MainWindow mw, bool source)
        {
            InitializeComponent();

            Owner = mw;
            this.mw = mw;

            cbCiudadEnvio.ItemsSource = mw.uof.GeografiaRepository.GetGeografia("PROVINCIA");
            dgDetallesVenta.ItemsSource = mw.detallesVenta;
            CalcularTotalVenta();

            if (source)
            {
                title.Content = "Auritex - Túnel de compra";
                panelDatosEnvio.DataContext = mw.cliente;
                panelCarrito.Visibility = Visibility.Collapsed;
                panelTunelCompra.Visibility = Visibility.Visible;
            }
        }

        #region PANEL CARRITO

        private void dgDetallesVenta_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgDetallesVenta.SelectedIndex >= 0)
            {
                imagenArticulo.Source = mw.ConvertArrayByteToImage(((DetalleVenta)dgDetallesVenta.SelectedItem).detalleArticulo.articulo.imagen);
            }
        }

        private void btQuitarDetalleArticuloCesta_Click(object sender, RoutedEventArgs e)
        {
            mw.detallesVenta.Remove((DetalleVenta)dgDetallesVenta.SelectedItem);
            imagenArticulo.Source = null;

            CalcularTotalVenta();

            if (mw.detallesVenta.Count == 0)
            {
                mw.detallesVenta = new HashSet<DetalleVenta>();
                mw.idDetalleVenta = 1;
            }
        }

        private void btVaciarCarrito_Click(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show("¿Estás seguro de que quieres vaciar el carrito", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                mw.detallesVenta = new HashSet<DetalleVenta>();
                mw.idDetalleVenta = 1;

                Close();
            }
        }

        private void btContinuarComprando_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btFinalizarCompra_Click(object sender, RoutedEventArgs e)
        {
            if (mw.detallesVenta.Count > 0)
            {
                mw.ActualizarContexto();

                if (mw.cliente == null)
                {
                    VentanaLoginRegistro vlr = new VentanaLoginRegistro(null, this, false);
                    vlr.ShowDialog();
                }
                else
                {
                    title.Content = "Auritex - Túnel de compra";
                    panelDatosEnvio.DataContext = mw.cliente;
                    panelCarrito.Visibility = Visibility.Collapsed;
                    panelTunelCompra.Visibility = Visibility.Visible;
                }
            }
            else
            {
                MessageBox.Show("Tu carrito está vacío", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void CalcularTotalVenta()
        {
            float total = 0f;

            dgDetallesVenta.Items.Refresh();

            foreach (DetalleVenta dv in mw.detallesVenta)
            {
                total += dv.detalleArticulo.articulo.precioVenta;
            }

            lbTotalVenta.Content = total;
            lbTotalVentaTunelCompra.Content = total;
        }

        #endregion

        #region PANEL TUNEL COMPRA

        private void btModificarDireccionEnvio_Click(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show(String.Format("{0}, ¿deseas actualizar tu direccón de envío con estos nuevos datos?", mw.cliente.nombre), "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                mw.cliente.ciudad = cbCiudadEnvio.Text;
                mw.cliente.codPostal = tbCodPostalEnvio.Text;
                mw.cliente.direccion = tbDireccionEnvio.Text;
                if (mw.Validar(mw.cliente))
                {
                    mw.uof.ClienteRepository.Modify(mw.cliente);

                    MessageBox.Show("Datos de la dirección de envío actualizados", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        private void btTarjetaCredito_Click(object sender, RoutedEventArgs e)
        {
            panelPaypal.Visibility = Visibility.Collapsed;
            panelTarjetaCredito.Visibility = Visibility.Visible;
        }

        private void btPaypal_Click(object sender, RoutedEventArgs e)
        {
            panelTarjetaCredito.Visibility = Visibility.Collapsed;
            panelPaypal.Visibility = Visibility.Visible;
        }

        private void btAtras_Click(object sender, RoutedEventArgs e)
        {
            title.Content = "Auritex - Carrito";
            panelTunelCompra.Visibility = Visibility.Collapsed;
            panelCarrito.Visibility = Visibility;
        }

        private void btPagar_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(cbCiudadEnvio.Text) 
                && !String.IsNullOrEmpty(tbCodPostalEnvio.Text) 
                && !String.IsNullOrEmpty(tbDireccionEnvio.Text) 
                && ((panelTarjetaCredito.IsVisible 
                    && !String.IsNullOrEmpty(tbNombreTarjetaCredito.Text) 
                    && !String.IsNullOrEmpty(tbNumeroTarjeta1.Text) 
                    && !String.IsNullOrEmpty(tbNumeroTarjeta2.Text) 
                    && !String.IsNullOrEmpty(tbNumeroTarjeta3.Text) 
                    && !String.IsNullOrEmpty(tbNumeroTarjeta4.Text) 
                    && !String.IsNullOrEmpty(cbMesTarjetaCredito.Text) 
                    && !String.IsNullOrEmpty(cbAnoTarjetaCredito.Text) 
                    && !String.IsNullOrEmpty(tbCodigoCCV.Text)) 
                || (panelPaypal.IsVisible && !String.IsNullOrEmpty(tbCorreoPaypal.Text) 
                    && !String.IsNullOrEmpty(pbContrasenaPaypal.Password))))
            {
                if ((panelTarjetaCredito.IsVisible && tbNombreTarjetaCredito.Text.Length >= 3 
                        && Regex.IsMatch(tbNumeroTarjeta1.Text, "[0-9]{4}") 
                        && Regex.IsMatch(tbNumeroTarjeta2.Text, "[0-9]{4}") 
                        && Regex.IsMatch(tbNumeroTarjeta3.Text, "[0-9]{4}") 
                        && Regex.IsMatch(tbNumeroTarjeta4.Text, "[0-9]{4}") 
                        && Regex.IsMatch(tbCodigoCCV.Text, "[0-9]{3}")) 
                    || (panelPaypal.IsVisible && tbCorreoPaypal.Text.Length >= 3 
                        && pbContrasenaPaypal.Password.Length >= 3))
                {
                    mw.ActualizarContexto();

                    venta = new Venta(cbCiudadEnvio.Text, tbCodPostalEnvio.Text, tbDireccionEnvio.Text, (panelTarjetaCredito.IsVisible) ? "TARJETA DE CRÉDITO" : "PAYPAL", mw.cliente.ClienteId, mw.detallesVenta);
                    if (mw.Validar(venta))
                    {
                        var dialogResult = MessageBox.Show(String.Format("{0}, ¿deseas finalizar la compra y pagar", mw.cliente.nombre), "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                        if (dialogResult == MessageBoxResult.Yes)
                        {
                            if (mw.CheckStockDetalleVentaDetalleArticulo())
                            {
                                foreach (DetalleVenta dv in mw.detallesVenta)
                                {
                                    dv.venta = venta;
                                }

                                mw.uof.VentaRepository.AddVenta(venta);

                                Autoreabastecimiento();

                                mw.SendEmail(mw.cliente.email, String.Format(MainWindow.HTML_PEDIDO_REALIZADO, venta.VentaId, mw.cliente.nombre, venta.totalUnidades, venta.totalPrecio));
                                mw.SendEmail(MainWindow.EMAIL_AURITEX, String.Format(MainWindow.HTML_VENTA_REALIZADA_MAESTRO, venta.VentaId, mw.cliente.ClienteId, venta.totalUnidades, venta.totalPrecio));

                                MessageBox.Show(String.Format("Compra realizada con éxito, código del pedido: {0}", venta.VentaId), "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                            }
                            else
                            {
                                MessageBox.Show("No se ha podido finalizar la compra debido a que no queda stock para ninguno de los artículos del carrito", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            }

                            mw.detallesVenta = new HashSet<DetalleVenta>();
                            mw.idDetalleVenta = 1;
                            Close();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Los datos introducidos en el método de pago no son correctos", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Faltan datos por cubrir", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void Autoreabastecimiento()
        {
            if (mw.uof.ConfiguracionRepository.MultiGet(c => c.nombre.Equals(MainWindow.AUTOREABASTECIMIENTO)).FirstOrDefault().estado)
            {
                List<Compra> listaCompras = new List<Compra>();
                HashSet<int> listaIds = new HashSet<int>();
                Compra c;

                foreach (DetalleVenta dv in venta.detallesVenta)
                {
                    if (dv.detalleArticulo.articulo.proveedor.estado.Equals(MainWindow.HABILITADO))
                    {
                        listaIds.Add(dv.detalleArticulo.articulo.ProveedorId);
                    }
                }

                foreach (int i in listaIds)
                {
                    c = new Compra(1, i);
                    c.fecha = DateTime.Now;

                    listaCompras.Add(c);
                }

                DetalleCompra dc;
                foreach (DetalleVenta dv in venta.detallesVenta)
                {
                    if (dv.detalleArticulo.articulo.nivelNuevoPedido > 0 && dv.detalleArticulo.stock < dv.detalleArticulo.articulo.nivelNuevoPedido && dv.detalleArticulo.unidadesEnPedido == 0)
                    {
                        foreach (Compra compra in listaCompras)
                        {
                            if (compra.ProveedorId == dv.detalleArticulo.articulo.ProveedorId)
                            {
                                dc = new DetalleCompra(compra.detallesCompra.Count + 1, dv.detalleArticulo, compra);
                                dc.unidades = dv.detalleArticulo.articulo.nivelMaximoPedido - dv.detalleArticulo.stock;

                                compra.detallesCompra.Add(dc);

                                dv.detalleArticulo.unidadesEnPedido = dc.unidades;

                                break;
                            }
                        }
                    }
                }

                foreach (Compra compra in listaCompras)
                {
                    if (compra.detallesCompra.Count > 0)
                    {
                        mw.uof.CompraRepository.Add(compra);
                        mw.SendEmail(MainWindow.EMAIL_AURITEX, String.Format(MainWindow.HTML_COMPRA_REALIZADA_MAESTRO, compra.CompraId, compra.totalUnidades, compra.totalPrecio));
                    }
                }
            }
        }

        private void tb_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int ascci = Convert.ToInt32(Convert.ToChar(e.Text));
            if (ascci >= 48 && ascci <= 57)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        #endregion

        #region METODOS VENTANA

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            try
            {
                base.OnMouseLeftButtonDown(e);
                DragMove();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error moviendo la ventana - {0}", ex.Message));
            }
        }

        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
        }

        #endregion

    }
}
