﻿using Auritex.Model;
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace ClienteAuritex.Windows
{
    /// <summary>
    /// Lógica de interacción para VentanaLoginRegistro.xaml
    /// </summary>
    public partial class VentanaLoginRegistro : Window
    {

        private MainWindow mw;
        private VentanaCarrito vc;
        private Cliente cliente;
        private bool source;

        public VentanaLoginRegistro(MainWindow mw, VentanaCarrito vc, bool source)
        {
            InitializeComponent();

            if (mw != null)
            {
                Owner = mw;
                this.mw = mw;
            }
            else
            {
                Owner = vc;
                this.mw = vc.mw;
                this.vc = vc;
            }
            this.source = source;

            cbCiudadCliente.ItemsSource =  this.mw.uof.GeografiaRepository.GetGeografia("PROVINCIA");
        }

        #region PANEL LOGIN

        private void tRegistrarse_Click(object sender, RoutedEventArgs e)
        {
            cliente = new Cliente();

            title.Content = "Auritex - Registro";

            panelResgistroCliente.DataContext = cliente;
            panelLogin.Visibility = Visibility.Collapsed;
            panelResgistroCliente.Visibility = Visibility.Visible;
        }

        private void tResetearContraseña_Click(object sender, RoutedEventArgs e)
        {
            title.Content = "Auritex - Recuperar contraseña";

            panelLogin.Visibility = Visibility.Collapsed;
            panelResetearContraseña.Visibility = Visibility.Visible;
        }

        private void btCanelarLogin_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btLogin_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void Login()
        {
            if (!String.IsNullOrEmpty(tbCorreoLogin.Text) && !String.IsNullOrEmpty(pbContraseñaLogin.Password))
            {
                mw.ActualizarContexto();

                mw.cliente = mw.uof.ClienteRepository.MultiGet(c => c.email.Equals(tbCorreoLogin.Text) && c.contraseña.Equals(pbContraseñaLogin.Password) && c.estado.Equals(MainWindow.HABILITADO)).FirstOrDefault();

                if (mw.cliente != null)
                {
                    if (vc != null)
                    {
                        vc.title.Content = "Auritex - Túnel de compra";
                        vc.panelDatosEnvio.DataContext = mw.cliente;
                        vc.panelCarrito.Visibility = Visibility.Collapsed;
                        vc.panelTunelCompra.Visibility = Visibility.Visible;

                        Close();
                    }
                    else
                    {
                        if (source)
                        {
                            vc = new VentanaCarrito(mw, true);
                            Close();
                            vc.ShowDialog();
                        }
                        else
                        {
                            VentanaAreaPersonal vap = new VentanaAreaPersonal(mw);
                            Close();
                            vap.ShowDialog();
                        }
                    }                 
                }
                else
                {
                    MessageBox.Show("Email y/o contraseña incorrectos", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Faltan campos por rellenar", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void panelLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Login();
            }
        }

        #endregion

        #region PANEL RESETEAR CONTRASEÑA

        private void btAtrasReseteo_Click(object sender, RoutedEventArgs e)
        {
            tbCorreoResetearContraseña.Clear();

            title.Content = "Auritex - Login";

            panelResetearContraseña.Visibility = Visibility.Collapsed;
            panelLogin.Visibility = Visibility.Visible;
        }

        private void btEnviarReseteoContraseña_Click(object sender, RoutedEventArgs e)
        {
            ResetearContraseña();
        }

        private void ResetearContraseña()
        {
            if (!String.IsNullOrEmpty(tbCorreoResetearContraseña.Text))
            {
                mw.ActualizarContexto();

                cliente = mw.uof.ClienteRepository.MultiGet(c => c.email.Equals(tbCorreoResetearContraseña.Text) && c.estado.Equals(MainWindow.HABILITADO)).FirstOrDefault();
                if (cliente != null)
                {
                    Random rd = new Random();
                    string letras = "Aa.Bb_Cc.Dd_Ee.Ff_Gg.Hh_Ii.Jj_Kk.Ll_Mm.Nn_Oo.Pp_Qq.Rr_Ss.Tt_IUu.Vv_Ww.Xx_Yy.Zz";
                    string newPassword = "";

                    for (int i = 0; i < 5; i++)
                    {
                        newPassword += letras.ElementAt(rd.Next(letras.Length)).ToString() + rd.Next(10);
                    }

                    if (mw.SendEmail(tbCorreoResetearContraseña.Text, String.Format(MainWindow.HTML_CONTRASEÑA_RESETEADA, cliente.nombre, newPassword)))
                    {
                        cliente.contraseña = newPassword;
                        mw.uof.ClienteRepository.Modify(cliente);

                        tbCorreoResetearContraseña.Clear();
                        panelResetearContraseña.Visibility = Visibility.Collapsed;
                        panelLogin.Visibility = Visibility.Visible;

                        MessageBox.Show("Contraseña reseteada, no olvides comprobar tu correo electrónico", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("No se ha podido enviar el correo con la nueva contraseña, por favor, vuelve a intentarlo", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("No existe ningún usuario registrado con este email", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Email no introducido", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void panelResetearContraseña_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ResetearContraseña();
            }
        }

        #endregion

        #region PANEL REGISTRO

        private void btAtrasRegistro_Click(object sender, RoutedEventArgs e)
        {
            panelResgistroCliente.DataContext = null;
            pbContraseñaCliente.Clear();

            title.Content = "Auritex - Login";

            panelResgistroCliente.Visibility = Visibility.Collapsed;
            panelLogin.Visibility = Visibility.Visible;
        }

        private void btRegistrarse_Click(object sender, RoutedEventArgs e)
        {
            Registrarse();
        }

        private void Registrarse()
        {
            cliente.contraseña = pbContraseñaCliente.Password;
            if (mw.Validar(cliente))
            {
                mw.ActualizarContexto();

                if (mw.uof.ClienteRepository.MultiGet(c => c.email.Equals(cliente.email) && c.estado.Equals(MainWindow.HABILITADO)).Count() == 0)
                {
                    mw.uof.ClienteRepository.Add(cliente);

                    mw.SendEmail(cliente.email, String.Format(MainWindow.HTML_CLIENTE_REGISTRADO, cliente.nombre.ToUpper()));
                    mw.SendEmail(MainWindow.EMAIL_AURITEX, String.Format(MainWindow.HTML_CLIENTE_REGISTRADO_MAESTRO, cliente.ClienteId));

                    mw.cliente = cliente;

                    MessageBox.Show("Usuario registrado con éxito", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);

                    if (vc != null)
                    {
                        vc.title.Content = "Auritex - Túnel de compra";
                        vc.panelDatosEnvio.DataContext = mw.cliente;
                        vc.panelCarrito.Visibility = Visibility.Collapsed;
                        vc.panelTunelCompra.Visibility = Visibility.Visible;
                        
                        Close();
                    }
                    else
                    {
                        if (source)
                        {
                            vc = new VentanaCarrito(mw, true);
                            Close();
                            vc.ShowDialog();
                        }
                        else
                        {
                            VentanaAreaPersonal vap = new VentanaAreaPersonal(mw);
                            Close();
                            vap.ShowDialog();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Ya existe un usuario registrado con este email", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        private void panelResgistroCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Registrarse();
            }
        }

        #endregion

        #region METODOS VENTANA

        private void tb_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int ascci = Convert.ToInt32(Convert.ToChar(e.Text));
            if (ascci >= 48 && ascci <= 57)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            try
            {
                base.OnMouseLeftButtonDown(e);
                DragMove();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error moviendo la ventana - {0}", ex.Message));
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
        }

        #endregion

    }
}
