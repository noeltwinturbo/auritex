﻿using Auritex.Model;
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace ClienteAuritex.Windows
{
    /// <summary>
    /// Lógica de interacción para VentanaDetalleArticuloInfo.xaml
    /// </summary>
    public partial class VentanaDetalleArticuloInfo : Window
    {

        private VentanaAreaPersonal vap;
        private DetalleVenta dv;

        public VentanaDetalleArticuloInfo(DetalleVenta dv, VentanaAreaPersonal vap)
        {
            InitializeComponent();

            Owner = vap;
            this.dv = dv;
            this.vap = vap;

            panelDatosArticulo.DataContext = dv;
            imagenArticulo.Source = vap.mw.ConvertArrayByteToImage(dv.detalleArticulo.articulo.imagen);
        }

        private void btDevolverDetalleArticulo_Click(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show(String.Format("{0}, ¿estás seguro de que quieres devolver este artículo?", vap.mw.cliente.nombre), "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                vap.mw.ActualizarContexto();

                if (vap.mw.uof.DetalleVentaRepository.MultiGet(c => c.DetalleVentaId == dv.DetalleVentaId && c.VentaId == dv.VentaId && c.estado.Equals("VENDIDO")).Count() == 1)
                {
                    if ((DateTime.Now - dv.venta.fecha).TotalDays <= 30)
                    {
                        dv.detalleArticulo.stock++;
                        dv.estado = "DEVUELTO";
                        vap.mw.uof.DetalleVentaRepository.Modify(dv);

                        vap.mw.SendEmail(vap.mw.cliente.email, String.Format(MainWindow.HTML_ARTICULO_DEVUELTO, vap.mw.cliente.nombre, dv.detalleArticulo.articulo.nombre, dv.detalleArticulo.talla, dv.detalleArticulo.articulo.sexo, dv.detalleArticulo.articulo.precioVenta));
                        vap.mw.SendEmail(MainWindow.EMAIL_AURITEX, String.Format(MainWindow.HTML_ARTICULO_DEVUELTO_MAESTRO, vap.mw.cliente.ClienteId, dv.detalleArticulo.articulo.nombre, dv.detalleArticulo.talla, dv.detalleArticulo.articulo.sexo, dv.detalleArticulo.articulo.precioVenta));

                        panelDatosArticulo.DataContext = null;
                        panelDatosArticulo.DataContext = dv;
                        vap.dgHistorialDetallesPedido.Items.Refresh();

                        MessageBox.Show("Artículo devuelto", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("No se puede devolver este artículo porque ya han pasado más de 30 días desde que fue comprado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Este artículo ya ha sido devuelto", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            try
            {
                base.OnMouseLeftButtonDown(e);
                DragMove();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error moviendo la ventana - {0}", ex.Message));
            }
        }

        private void btClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
        }

    }
}
