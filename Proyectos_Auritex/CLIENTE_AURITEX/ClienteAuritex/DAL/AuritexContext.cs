﻿using Auritex.Model;
using System.Data.Entity;

namespace Auritex.DAL
{
    public class AuritexContext : DbContext
    {

        public AuritexContext() : base("Auritex") { }

        public DbSet<Proveedor> Proveedor { get; set; }
        public DbSet<Compra> Compra { get; set; }
        public DbSet<DetalleCompra> DetalleCompra { get; set; }
        public DbSet<Articulo> Articulo { get; set; }
        public DbSet<Categoria> Categoria { get; set; }
        public DbSet<DetalleArticulo> DetalleArticulo { get; set; }
        public DbSet<DetalleVenta> DetalleVenta { get; set; }
        public DbSet<Venta> Venta { get; set; }
        public DbSet<Empleado> Empleado { get; set; }
        public DbSet<Turno> Turno { get; set; }
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Configuracion> Configuracion { get; set; }
        public DbSet<Login> Login { get; set; }
        public DbSet<Geografia> Geografia { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }
}
