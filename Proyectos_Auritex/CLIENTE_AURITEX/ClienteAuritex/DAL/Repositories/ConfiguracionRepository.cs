﻿using Auritex.Model;

namespace Auritex.DAL.Repositories
{
    public class ConfiguracionRepository : GenericRepository<Configuracion>
    {
        public ConfiguracionRepository(AuritexContext context) : base(context) { }
    }
}
