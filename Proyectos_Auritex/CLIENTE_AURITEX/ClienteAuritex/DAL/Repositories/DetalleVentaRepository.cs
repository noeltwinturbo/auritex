﻿using Auritex.Model;
using System.Collections.Generic;

namespace Auritex.DAL.Repositories
{
    public class DetalleVentaRepository : GenericRepository<DetalleVenta>
    {

        public DetalleVentaRepository(AuritexContext context) : base(context) { }

        public bool CheckStockDetalleVentaDetalleArticuloCarrito(HashSet<DetalleVenta> detallesVenta, DetalleArticulo da)
        {
            int unidades = 0;

            foreach (DetalleVenta dv in detallesVenta)
            {
                if ((dv.detalleArticulo.talla + dv.detalleArticulo.ArticuloId).Equals(da.talla + da.ArticuloId))
                {
                    unidades++;
                }
            }

            return (da.stock > unidades) ? true : false;
        }

    }
}
