﻿using Auritex.Model;
using System.Collections.Generic;

namespace Auritex.DAL.Repositories
{
    public class GeografiaRepository : GenericRepository<Geografia>
    {

        public GeografiaRepository(AuritexContext context) : base(context) { }

        public List<string> GetGeografia (string tipo)
        {
            List<string> listaGeografia = new List<string>();

            foreach (Geografia g in MultiGet(c => c.tipo.Equals(tipo)))
            {
                listaGeografia.Add(g.nombre);
            }

            return listaGeografia;
        }

    }
}
