﻿using Auritex.Model;

namespace Auritex.DAL.Repositories
{
    public class DetalleCompraRepository : GenericRepository<DetalleCompra>
    {
        public DetalleCompraRepository(AuritexContext context) : base(context) { }
    }
}
