﻿using Auritex.Model;

namespace Auritex.DAL.Repositories
{
    public class ProveedorRepository : GenericRepository<Proveedor>
    {
        public ProveedorRepository(AuritexContext context) : base(context) { }
    }
}
