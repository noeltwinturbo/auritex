﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;

namespace Auritex.DAL.Repositories
{
    public class GenericRepository<TEntity> where TEntity: class
    {

        protected AuritexContext context;
        private DbSet<TEntity> dbSet;

        public GenericRepository(AuritexContext context)
        {
            this.context = context;
            dbSet = context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            dbSet.Add(entity);
            context.SaveChanges();

            ActualizarContexto();
        }

        public void Modify(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();

            ActualizarContexto();
        }

        public void Remove(TEntity entity)
        {
            dbSet.Remove(entity);
            context.SaveChanges();

            ActualizarContexto();
        }

        public int RemoveMore(Expression<Func<TEntity, bool>> predicate)
        {
            var entities = dbSet.Where(predicate).ToList();

            entities.ForEach(x => dbSet.Remove(x));
            context.SaveChanges();

            ActualizarContexto();

            return entities.Count();
        }

        public virtual IEnumerable<TEntity> MultiGet(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        private void ActualizarContexto()
        {
            try
            {
                foreach (var entity in context.ChangeTracker.Entries())
                {
                    entity.Reload();
                }

                Debug.WriteLine("Contexto actualizado");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error actualizando el contexto - {0}", ex.Message));
            }
        }

    }
}
