﻿using Auritex.DAL.Repositories;

namespace Auritex.DAL
{
    public class UnitOfWork
    {

        public AuritexContext context { get; } = new AuritexContext();
        private ProveedorRepository proveedorRepository;
        private CompraRepository compraRepository;
        private DetalleCompraRepository detalleCompraRepository;
        private ArticuloRepository articuloRepository;
        private CategoriaRepository categoriaRepository;
        private DetalleArticuloRepository detalleArticuloRepository;
        private DetalleVentaRepository detalleVentaRepository;
        private VentaRepository ventaRepository;
        private EmpleadoRepository empleadoRepository;
        private TurnoRepository turnoRepository;
        private ClienteRepository clienteRepository;
        private ConfiguracionRepository configuracionRepository;
        private LoginRepository loginRepository;
        private GeografiaRepository geografiaRepository;

        public ProveedorRepository ProveedorRepository
        {
            get
            {
                if (proveedorRepository == null)
                {
                    proveedorRepository = new ProveedorRepository(context);
                }
                return proveedorRepository;
            }
        }

        public CompraRepository CompraRepository
        {
            get
            {
                if (compraRepository == null)
                {
                    compraRepository = new CompraRepository(context);
                }
                return compraRepository;
            }
        }

        public DetalleCompraRepository DetalleCompraRepository
        {
            get
            {
                if (detalleCompraRepository == null)
                {
                    detalleCompraRepository = new DetalleCompraRepository(context);
                }
                return detalleCompraRepository;
            }
        }

        public ArticuloRepository ArticuloRepository
        {
            get
            {
                if (articuloRepository == null)
                {
                    articuloRepository = new ArticuloRepository(context);
                }
                return articuloRepository;
            }
        }

        public CategoriaRepository CategoriaRepository
        {
            get
            {
                if (categoriaRepository == null)
                {
                    categoriaRepository = new CategoriaRepository(context);
                }
                return categoriaRepository;
            }
        }

        public DetalleArticuloRepository DetalleArticuloRepository
        {
            get
            {
                if (detalleArticuloRepository == null)
                {
                    detalleArticuloRepository = new DetalleArticuloRepository(context);
                }
                return detalleArticuloRepository;
            }
        }

        public DetalleVentaRepository DetalleVentaRepository
        {
            get
            {
                if (detalleVentaRepository == null)
                {
                    detalleVentaRepository = new DetalleVentaRepository(context);
                }
                return detalleVentaRepository;
            }
        }

        public VentaRepository VentaRepository
        {
            get
            {
                if (ventaRepository == null)
                {
                    ventaRepository = new VentaRepository(context);
                }
                return ventaRepository;
            }
        }

        public EmpleadoRepository EmpleadoRepository
        {
            get
            {
                if (empleadoRepository == null)
                {
                    empleadoRepository = new EmpleadoRepository(context);
                }
                return empleadoRepository;
            }
        }

        public TurnoRepository TurnoRepository
        {
            get
            {
                if (turnoRepository == null)
                {
                    turnoRepository = new TurnoRepository(context);
                }
                return turnoRepository;
            }
        }

        public ClienteRepository ClienteRepository
        {
            get
            {
                if (clienteRepository == null)
                {
                    clienteRepository = new ClienteRepository(context);
                }
                return clienteRepository;
            }
        }

        public ConfiguracionRepository ConfiguracionRepository
        {
            get
            {
                if (configuracionRepository == null)
                {
                    configuracionRepository = new ConfiguracionRepository(context);
                }
                return configuracionRepository;
            }
        }

        public LoginRepository LoginRepository
        {
            get
            {
                if (loginRepository == null)
                {
                    loginRepository = new LoginRepository(context);
                }
                return loginRepository;
            }
        }

        public GeografiaRepository GeografiaRepository
        {
            get
            {
                if (geografiaRepository == null)
                {
                    geografiaRepository = new GeografiaRepository(context);
                }
                return geografiaRepository;
            }
        }

    }
}
