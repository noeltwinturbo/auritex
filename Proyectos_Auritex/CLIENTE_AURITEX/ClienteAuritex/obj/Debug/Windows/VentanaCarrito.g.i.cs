﻿#pragma checksum "..\..\..\Windows\VentanaCarrito.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "14D656C37C60EC7AE47C7E66337A5DE90B354705"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using ClienteAuritex.Windows;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ClienteAuritex.Windows {
    
    
    /// <summary>
    /// VentanaCarrito
    /// </summary>
    public partial class VentanaCarrito : System.Windows.Window, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 9 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DockPanel ventana;
        
        #line default
        #line hidden
        
        
        #line 10 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.StatusBar toolbar;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem btClose;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label title;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid panelCarrito;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btVaciarCarrito;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imagenArticulo;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgDetallesVenta;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbTotalVenta;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btContinuarComprando;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btFinalizarCompra;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid panelTunelCompra;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid panelDatosEnvio;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbCiudadEnvio;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbCodPostalEnvio;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbDireccionEnvio;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btModificarDireccionEnvio;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid panelFormaPago;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btTarjetaCredito;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btPaypal;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid panelTarjetaCredito;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNombreTarjetaCredito;
        
        #line default
        #line hidden
        
        
        #line 120 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNumeroTarjeta1;
        
        #line default
        #line hidden
        
        
        #line 121 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNumeroTarjeta2;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNumeroTarjeta3;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNumeroTarjeta4;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbMesTarjetaCredito;
        
        #line default
        #line hidden
        
        
        #line 131 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbAnoTarjetaCredito;
        
        #line default
        #line hidden
        
        
        #line 136 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbCodigoCCV;
        
        #line default
        #line hidden
        
        
        #line 140 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid panelPaypal;
        
        #line default
        #line hidden
        
        
        #line 151 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbCorreoPaypal;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox pbContrasenaPaypal;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbTotalVentaTunelCompra;
        
        #line default
        #line hidden
        
        
        #line 164 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btAtras;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\..\Windows\VentanaCarrito.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btPagar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ClienteAuritex;component/windows/ventanacarrito.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Windows\VentanaCarrito.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 8 "..\..\..\Windows\VentanaCarrito.xaml"
            ((ClienteAuritex.Windows.VentanaCarrito)(target)).KeyDown += new System.Windows.Input.KeyEventHandler(this.Window_KeyDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.ventana = ((System.Windows.Controls.DockPanel)(target));
            return;
            case 3:
            this.toolbar = ((System.Windows.Controls.Primitives.StatusBar)(target));
            return;
            case 4:
            this.btClose = ((System.Windows.Controls.MenuItem)(target));
            
            #line 12 "..\..\..\Windows\VentanaCarrito.xaml"
            this.btClose.Click += new System.Windows.RoutedEventHandler(this.btClose_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.title = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.panelCarrito = ((System.Windows.Controls.Grid)(target));
            return;
            case 7:
            this.btVaciarCarrito = ((System.Windows.Controls.Button)(target));
            
            #line 37 "..\..\..\Windows\VentanaCarrito.xaml"
            this.btVaciarCarrito.Click += new System.Windows.RoutedEventHandler(this.btVaciarCarrito_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.imagenArticulo = ((System.Windows.Controls.Image)(target));
            return;
            case 9:
            this.dgDetallesVenta = ((System.Windows.Controls.DataGrid)(target));
            
            #line 39 "..\..\..\Windows\VentanaCarrito.xaml"
            this.dgDetallesVenta.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dgDetallesVenta_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 11:
            this.lbTotalVenta = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.btContinuarComprando = ((System.Windows.Controls.Button)(target));
            
            #line 58 "..\..\..\Windows\VentanaCarrito.xaml"
            this.btContinuarComprando.Click += new System.Windows.RoutedEventHandler(this.btContinuarComprando_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.btFinalizarCompra = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\..\Windows\VentanaCarrito.xaml"
            this.btFinalizarCompra.Click += new System.Windows.RoutedEventHandler(this.btFinalizarCompra_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.panelTunelCompra = ((System.Windows.Controls.Grid)(target));
            return;
            case 15:
            this.panelDatosEnvio = ((System.Windows.Controls.Grid)(target));
            return;
            case 16:
            this.cbCiudadEnvio = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 17:
            this.tbCodPostalEnvio = ((System.Windows.Controls.TextBox)(target));
            
            #line 87 "..\..\..\Windows\VentanaCarrito.xaml"
            this.tbCodPostalEnvio.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.tb_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 18:
            this.tbDireccionEnvio = ((System.Windows.Controls.TextBox)(target));
            return;
            case 19:
            this.btModificarDireccionEnvio = ((System.Windows.Controls.Button)(target));
            
            #line 89 "..\..\..\Windows\VentanaCarrito.xaml"
            this.btModificarDireccionEnvio.Click += new System.Windows.RoutedEventHandler(this.btModificarDireccionEnvio_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.panelFormaPago = ((System.Windows.Controls.Grid)(target));
            return;
            case 21:
            this.btTarjetaCredito = ((System.Windows.Controls.Button)(target));
            
            #line 99 "..\..\..\Windows\VentanaCarrito.xaml"
            this.btTarjetaCredito.Click += new System.Windows.RoutedEventHandler(this.btTarjetaCredito_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.btPaypal = ((System.Windows.Controls.Button)(target));
            
            #line 102 "..\..\..\Windows\VentanaCarrito.xaml"
            this.btPaypal.Click += new System.Windows.RoutedEventHandler(this.btPaypal_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.panelTarjetaCredito = ((System.Windows.Controls.Grid)(target));
            return;
            case 24:
            this.tbNombreTarjetaCredito = ((System.Windows.Controls.TextBox)(target));
            return;
            case 25:
            this.tbNumeroTarjeta1 = ((System.Windows.Controls.TextBox)(target));
            
            #line 120 "..\..\..\Windows\VentanaCarrito.xaml"
            this.tbNumeroTarjeta1.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.tb_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 26:
            this.tbNumeroTarjeta2 = ((System.Windows.Controls.TextBox)(target));
            
            #line 121 "..\..\..\Windows\VentanaCarrito.xaml"
            this.tbNumeroTarjeta2.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.tb_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 27:
            this.tbNumeroTarjeta3 = ((System.Windows.Controls.TextBox)(target));
            
            #line 122 "..\..\..\Windows\VentanaCarrito.xaml"
            this.tbNumeroTarjeta3.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.tb_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 28:
            this.tbNumeroTarjeta4 = ((System.Windows.Controls.TextBox)(target));
            
            #line 123 "..\..\..\Windows\VentanaCarrito.xaml"
            this.tbNumeroTarjeta4.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.tb_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 29:
            this.cbMesTarjetaCredito = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 30:
            this.cbAnoTarjetaCredito = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 31:
            this.tbCodigoCCV = ((System.Windows.Controls.TextBox)(target));
            
            #line 136 "..\..\..\Windows\VentanaCarrito.xaml"
            this.tbCodigoCCV.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.tb_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 32:
            this.panelPaypal = ((System.Windows.Controls.Grid)(target));
            return;
            case 33:
            this.tbCorreoPaypal = ((System.Windows.Controls.TextBox)(target));
            return;
            case 34:
            this.pbContrasenaPaypal = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 35:
            this.lbTotalVentaTunelCompra = ((System.Windows.Controls.Label)(target));
            return;
            case 36:
            this.btAtras = ((System.Windows.Controls.Button)(target));
            
            #line 164 "..\..\..\Windows\VentanaCarrito.xaml"
            this.btAtras.Click += new System.Windows.RoutedEventHandler(this.btAtras_Click);
            
            #line default
            #line hidden
            return;
            case 37:
            this.btPagar = ((System.Windows.Controls.Button)(target));
            
            #line 165 "..\..\..\Windows\VentanaCarrito.xaml"
            this.btPagar.Click += new System.Windows.RoutedEventHandler(this.btPagar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 10:
            
            #line 49 "..\..\..\Windows\VentanaCarrito.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btQuitarDetalleArticuloCesta_Click);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

