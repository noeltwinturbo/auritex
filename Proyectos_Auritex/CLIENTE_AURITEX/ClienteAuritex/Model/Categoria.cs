﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auritex.Model
{
    [Table("Categoria")]
    public class Categoria
    {

        public Categoria()
        {
            articulos = new HashSet<Articulo>();
        }

        public int CategoriaId { get; set; }

        [Required(ErrorMessage = "Nombre requerido")]
        [DataType(DataType.Text, ErrorMessage = "Nombre no válido")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 100 caracteres para el nombre")]
        public string nombre { get; set; }

        [DataType(DataType.Text, ErrorMessage = "Descripción no válido")]
        [StringLength(500, ErrorMessage = "Máximo 500 caracteres para la descpripción")]
        public string descripcion { get; set; }

        public byte[] imagen { get; set; }

        public virtual ICollection<Articulo> articulos { get; set; }

    }
}
