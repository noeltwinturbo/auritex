﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auritex.Model
{
    [Table("Geografia")]
    public class Geografia
    {

        public Geografia() { }

        public int GeografiaId { get; set; }

        [Required(ErrorMessage = "Nombre requerido")]
        [DataType(DataType.Text, ErrorMessage = "Nombre no válido")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 100 caracteres para el nombre")]
        public string nombre { get; set; }

        [Required(ErrorMessage = "Tipo requerido")]
        [DataType(DataType.Text, ErrorMessage = "Tipo no válido")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 100 caracteres para el tipo")]
        public string tipo { get; set; }

    }
}
