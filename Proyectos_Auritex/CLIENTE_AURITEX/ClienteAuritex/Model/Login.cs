﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auritex.Model
{
    [Table("Login")]
    public class Login
    {

        public Login() { }

        [Key, Column(Order = 0)]
        [Required(ErrorMessage = "Fecha entrada requerida")]
        [DataType(DataType.DateTime, ErrorMessage = "Fecha entrada no válida")]
        public DateTime fechaEntrada { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "Fecha salida no válida")]
        public DateTime? fechaSalida { get; set; }

        [Key, Column(Order = 1)]
        public int EmpleadoId { get; set; }

        public virtual Empleado empleado { get; set; }

    }
}
