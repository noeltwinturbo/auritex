﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auritex.Model
{
    [Table("Turno")]
    public class Turno
    {

        public Turno()
        {
            empleados = new HashSet<Empleado>();
        }

        [Key, Column(Order = 0)]
        [Required(ErrorMessage = "Dia requerido")]
        [RegularExpression("lunes|martes|miércoles|jueves|viernes|sábado", ErrorMessage = "Día no válido")]
        public string diaSemana { get; set; }

        [Key, Column(Order = 1)]
        [Required(ErrorMessage = "Turno requerido")]
        [RegularExpression("mañana|tarde", ErrorMessage = "Turno no válido")]
        public string turno { get; set; }

        public virtual ICollection<Empleado> empleados { get; set; }

    }
}
