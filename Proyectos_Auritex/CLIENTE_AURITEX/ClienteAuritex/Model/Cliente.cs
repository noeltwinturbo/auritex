﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auritex.Model
{
    [Table("Cliente")]
    public class Cliente
    {

        public Cliente()
        {
            estado = "HABILITADO";
            ventas = new HashSet<Venta>();
        }

        public int ClienteId { get; set; }

        [Required(ErrorMessage = "Nombre requerido")]
        [DataType(DataType.Text, ErrorMessage = "Nombre no válido")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 50 caracteres para el nombre")]
        public string nombre { get; set; }

        [Required(ErrorMessage = "Apellidos requeridos")]
        [DataType(DataType.Text, ErrorMessage = "Apellidos no válidos")]
        [StringLength(150, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 150 caracteres para los apellidos")]
        public string apellidos { get; set; }

        [DataType(DataType.DateTime, ErrorMessage = "Fecha nacimiento no válida")]
        public DateTime? fechaNacimiento { get; set; }

        [DataType(DataType.Text, ErrorMessage = "DNI no válido")]
        [RegularExpression("[0-9]{8}[A-Z]{1}", ErrorMessage = "Dni no válido")]
        public string dni { get; set; }

        [RegularExpression("[0-9]{9}", ErrorMessage = "Teléfono no válido")]
        public string telefono { get; set; }

        [Required(ErrorMessage = "Email requerido")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Email no válido")]
        [StringLength(75, ErrorMessage = "Máximo 75 caracteres para el email")]
        public string email { get; set; }

        [Required(ErrorMessage = "Contraseña requerida")]
        [DataType(DataType.Password, ErrorMessage = "Contraseña no válida")]
        [StringLength(25, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 25 caracteres para la contraseña")]
        public string contraseña { get; set; }

        [DataType(DataType.Text, ErrorMessage = "Ciudad no válida")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 100 caracteres para la ciudad")]
        public string ciudad { get; set; }

        [RegularExpression("[0-9]{5}", ErrorMessage = "Código postal no válido")]
        public string codPostal { get; set; }

        [DataType(DataType.Text, ErrorMessage = "Dirección no válida")]
        [StringLength(150, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 150 caracteres para la dirección")]
        public string direccion { get; set; }

        [Required(ErrorMessage = "Estado Requerido")]
        [RegularExpression("HABILITADO|DESHABILITADO", ErrorMessage = "Estado no válido")]
        public string estado { get; set; }

        public string fechaNacCorta
        {
            get
            {
                return (fechaNacimiento != null) ? ((DateTime)fechaNacimiento).ToShortDateString() : null;
            }
        }

        public virtual ICollection<Venta> ventas { get; set; }

    }
}
