﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auritex.Model
{
    [Table("Articulo")]
    public class Articulo
    {

        public Articulo()
        {
            estado = "HABILITADO";
            detallesArticulo = new HashSet<DetalleArticulo>();
        }

        public int ArticuloId { get; set; }

        [Required(ErrorMessage = "Nombre requerido")]
        [DataType(DataType.Text, ErrorMessage = "Nombre no válido")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 100 caracteres para el nombre")]
        public string nombre { get; set; }

        [Required(ErrorMessage = "Sexo requerido")]
        [RegularExpression("CHICO|CHICA", ErrorMessage = "Estado no válido")]
        public string sexo { get; set; }

        [Required(ErrorMessage = "Precio compra requerido")]
        [Range(0f, 999f, ErrorMessage = "Máximo 999€ precio de compra")]
        public float precioCompra { get; set; }

        [Required(ErrorMessage = "Precio venta requerido")]
        [Range(0f, 999f, ErrorMessage = "Máximo 999€ precio de venta")]
        public float precioVenta { get; set; }

        [Required(ErrorMessage = "Nivel nuevo pedido requerido")]
        [Range(0, 1000, ErrorMessage = "Máximo 999 unidades de nivel nuevo pedido")]
        public int nivelNuevoPedido { get; set; }

        [Required(ErrorMessage = "Nivel máximo pedido requerido")]
        [Range(0, 1000, ErrorMessage = "Máximo 999 unidades de nivel máximo pedido")]
        public int nivelMaximoPedido { get; set; }

        [DataType(DataType.Text, ErrorMessage = "Descripción no válida")]
        [StringLength(500, ErrorMessage = "Máximo 500 caracteres para la descripción")]
        public string descripcion { get; set; }

        public byte[] imagen { get; set; }

        [Required(ErrorMessage = "Estado requerido")]
        [RegularExpression("HABILITADO|DESHABILITADO", ErrorMessage = "Estado no válido")]
        public string estado { get; set; }

        public int stockTotal
        {
            get
            {
                int total = 0;

                foreach (DetalleArticulo da in detallesArticulo)
                {
                    total += da.stock;
                }

                return total;
            }
        }

        public int ProveedorId { get; set; }
        public int CategoriaId { get; set; }

        public virtual Proveedor proveedor { get; set; }
        public virtual Categoria categoria { get; set; }

        public virtual ICollection<DetalleArticulo> detallesArticulo { get; set; }

    }
}
