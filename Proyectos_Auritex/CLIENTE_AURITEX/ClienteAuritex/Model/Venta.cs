﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Auritex.Model
{
    [Table("Venta")]
    public class Venta
    {

        public Venta()
        {
            detallesVenta = new HashSet<DetalleVenta>();
        }

        public Venta(string ciudadEnvio, string codPostalEnvio, string direccionEnvio, string formaPago, int clienteId, HashSet<DetalleVenta> detallesVenta)
        {
            fecha = DateTime.Now;
            this.ciudadEnvio = ciudadEnvio;
            this.codPostalEnvio = codPostalEnvio;
            this.direccionEnvio = direccionEnvio;
            this.formaPago = formaPago;
            descuento = 0f;
            EmpleadoId = 1;
            ClienteId = clienteId;
            this.detallesVenta = detallesVenta;
        }

        public int VentaId { get; set; }

        [Required(ErrorMessage = "Fecha requerida")]
        [DataType(DataType.DateTime, ErrorMessage = "Fecha no válida")]
        public DateTime fecha { get; set; }

        [DataType(DataType.Text, ErrorMessage = "Ciudad no válida")]
        [StringLength(100, ErrorMessage = "Máximo 100 caracteres para la ciudad")]
        public string ciudadEnvio { get; set; }

        [RegularExpression("[0-9]{5}", ErrorMessage = "Código postal no válido")]
        public string codPostalEnvio { get; set; }

        [DataType(DataType.Text, ErrorMessage = "Dirección no válida")]
        [StringLength(150, ErrorMessage = "Máximo 150 caracteres para la dirección")]
        public string direccionEnvio { get; set; }

        [Required(ErrorMessage = "Forma de pago requerido")]
        [RegularExpression("TARJETA DE CRÉDITO|EFECTIVO|PAYPAL", ErrorMessage = "Forma de pago no válida")]
        public string formaPago { get; set; }

        [Required(ErrorMessage = "Descuento requerido")]
        [Range(0f, 100f, ErrorMessage = "Máximo 100% de descuento")]
        public float descuento { get; set; }

        public int totalUnidades
        {
            get
            {
                return detallesVenta.Count();
            }
        }

        public float totalPrecio
        {
            get
            {
                float total = 0;
                
                foreach (DetalleVenta dv in detallesVenta)
                {
                    total += dv.precio;
                }

                return total - ((descuento / 100f) * total);
            }
        }

        public int EmpleadoId { get; set; }
        public int ClienteId { get; set; }

        public virtual Empleado empleado { get; set; }
        public virtual Cliente cliente { get; set; }

        public virtual ICollection<DetalleVenta> detallesVenta { get; set; }

    }
}
