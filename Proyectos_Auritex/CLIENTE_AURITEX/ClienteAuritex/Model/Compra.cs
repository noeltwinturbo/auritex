﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Auritex.Model
{
    [Table("Compra")]
    public class Compra
    {

        public Compra()
        {
            estado = "PENDIENTE";
            detallesCompra = new HashSet<DetalleCompra>();
        }

        public Compra(int empleadoId, int proveedorId)
        {
            EmpleadoId = empleadoId;
            ProveedorId = proveedorId;
            estado = "PENDIENTE";
            detallesCompra = new HashSet<DetalleCompra>();
        }

        public int CompraId { get; set; }

        [Required(ErrorMessage = "Fecha requerida")]
        [DataType(DataType.DateTime, ErrorMessage = "Fecha no válida")]
        public DateTime fecha { get; set; }

        [Required(ErrorMessage = "Estado requerido")]
        [RegularExpression("FINALIZADA|PENDIENTE|CANCELADA", ErrorMessage = "Estado no válido")]
        public string estado { get; set; }

        public int totalUnidades
        {
            get
            {
                return detallesCompra.Count();
            }
        }

        public float totalPrecio
        {
            get
            {
                float total = 0;

                foreach (DetalleCompra dc in detallesCompra)
                {
                    total += dc.subTotalPrecio;
                }

                return total;
            }
        }

        public int ProveedorId { get; set; }
        public int EmpleadoId { get; set; }

        public virtual Proveedor proveedor { get; set; }
        public virtual Empleado empleado { get; set; }

        public virtual ICollection<DetalleCompra> detallesCompra { get; set; }

    }
}
