﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auritex.Model
{
    [Table("DetalleVenta")]
    public class DetalleVenta
    {

        public DetalleVenta() { }

        public DetalleVenta(int id, DetalleArticulo detalleArticulo)
        {
            DetalleVentaId = id;
            precio = detalleArticulo.articulo.precioVenta;
            estado = "VENDIDO";
            this.detalleArticulo = detalleArticulo;
        }

        [Key, Column(Order = 0)]
        public int DetalleVentaId { get; set; }

        [Required(ErrorMessage = "Precio requerido")]
        [Range(0f, 999f, ErrorMessage = "Máximo 999€ precio")]
        public float precio { get; set; }

        [Required(ErrorMessage = "Estado requerido")]
        [RegularExpression("VENDIDO|DEVUELTO", ErrorMessage = "Estado no válido")]
        public string estado { get; set; }

        [Key, Column(Order = 1)]
        public int VentaId { get; set; }
        public string talla { get; set; }
        public int ArticuloId { get; set; }

        public virtual DetalleArticulo detalleArticulo { get; set; }
        public virtual Venta venta { get; set; }

    }
}
