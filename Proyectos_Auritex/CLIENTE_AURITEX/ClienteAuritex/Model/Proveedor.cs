﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auritex.Model
{
    [Table("Proveedor")]
    public class Proveedor
    {

        public Proveedor()
        {
            estado = "HABILITADO";
            articulos = new HashSet<Articulo>();
            compras = new HashSet<Compra>();
        }

        public int ProveedorId { get; set; }

        [Required(ErrorMessage = "Nombre requerido")]
        [DataType(DataType.Text, ErrorMessage = "Nombre no válido")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 50 caracteres para el nombre")]
        public string nombre { get; set; }

        [Required(ErrorMessage = "Teléfono requerido")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Teléfono no válido")]
        [StringLength(12, MinimumLength = 9, ErrorMessage = "Mínimo 9 y máximo 12 caracteres para el teléfono")]
        public string telefono { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "Email no válido")]
        [StringLength(75, ErrorMessage = "Máximo 75 caracteres para el email")]
        public string email { get; set; }

        [DataType(DataType.Text, ErrorMessage = "País no válido")]
        [StringLength(75, ErrorMessage = "Máximo 75 caracteres para el país")]
        public string pais { get; set; }

        [DataType(DataType.Text, ErrorMessage = "Ciudad no válida")]
        [StringLength(100, ErrorMessage = "Máximo 100 caracteres para la ciudad")]
        public string ciudad { get; set; }

        [DataType(DataType.PostalCode, ErrorMessage = "Código postal no válido")]
        [StringLength(10, ErrorMessage = "Máximo 10 caracteres para el código postal")]
        public string codPostal { get; set; }

        [DataType(DataType.Text, ErrorMessage = "Dirección no válida")]
        [StringLength(150, ErrorMessage = "Máximo 150 caracteres para la dirección")]
        public string direccion { get; set; }

        public byte[] imagen { get; set; }

        [Required(ErrorMessage = "Estado Requerido")]
        [RegularExpression("HABILITADO|DESHABILITADO", ErrorMessage = "Estado no válido")]
        public string estado { get; set; }

        public virtual ICollection<Articulo> articulos { get; set; }
        public virtual ICollection<Compra> compras { get; set; }

    }
}
