﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auritex.Model
{
    [Table("Empleado")]
    public class Empleado
    {

        public Empleado()
        {
            fechaContratacion = DateTime.Today;
            fechaNacimiento = DateTime.Today;
            estado = "HABILITADO";
            turnos = new HashSet<Turno>();
            ventas = new HashSet<Venta>();
            compras = new HashSet<Compra>();
            logins = new HashSet<Login>();
        }

        public int EmpleadoId { get; set; }

        [Required(ErrorMessage = "Nombre requerido")]
        [DataType(DataType.Text, ErrorMessage = "Nombre no válido")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 50 caracteres para el nombre")]
        public string nombre { get; set; }

        [Required(ErrorMessage = "Apellidos requeridos")]
        [DataType(DataType.Text, ErrorMessage = "Apellidos no válidos")]
        [StringLength(150, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 150 caracteres para los apellidos")]
        public string apellidos { get; set; }

        [Required(ErrorMessage = "DNI requerido")]
        [RegularExpression("[0-9]{8}[A-Z]{1}", ErrorMessage = "DNI no válido")]
        public string dni { get; set; }

        [Required(ErrorMessage = "Teléfono requerido")]
        [RegularExpression("[0-9]{9}", ErrorMessage = "Teléfono no válido")]
        public string telefono { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "Email no válido")]
        [StringLength(75, ErrorMessage = "Máximo 75 caracteres para el email")]
        public string email { get; set; }

        [DataType(DataType.Text, ErrorMessage = "Dirección no válida")]
        [StringLength(150, ErrorMessage = "Máximo 150 caracteres para la dirección")]
        public string direccion { get; set; }

        [Required(ErrorMessage = "Usuario requerido")]
        [DataType(DataType.Text, ErrorMessage = "Usuario no válido")]
        [StringLength(25, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 25 caracteres para el usuario")]
        public string usuario { get; set; }

        [Required(ErrorMessage = "Contraseña requerida")]
        [DataType(DataType.Password, ErrorMessage = "Contraseña no válida")]
        [StringLength(25, MinimumLength = 3, ErrorMessage = "Mínimo 5 y máximo 25 caracteres para la contraseña")]
        public string contraseña { get; set; }

        [Required(ErrorMessage = "Cargo requerido")]
        [RegularExpression("JEFE|VENTAS", ErrorMessage = "Cargo no válido")]
        public string cargo { get; set; }

        [Required(ErrorMessage = "Fecha nacimiento requerida")]
        [DataType(DataType.DateTime, ErrorMessage = "Fecha nacimiento no válida")]
        public DateTime fechaNacimiento { get; set; }

        [Required(ErrorMessage = "Fecha contratación requerida")]
        [DataType(DataType.DateTime, ErrorMessage = "Fecha contratación no válida")]
        public DateTime fechaContratacion { get; set; }

        public byte[] imagen { get; set; }

        [Required(ErrorMessage = "Estado Requerido")]
        [RegularExpression("HABILITADO|DESHABILITADO", ErrorMessage = "Estado no válido")]
        public string estado { get; set; }

        public string fechaNacCorta
        {
            get
            {
                return fechaNacimiento.ToShortDateString();
            }
        }

        public string fechaContrCorta
        {
            get
            {
                return fechaContratacion.ToShortDateString();
            }
        }

        public virtual ICollection<Turno> turnos { get; set; }
        public virtual ICollection<Venta> ventas { get; set; }
        public virtual ICollection<Compra> compras { get; set; }
        public virtual ICollection<Login> logins { get; set; }

    }
}
