﻿using Auritex.DAL;
using Auritex.Model;
using ClienteAuritex.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ClienteAuritex
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        //***TRABAJO NOEL SEARA RODRIGUEZ DAM221***//

        #region VARIABLES

        public UnitOfWork uof { get; }
        public Cliente cliente { get; set; }
        public HashSet<DetalleVenta> detallesVenta { get; set; }
        private Thread hiloActualizarReloj;
        private Thread hiloCheckMantenimiento;
        private VentanaAreaPersonal vap;
        private VentanaLoginRegistro vlr;
        private VentanaCarrito vc;

        private string idCategoria;
        private string articuloId;
        public int idDetalleVenta { get; set; } = 1;

        public static readonly string AUTOREABASTECIMIENTO = "AUTOREABASTECIMIENTO";
        public static readonly string MANTENIMIENTO = "MANTENIMIENTO";
        public static readonly string HABILITADO = "HABILITADO";
        public static readonly string DESHABILITADO = "DESHABILITADO";
        public static readonly string EMAIL_AURITEX = "auritex@gmx.es";

        #region HTML CORREOS

        public static readonly string HTML_ARTICULO_DEVUELTO =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">DEVOLUCIÓN</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Hola {0}, nos ponemos en contacto contigo para comunicarte de que has devuelto el siguiente artículo:</h3>"
            + "<h3 style=\"padding-left: 60px;\">Artículo: {1}</h3>"
            + "<h3 style=\"padding-left: 60px;\">Talla: {2}</h3>"
            + "<h3 style=\"padding-left: 60px;\">Sexo: {3}</h3>"
            + "<h3 style=\"padding-left: 60px;\">Precio: {4}€</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"padding-left: 30px;\">Para cualquier consulta, por favor, contacta con nuestro servicio de atención al cliente.</p>"
            + "<p style=\"padding-left: 30px;\">Atentamente, equipo de Auritex.</p>"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_CONTRASEÑA_RESETEADA =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">NUEVA CONTRASEÑA</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Hola {0}, aquí tienes tu nueva contraseña. Cuando inicies sesión te recomendamos que la cambies.</h3>"
            + "<h3 style=\"padding-left: 30px;\">Tu nueva contraseña: {1}</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"padding-left: 30px;\">Para cualquier consulta, por favor, contacta con nuestro servicio de atención al cliente.</p>"
            + "<p style=\"padding-left: 30px;\">Atentamente, equipo de Auritex.</p>"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_CLIENTE_REGISTRADO =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">{0} BIENVENIDO A AURITEX</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Nos alegramos mucho de que formes parte de nosotros. Esperamos que nuestros artículos y promociones te satisfagan.</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"padding-left: 30px;\">Para cualquier consulta, por favor, contacta con nuestro servicio de atención al cliente.</p>"
            + "<p style=\"padding-left: 30px;\">Atentamente, equipo de Auritex.</p>"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_ACCESO_MODIFICADO =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">DATOS DE ACCESO MODIFICADOS</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Hola {0}, nos ponemos en contacto contigo para comunicarte que has modificado tus datos de acceso. En caso de no haber sido tú, por favor, contacta con nuestro servicio de atención al cliente para resolverlo.</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"padding-left: 30px;\">Para cualquier consulta, por favor, contacta con nuestro servicio de atención al cliente.</p>"
            + "<p style=\"padding-left: 30px;\">Atentamente, equipo de Auritex.</p>"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_CLIENTE_ELIMINADO =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">CUENTA DE USUARIO ELIMINADA</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Hola {0}, lamentamos mucho que te tengas que ir, esperamos volver a verte muy pronto.</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"padding-left: 30px;\">Para cualquier consulta, por favor, contacta con nuestro servicio de atención al cliente.</p>"
            + "<p style=\"padding-left: 30px;\">Atentamente, equipo de Auritex.</p>"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_PEDIDO_REALIZADO =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">PEDIDO Nº{0}</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Hola {1}, nos ponemos en contacto contigo para comunicarte de que se ha realizado con éxito tu compra de {2} artículos, por un importe total de {3}€.</h3>"
            + "<h3 style=\"padding-left: 30px;\">En un plazo de 3-5 días, recibirás tu pedido.</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"padding-left: 30px;\">Para cualquier consulta, por favor, contacta con nuestro servicio de atención al cliente.</p>"
            + "<p style=\"padding-left: 30px;\">Atentamente, equipo de Auritex.</p>"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_ARTICULO_DEVUELTO_MAESTRO =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">DEVOLUCIÓN</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">El cliente con ID:{0}, ha devuelto el siguiente artículo:</h3>"
            + "<h3 style=\"padding-left: 60px;\">Artículo: {1}</h3>"
            + "<h3 style=\"padding-left: 60px;\">Talla: {2}</h3>"
            + "<h3 style=\"padding-left: 60px;\">Sexo: {3}</h3>"
            + "<h3 style=\"padding-left: 60px;\">Precio: {4}€</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_VENTA_REALIZADA_MAESTRO =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">VENTA Nº{0}</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">El cliente con ID:{1}, ha realizado una compra de {2} artículos, por un importe total de {3}€.</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_COMPRA_REALIZADA_MAESTRO =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">COMPRA POR AUTOREABASTECIMIENTO Nº{0}</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Se ha realizado con éxito una compra de {1} artículos, por un importe total de {2}€.</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_CLIENTE_REGISTRADO_MAESTRO =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">NUEVO USUARIO REGISTRADO</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Se ha registrado un nuevo cliente con ID: {0}.</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_CLIENTE_ELIMINADO_MAESTRO =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">CUENTA DE USUARIO ELIMINADA</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Se ha eliminado el cliente con ID: {0}.</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        #endregion

        #endregion

        public MainWindow()
        {
            InitializeComponent();

            uof = new UnitOfWork();
            MetodosInicio();
        }

        #region METODOS VARIOS

        private void MetodosInicio()
        {
            detallesVenta = new HashSet<DetalleVenta>();

            hiloActualizarReloj = new Thread(ActualizarReloj);
            hiloCheckMantenimiento = new Thread(autoCheckMantenimiento);
            hiloActualizarReloj.Start();
            hiloCheckMantenimiento.Start();

            CargarCategorias();
            idCategoria = "0";
            CargarArticulos(null);
        }

        public BitmapImage ConvertArrayByteToImage(byte[] byteArrayIn)
        {
            try
            {
                MemoryStream stream = new MemoryStream();
                stream.Write(byteArrayIn, 0, byteArrayIn.Length);
                stream.Position = 0;
                System.Drawing.Image img = System.Drawing.Image.FromStream(stream);

                BitmapImage returnImage = new BitmapImage();
                returnImage.BeginInit();

                MemoryStream ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                ms.Seek(0, SeekOrigin.Begin);
                returnImage.StreamSource = ms;

                returnImage.EndInit();

                return returnImage;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error al convertir un array de bytes a BitmapImage - {0}", ex.Message));
                return null;
            }
        }

        public byte[] ConvertImageToArrayByte(BitmapImage image)
        {
            try
            {
                MemoryStream memStream = new MemoryStream();
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();

                encoder.Frames.Add(BitmapFrame.Create(image));
                encoder.Save(memStream);

                return memStream.ToArray();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error al convertir un BitmapImage a un array de bytes - {0}", ex.Message));
                return null;
            }
        }

        public bool SendEmail(string correoCliente, string html)
        {
            MailMessage msg = new MailMessage(EMAIL_AURITEX, correoCliente, ".:Auritex:.", html);
            msg.IsBodyHtml = true;
            SmtpClient sc = new SmtpClient("mail.gmx.es", 587);
            NetworkCredential cre = new NetworkCredential(EMAIL_AURITEX, "Auritex123.");
            sc.Credentials = cre;
            sc.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

            try
            {
                sc.Send(msg);
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error al enviar el correo para \"{0}\" - {1}", correoCliente, ex.Message));
                return false;
            }
        }

        public void ActualizarContexto()
        {
            Dispatcher.Invoke(ActivarCursor);
            try
            {
                foreach (var entity in uof.context.ChangeTracker.Entries())
                {
                    entity.Reload();
                }

                Debug.WriteLine("Contexto actualizado");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error actualizando el contexto - {0}", ex.Message));
            }
            finally
            {
                Dispatcher.Invoke(DesactivarCursor);
            }
        }

        private void ActivarCursor()
        {
            Cursor = Cursors.Wait;
        }

        private void DesactivarCursor()
        {
            Cursor = Cursors.Arrow;
        }

        public Boolean Validar(Object obj)
        {
            ValidationContext validationContext = new ValidationContext(obj, null, null);
            List<System.ComponentModel.DataAnnotations.ValidationResult> errors = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            Validator.TryValidateObject(obj, validationContext, errors, true);
            if (errors.Count() > 0)
            {
                string mensageErrores = string.Empty;
                foreach (var error in errors)
                {
                    error.MemberNames.First();
                    mensageErrores += error.ErrorMessage + Environment.NewLine;
                }
                MessageBox.Show(mensageErrores, "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation); return false;
            }
            else
            {
                return true;
            }
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            try
            {
                base.OnMouseLeftButtonDown(e);
                DragMove();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error moviendo la ventana - {0}", ex.Message));
            }
        }

        #endregion

        #region HILOS

        private void ActualizarReloj()
        {
            while (true)
            {
                Dispatcher.Invoke(Reloj);
                Thread.Sleep(1000);
            }
        }

        private void Reloj()
        {
            lbFechaHora.Content = DateTime.Now.ToLongDateString() + " - " + DateTime.Now.ToLongTimeString();
        }

        private void autoCheckMantenimiento()
        {
            Dispatcher.Invoke(ComprobarMantenimiento);

            while (true)
            {
                Thread.Sleep(15000);

                ActualizarContexto();
                Dispatcher.Invoke(ComprobarMantenimiento);
            }
        }

        private void ComprobarMantenimiento()
        {
            if (uof.ConfiguracionRepository.MultiGet(c => c.nombre.Equals(MANTENIMIENTO)).FirstOrDefault().estado)
            {
                if (vap != null)
                {
                    vap.Close();
                }
                if (vlr != null)
                {
                    vlr.Close();
                }
                if (vc != null)
                {
                    vc.Close();
                }
                if (!panelMantenimiento.IsVisible)
                {
                    panelMantenimiento.Visibility = Visibility.Visible;
                    panelDetalleArticulo.Visibility = Visibility.Collapsed;
                    menu.Visibility = Visibility.Collapsed;
                    panelCategorias.Visibility = Visibility.Collapsed;
                    split.Visibility = Visibility.Collapsed;
                    wpBotonesArticulosVenta.Visibility = Visibility.Collapsed;

                    idDetalleVenta = 1;
                    cliente = null;
                    detallesVenta = new HashSet<DetalleVenta>();
                }
                Debug.WriteLine("Mantenimiento activado");
            }
            else
            {
                if (panelMantenimiento.IsVisible)
                {
                    CargarCategorias();
                    idCategoria = "0";
                    CargarArticulos(null);

                    panelMantenimiento.Visibility = Visibility.Collapsed;
                    wpBotonesArticulosVenta.Visibility = Visibility.Visible;
                    menu.Visibility = Visibility.Visible;
                }
                Debug.WriteLine("Mantenimiento desactivado");
            }
        }

        #endregion

        #region BOTONES MENU

        private void btMenu_Click(object sender, RoutedEventArgs e)
        {
            if (panelCategorias.IsVisible)
            {
                panelCategorias.Visibility = Visibility.Collapsed;
                split.Visibility = Visibility.Collapsed;
            }
            else
            {
                panelCategorias.Visibility = Visibility.Visible;
                split.Visibility = Visibility.Visible;
            }
        }

        private void tbBuscarArticulos_TextChanged(object sender, TextChangedEventArgs e)
        {            
            if (!tbBuscarArticulos.Text.Equals("Buscar..."))
            {
                if (!String.IsNullOrEmpty(tbBuscarArticulos.Text))
                {
                    if (!wpBotonesArticulosVenta.IsVisible)
                    {
                        panelDetalleArticulo.Visibility = Visibility.Collapsed;
                        wpBotonesArticulosVenta.Visibility = Visibility.Visible;
                    }

                    CargarArticulos(tbBuscarArticulos.Text);
                }
                else
                {
                    CargarArticulos(null);
                }
            }
        }

        private void tbBuscarArticulos_GotFocus(object sender, RoutedEventArgs e)
        {
            tbBuscarArticulos.Text = "";
        }

        private void tbBuscarArticulos_LostFocus(object sender, RoutedEventArgs e)
        {
            tbBuscarArticulos.Text = "Buscar...";
        }

        private void btAreaPersonal_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            if (cliente == null)
            {
                vlr = new VentanaLoginRegistro(this, null, false);
                vlr.ShowDialog();
            }
            else
            {
                vap = new VentanaAreaPersonal(this);
                vap.ShowDialog();
            }
        }

        private void btCarrito_Click(object sender, RoutedEventArgs e)
        {
            if (detallesVenta.Count > 0)
            {
                ActualizarContexto();

                if (CheckStockDetalleVentaDetalleArticulo())
                {
                    vc = new VentanaCarrito(this, false);
                    vc.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Se ha vaciado el carrito debido a que no queda stock para ninguno de los artículos añadidos", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("No tienes nada en el carrito", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btSalir_Click(object sender, RoutedEventArgs e)
        {
            Salir();
        }

        private void Salir()
        {
            var dialogResult = MessageBox.Show("¿Estás seguro de que quieres salir?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                hiloActualizarReloj.Abort();
                hiloCheckMantenimiento.Abort();
                Close();
            }
        }

        #endregion

        #region PANEL TIENDA

        private void rbChico_Checked(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(idCategoria))
            {
                ActualizarContexto();
                CargarArticulos(null);
            }
        }

        private void rbChica_Checked(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(idCategoria))
            {
                ActualizarContexto();
                CargarArticulos(null);
            }
        }

        private void btCategoria_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            idCategoria = ((Button)sender).Name.ToString().Split('_')[1];

            if (panelDetalleArticulo.IsVisible)
            {
                panelDetalleArticulo.Visibility = Visibility.Collapsed;
                wpBotonesArticulosVenta.Visibility = Visibility.Visible;
            }

            CargarArticulos(null);
        }

        private void btArticulo_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            articuloId = ((Button)sender).Name.ToString().Split('_')[1];
            Articulo a = uof.ArticuloRepository.MultiGet(c => c.ArticuloId.ToString().Equals(articuloId)).FirstOrDefault();

            panelCategorias.Visibility = Visibility.Collapsed;
            split.Visibility = Visibility.Collapsed;
            wpBotonesArticulosVenta.Visibility = Visibility.Collapsed;

            lbNombreArticulo.Content = a.nombre;
            tbDescripcionArticuloVenta.Text = a.descripcion;
            imagenArticulo.Source = ConvertArrayByteToImage(a.imagen);

            cargarDetallesArticulo();

            panelDetalleArticulo.Visibility = Visibility.Visible;
        }

        private void btDetalleArticulo_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            string talla = ((Button)sender).Name.ToString().Split('_')[1];
            DetalleArticulo da = uof.DetalleArticuloRepository.MultiGet(c => (c.talla + c.ArticuloId).Equals(talla)).FirstOrDefault();

            if (uof.DetalleVentaRepository.CheckStockDetalleVentaDetalleArticuloCarrito(detallesVenta, da))
            {
                detallesVenta.Add(new DetalleVenta(idDetalleVenta++, da));
                MessageBox.Show("Artículo añadido al carrito con éxito", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("No queda más stock para esta talla", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btAtras_Click(object sender, RoutedEventArgs e)
        {
            panelCategorias.Visibility = Visibility.Collapsed;
            split.Visibility = Visibility.Collapsed;
            if (panelDetalleArticulo.IsVisible)
            {
                panelDetalleArticulo.Visibility = Visibility.Collapsed;
                wpBotonesArticulosVenta.Visibility = Visibility.Visible;
            }
        }

        private void btRefrescar_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();
            cargarDetallesArticulo();
        }

        private void btFinalizarPagar_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            if (detallesVenta.Count > 0)
            {
                if (cliente != null)
                {
                    if (CheckStockDetalleVentaDetalleArticulo())
                    {
                        vc = new VentanaCarrito(this, true);
                        vc.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Se ha vaciado el carrito debido a que no queda stock para ninguno de los artículos añadidos", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    vlr = new VentanaLoginRegistro(this, null, true);
                    vlr.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("No tienes nada en el carrito", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void CargarCategorias()
        {
            panelBotonesCategorias.Children.Clear();            

            foreach (Categoria c in uof.CategoriaRepository.MultiGet(c => c.CategoriaId != 1).ToList())
            {
                WrapPanel dp = new WrapPanel();

                Label lbNombre = new Label();
                lbNombre.HorizontalAlignment = HorizontalAlignment.Center;
                lbNombre.VerticalAlignment = VerticalAlignment.Center;
                lbNombre.Content = c.nombre;
                lbNombre.FontSize = 14;
                lbNombre.FontWeight = FontWeights.SemiBold;
                lbNombre.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#BABABA"));
                lbNombre.Margin = new Thickness(5);

                dp.Children.Add(lbNombre);

                Button bt = new Button();
                bt.Name = "btCategoria_" + c.CategoriaId;
                bt.ToolTip = c.descripcion;
                bt.Content = dp;
                bt.Cursor = Cursors.Hand;
                bt.Width = 175;
                bt.Height = 50;
                bt.Margin = new Thickness(5);
                bt.Click += btCategoria_Click;

                panelBotonesCategorias.Children.Add(bt);
            }
        }

        private void CargarArticulos(string busqueda)
        {
            wpBotonesArticulosVenta.Children.Clear();

            Expression<Func<Articulo, bool>> lambda;
            if ("0".Equals(idCategoria))
            {
                if (String.IsNullOrEmpty(busqueda))
                {
                    lambda = c => c.estado.Equals(HABILITADO) && c.precioVenta > 0.0 && c.CategoriaId != 1;
                }
                else
                {
                    lambda = c => c.estado.Equals(HABILITADO) && c.precioVenta > 0.0 && c.CategoriaId != 1 && c.nombre.Contains(busqueda);
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(busqueda))
                {
                    if (rbChico.IsChecked == true)
                    {
                        lambda = c => c.CategoriaId.ToString().Equals(idCategoria) && c.estado.Equals(HABILITADO) && c.sexo.Equals("CHICO") && c.nombre.Contains(busqueda) && c.precioVenta > 0.0;
                    }
                    else
                    {
                        lambda = c => c.CategoriaId.ToString().Equals(idCategoria) && c.estado.Equals(HABILITADO) && c.sexo.Equals("CHICA") && c.nombre.Contains(busqueda) && c.precioVenta > 0.0;
                    }
                }
                else
                {
                    if (rbChico.IsChecked == true)
                    {
                        lambda = c => c.CategoriaId.ToString().Equals(idCategoria) && c.estado.Equals(HABILITADO) && c.sexo.Equals("CHICO") && c.precioVenta > 0.0;
                    }
                    else
                    {
                        lambda = c => c.CategoriaId.ToString().Equals(idCategoria) && c.estado.Equals(HABILITADO) && c.sexo.Equals("CHICA") && c.precioVenta > 0.0;
                    }
                }
            }

            foreach (Articulo a in uof.ArticuloRepository.MultiGet(lambda).ToList())
            {
                DockPanel dp = new DockPanel();
                dp.LastChildFill = true;

                StackPanel sp = new StackPanel();

                Label lbNombre = new Label();
                lbNombre.HorizontalAlignment = HorizontalAlignment.Center;
                lbNombre.Content = a.nombre;
                lbNombre.FontSize = 14;
                lbNombre.FontWeight = FontWeights.SemiBold;
                lbNombre.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#606060"));

                Label lbPrecio = new Label();
                lbPrecio.HorizontalAlignment = HorizontalAlignment.Center;
                lbPrecio.Content = a.precioVenta + "€";
                lbPrecio.FontSize = 12;
                lbPrecio.FontWeight = FontWeights.Normal;
                lbPrecio.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#606060"));

                sp.Children.Add(lbNombre);
                sp.Children.Add(lbPrecio);

                Image img = new Image();
                try
                {
                    img.Source = ConvertArrayByteToImage(a.imagen);
                    img.Margin = new Thickness(5);
                }
                catch (Exception ex) { Debug.WriteLine(String.Format("Error al cargar la imagen del artíuclo - {0}", ex.Message)); }
                img.Width = 200;
                img.Height = 300;

                DockPanel.SetDock(sp, Dock.Bottom);
                dp.Children.Add(sp);
                dp.Children.Add(img);

                Button bt = new Button();
                if (a.stockTotal == 0)
                {
                    bt.IsEnabled = false;
                }
                bt.Background = Brushes.Transparent;
                bt.BorderThickness = new Thickness(0);
                bt.Name = "btArticulo_" + a.ArticuloId;
                bt.Content = dp;
                bt.Cursor = Cursors.Hand;
                bt.Width = 250;
                bt.Height = 325;
                bt.Margin = new Thickness(5);
                bt.Click += btArticulo_Click;

                wpBotonesArticulosVenta.Children.Add(bt);
            }
        }

        private void cargarDetallesArticulo()
        {
            wpBotonesDetallesArticuloVenta.Children.Clear();

            foreach (DetalleArticulo da in uof.DetalleArticuloRepository.MultiGet(c => c.ArticuloId.ToString().Equals(articuloId) && c.estado.Equals(HABILITADO)).ToList())
            {
                Button bt = new Button();
                if (da.stock == 0)
                {
                    bt.IsEnabled = false;
                }
                bt.Name = "btTalla_" + da.talla + da.ArticuloId;
                bt.Content = da.talla.ToUpper();
                bt.Cursor = Cursors.Hand;
                bt.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#BABABA"));
                bt.Width = 50;
                bt.Height = 50;
                bt.Margin = new Thickness(5);
                bt.Click += btDetalleArticulo_Click;

                wpBotonesDetallesArticuloVenta.Children.Add(bt);
            }
        }

        public bool CheckStockDetalleVentaDetalleArticulo()
        {
            int stock, eliminados = 0;
            bool check = false;
            HashSet<DetalleArticulo> listaDetalleArticulos = new HashSet<DetalleArticulo>();

            foreach (DetalleVenta dv in detallesVenta)
            {
                listaDetalleArticulos.Add(dv.detalleArticulo);
            }

            foreach (DetalleArticulo da in listaDetalleArticulos)
            {
                stock = 0;

                foreach (DetalleVenta dv in detallesVenta)
                {
                    if ((dv.detalleArticulo.talla + dv.detalleArticulo.ArticuloId).Equals(da.talla + da.ArticuloId))
                    {
                        stock++;
                    }
                }
                if (stock > da.stock)
                {
                    stock = stock - da.stock;
                    eliminados += stock;
                    check = true;

                    foreach (DetalleVenta dv in detallesVenta)
                    {
                        if ((dv.detalleArticulo.talla + dv.detalleArticulo.ArticuloId).Equals(da.talla + da.ArticuloId))
                        {
                            detallesVenta.Remove(dv);

                            stock--;

                            if (stock == 0)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            if (check && detallesVenta.Count > 0)
            {
                MessageBox.Show(String.Format("Se han quitado {0} artículos, debido a que no queda stock", eliminados), "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            return (detallesVenta.Count > 0) ? true : false;
        }

        #endregion

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Salir();
            }
            else if (panelDetalleArticulo.IsVisible)
            {
                if (e.Key == Key.Back)
                {
                    panelCategorias.Visibility = Visibility.Collapsed;
                    split.Visibility = Visibility.Collapsed;
                    panelDetalleArticulo.Visibility = Visibility.Collapsed;
                    wpBotonesArticulosVenta.Visibility = Visibility.Visible;
                }
                else if (e.Key == Key.F5)
                {
                    ActualizarContexto();
                    cargarDetallesArticulo();
                }
            }
        }

    }
}
