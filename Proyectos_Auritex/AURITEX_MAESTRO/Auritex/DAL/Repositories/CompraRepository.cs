﻿using Auritex.Model;

namespace Auritex.DAL.Repositories
{
    public class CompraRepository : GenericRepository<Compra>
    {
        public CompraRepository(AuritexContext context) : base(context) { }

        public void AddCompra(Compra compra)
        {
            foreach (DetalleCompra dc in compra.detallesCompra)
            {
                dc.detalleArticulo.unidadesEnPedido += dc.unidades;
            }

            Add(compra);
        }

    }
}
