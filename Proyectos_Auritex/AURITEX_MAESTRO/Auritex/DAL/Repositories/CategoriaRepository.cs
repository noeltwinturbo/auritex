﻿using Auritex.Model;
using System.Linq;

namespace Auritex.DAL.Repositories
{
    public class CategoriaRepository : GenericRepository<Categoria>
    {

        public CategoriaRepository(AuritexContext context) : base(context) { }

        public int RemoveCategoria(Categoria categoria)
        {
            int afectados = categoria.articulos.Count();

            foreach (Articulo a in categoria.articulos)
            {
                a.CategoriaId = 1;
            }

            Remove(categoria);

            return afectados;
        }

    }
}
