﻿using Auritex.Model;

namespace Auritex.DAL.Repositories
{
    public class DetalleArticuloRepository : GenericRepository<DetalleArticulo>
    {

        public DetalleArticuloRepository(AuritexContext context) : base(context) { }

        public bool CheckExistsDetalleArticulo(string talla, Articulo articulo)
        {
            foreach (DetalleArticulo da in articulo.detallesArticulo)
            {
                if (da.talla.ToUpper().Equals(talla.ToUpper()))
                {
                    return false;
                }
            }

            return true;
        }

    }
}
