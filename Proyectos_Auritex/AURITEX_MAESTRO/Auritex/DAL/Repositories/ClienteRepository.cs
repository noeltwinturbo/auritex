﻿using Auritex.Model;

namespace Auritex.DAL.Repositories
{
    public class ClienteRepository : GenericRepository<Cliente>
    {
        public ClienteRepository(AuritexContext context) : base(context) { }
    }
}
