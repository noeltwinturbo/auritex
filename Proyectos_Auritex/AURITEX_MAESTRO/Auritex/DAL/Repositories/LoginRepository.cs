﻿using Auritex.Model;

namespace Auritex.DAL.Repositories
{
    public class LoginRepository : GenericRepository<Login>
    {
        public LoginRepository(AuritexContext context) : base(context) { }
    }
}
