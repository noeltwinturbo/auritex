﻿using Auritex.Model;
using System.Collections.Generic;
using System.Linq;

namespace Auritex.DAL.Repositories
{
    public class ArticuloRepository : GenericRepository<Articulo>
    {

        public ArticuloRepository(AuritexContext context) : base(context) { }

        public int DisableArticulo(Articulo articulo)
        {
            int afectados = articulo.detallesArticulo.Count();

            foreach (DetalleArticulo da in articulo.detallesArticulo)
            {
                da.estado = MainWindow.DESHABILITADO;
            }

            articulo.estado = MainWindow.DESHABILITADO;
            Modify(articulo);

            return afectados;
        }

        public HashSet<Articulo> GetArticulosTalla(string talla)
        {
            HashSet<Articulo> listaArticulos = new HashSet<Articulo>();

            foreach (Articulo a in MultiGet())
            {
                foreach (DetalleArticulo da in a.detallesArticulo)
                {
                    if (da.talla.Equals(talla))
                    {
                        listaArticulos.Add(a);
                        break;
                    }
                }
            }

            return listaArticulos;
        }

    }
}
