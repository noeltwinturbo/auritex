﻿using Auritex.Model;

namespace Auritex.DAL.Repositories
{
    public class TurnoRepository : GenericRepository<Turno>
    {
        public TurnoRepository(AuritexContext context) : base(context) { }
    }
}
