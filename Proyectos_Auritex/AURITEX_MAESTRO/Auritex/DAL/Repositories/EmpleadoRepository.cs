﻿using Auritex.Model;
using System.Collections.Generic;

namespace Auritex.DAL.Repositories
{
    public class EmpleadoRepository : GenericRepository<Empleado>
    {

        public EmpleadoRepository(AuritexContext context) : base(context) { }

        public HashSet<Empleado> GetEmpleadosTurnos(bool check, string busqueda)
        {
            HashSet<Empleado> listaEmpleados = new HashSet<Empleado>();

            foreach (Empleado e in MultiGet())
            {
                foreach(Turno t in e.turnos)
                {
                    if (check && t.diaSemana.Equals(busqueda))
                    {
                        listaEmpleados.Add(e);
                    }
                    else if (!check && t.turno.Equals(busqueda))
                    {
                        listaEmpleados.Add(e);
                    }
                }
            }

            return listaEmpleados;
        }

    }
}
