﻿using Auritex.Model;

namespace Auritex.DAL.Repositories
{
    public class VentaRepository : GenericRepository<Venta>
    {

        public VentaRepository(AuritexContext context) : base(context) { }

        public void AddVenta(Venta venta)
        {
            foreach (DetalleVenta dv in venta.detallesVenta)
            {
                dv.detalleArticulo.stock--;
            }

            Add(venta);
        }

    }
}
