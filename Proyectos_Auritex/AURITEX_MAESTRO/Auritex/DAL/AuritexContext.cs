﻿using Auritex.Migrations;
using Auritex.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Auritex.DAL
{
    public class AuritexContext : DbContext
    {

        public AuritexContext() : base("Auritex")
        {
            if (Database.Exists())
            {
                Database.SetInitializer(new MigrateDatabaseToLatestVersion<AuritexContext, Configuration>());
            }
            else
            {
                Database.SetInitializer(new creardb());
            }
        }

        public DbSet<Proveedor> Proveedor { get; set; }
        public DbSet<Compra> Compra { get; set; }
        public DbSet<DetalleCompra> DetalleCompra { get; set; }
        public DbSet<Articulo> Articulo { get; set; }
        public DbSet<Categoria> Categoria { get; set; }
        public DbSet<DetalleArticulo> DetalleArticulo { get; set; }
        public DbSet<DetalleVenta> DetalleVenta { get; set; }
        public DbSet<Venta> Venta { get; set; }
        public DbSet<Empleado> Empleado { get; set; }
        public DbSet<Turno> Turno { get; set; }
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Configuracion> Configuracion { get; set; }
        public DbSet<Login> Login { get; set; }
        public DbSet<Geografia> Geografia { get; set; }

        class creardb : CreateDatabaseIfNotExists<AuritexContext>
        {
            protected override void Seed(AuritexContext context)
            {
                HashSet<Turno> turnos = new HashSet<Turno>()
                {
                    new Turno("lunes", "mañana"),
                    new Turno("lunes", "tarde"),
                    new Turno("martes", "mañana"),
                    new Turno("martes", "tarde"),
                    new Turno("miércoles", "mañana"),
                    new Turno("miércoles", "tarde"),
                    new Turno("jueves", "mañana"),
                    new Turno("jueves", "tarde"),
                    new Turno("viernes", "mañana"),
                    new Turno("viernes", "tarde"),
                    new Turno("sábado", "mañana"),
                    new Turno("sábado", "tarde")
                };
                foreach (Turno t in turnos)
                {
                    context.Turno.AddOrUpdate(t);
                }

                context.Empleado.AddOrUpdate(new Empleado("ADMIN", new HashSet<Turno>()));
                context.Cliente.AddOrUpdate(new Cliente("CAJA"));
                context.Categoria.AddOrUpdate(new Categoria("DEFAULT"));
                context.Configuracion.AddOrUpdate(new Configuracion("MANTENIMIENTO", false), new Configuracion("AUTOREABASTECIMIENTO", false));

                string pathListaPaises = Path.Combine(Path.Combine(Environment.CurrentDirectory, "Listas", "listaPaisesEuropa.txt"));
                string pathListaCiudades = Path.Combine(Path.Combine(Environment.CurrentDirectory, "Listas", "listaCiudadesEspana.txt"));

                if (File.Exists(pathListaPaises))
                {
                    StreamReader entrada = new StreamReader(pathListaPaises, Encoding.Default, true);
                    while (!entrada.EndOfStream)
                    {
                        context.Geografia.AddOrUpdate(new Geografia(entrada.ReadLine(), "PAIS"));
                    }
                    entrada.Close();
                }
                else
                {
                    Debug.WriteLine("Error al cargar la lista de paises, no existe el fichero");
                }

                if (File.Exists(pathListaCiudades))
                {
                    StreamReader entrada = new StreamReader(pathListaCiudades, Encoding.Default, true);
                    while (!entrada.EndOfStream)
                    {
                        context.Geografia.AddOrUpdate(new Geografia(entrada.ReadLine(), "PROVINCIA"));
                    }
                    entrada.Close();
                }
                else
                {
                    Debug.WriteLine("Error al cargar la lista de provincias, no existe el fichero");
                }
            }
        }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }
}
