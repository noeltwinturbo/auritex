namespace Auritex.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Base : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Articulo",
                c => new
                    {
                        ArticuloId = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 100),
                        sexo = c.String(nullable: false),
                        precioCompra = c.Single(nullable: false),
                        precioVenta = c.Single(nullable: false),
                        nivelNuevoPedido = c.Int(nullable: false),
                        nivelMaximoPedido = c.Int(nullable: false),
                        descripcion = c.String(maxLength: 500),
                        imagen = c.Binary(),
                        estado = c.String(nullable: false),
                        ProveedorId = c.Int(nullable: false),
                        CategoriaId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ArticuloId)
                .ForeignKey("dbo.Categoria", t => t.CategoriaId, cascadeDelete: true)
                .ForeignKey("dbo.Proveedor", t => t.ProveedorId, cascadeDelete: true)
                .Index(t => t.ProveedorId)
                .Index(t => t.CategoriaId);
            
            CreateTable(
                "dbo.Categoria",
                c => new
                    {
                        CategoriaId = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 100),
                        descripcion = c.String(maxLength: 500),
                        imagen = c.Binary(),
                    })
                .PrimaryKey(t => t.CategoriaId);
            
            CreateTable(
                "dbo.DetalleArticulo",
                c => new
                    {
                        talla = c.String(nullable: false, maxLength: 10),
                        ArticuloId = c.Int(nullable: false),
                        stock = c.Int(nullable: false),
                        unidadesEnPedido = c.Int(nullable: false),
                        estado = c.String(nullable: false),
                    })
                .PrimaryKey(t => new { t.talla, t.ArticuloId })
                .ForeignKey("dbo.Articulo", t => t.ArticuloId, cascadeDelete: true)
                .Index(t => t.ArticuloId);
            
            CreateTable(
                "dbo.DetalleCompra",
                c => new
                    {
                        DetalleCompraId = c.Int(nullable: false),
                        CompraId = c.Int(nullable: false),
                        unidades = c.Int(nullable: false),
                        precio = c.Single(nullable: false),
                        talla = c.String(maxLength: 10),
                        ArticuloId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DetalleCompraId, t.CompraId })
                .ForeignKey("dbo.Compra", t => t.CompraId, cascadeDelete: true)
                .ForeignKey("dbo.DetalleArticulo", t => new { t.talla, t.ArticuloId })
                .Index(t => t.CompraId)
                .Index(t => new { t.talla, t.ArticuloId });
            
            CreateTable(
                "dbo.Compra",
                c => new
                    {
                        CompraId = c.Int(nullable: false, identity: true),
                        fecha = c.DateTime(nullable: false),
                        estado = c.String(nullable: false),
                        ProveedorId = c.Int(nullable: false),
                        EmpleadoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CompraId)
                .ForeignKey("dbo.Empleado", t => t.EmpleadoId, cascadeDelete: true)
                .ForeignKey("dbo.Proveedor", t => t.ProveedorId, cascadeDelete: true)
                .Index(t => t.ProveedorId)
                .Index(t => t.EmpleadoId);
            
            CreateTable(
                "dbo.Empleado",
                c => new
                    {
                        EmpleadoId = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 50),
                        apellidos = c.String(nullable: false, maxLength: 150),
                        dni = c.String(nullable: false),
                        telefono = c.String(nullable: false),
                        email = c.String(maxLength: 75),
                        direccion = c.String(maxLength: 150),
                        usuario = c.String(nullable: false, maxLength: 25),
                        contraseña = c.String(nullable: false, maxLength: 25),
                        cargo = c.String(nullable: false),
                        fechaNacimiento = c.DateTime(nullable: false),
                        fechaContratacion = c.DateTime(nullable: false),
                        imagen = c.Binary(),
                        estado = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.EmpleadoId);
            
            CreateTable(
                "dbo.Login",
                c => new
                    {
                        fechaEntrada = c.DateTime(nullable: false),
                        EmpleadoId = c.Int(nullable: false),
                        fechaSalida = c.DateTime(),
                    })
                .PrimaryKey(t => new { t.fechaEntrada, t.EmpleadoId })
                .ForeignKey("dbo.Empleado", t => t.EmpleadoId, cascadeDelete: true)
                .Index(t => t.EmpleadoId);
            
            CreateTable(
                "dbo.Turno",
                c => new
                    {
                        diaSemana = c.String(nullable: false, maxLength: 128),
                        turno = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.diaSemana, t.turno });
            
            CreateTable(
                "dbo.Venta",
                c => new
                    {
                        VentaId = c.Int(nullable: false, identity: true),
                        fecha = c.DateTime(nullable: false),
                        ciudadEnvio = c.String(maxLength: 100),
                        codPostalEnvio = c.String(),
                        direccionEnvio = c.String(maxLength: 150),
                        formaPago = c.String(nullable: false),
                        descuento = c.Single(nullable: false),
                        EmpleadoId = c.Int(nullable: false),
                        ClienteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.VentaId)
                .ForeignKey("dbo.Cliente", t => t.ClienteId, cascadeDelete: true)
                .ForeignKey("dbo.Empleado", t => t.EmpleadoId, cascadeDelete: true)
                .Index(t => t.EmpleadoId)
                .Index(t => t.ClienteId);
            
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        ClienteId = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 50),
                        apellidos = c.String(nullable: false, maxLength: 150),
                        fechaNacimiento = c.DateTime(),
                        dni = c.String(),
                        telefono = c.String(),
                        email = c.String(nullable: false, maxLength: 75),
                        contraseña = c.String(nullable: false, maxLength: 25),
                        ciudad = c.String(maxLength: 100),
                        codPostal = c.String(),
                        direccion = c.String(maxLength: 150),
                        estado = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ClienteId);
            
            CreateTable(
                "dbo.DetalleVenta",
                c => new
                    {
                        DetalleVentaId = c.Int(nullable: false),
                        VentaId = c.Int(nullable: false),
                        precio = c.Single(nullable: false),
                        estado = c.String(nullable: false),
                        talla = c.String(maxLength: 10),
                        ArticuloId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DetalleVentaId, t.VentaId })
                .ForeignKey("dbo.DetalleArticulo", t => new { t.talla, t.ArticuloId })
                .ForeignKey("dbo.Venta", t => t.VentaId, cascadeDelete: true)
                .Index(t => t.VentaId)
                .Index(t => new { t.talla, t.ArticuloId });
            
            CreateTable(
                "dbo.Proveedor",
                c => new
                    {
                        ProveedorId = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 50),
                        telefono = c.String(nullable: false, maxLength: 12),
                        email = c.String(maxLength: 75),
                        pais = c.String(maxLength: 75),
                        ciudad = c.String(maxLength: 100),
                        codPostal = c.String(maxLength: 10),
                        direccion = c.String(maxLength: 150),
                        imagen = c.Binary(),
                        estado = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ProveedorId);
            
            CreateTable(
                "dbo.Configuracion",
                c => new
                    {
                        nombre = c.String(nullable: false, maxLength: 100),
                        estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.nombre);
            
            CreateTable(
                "dbo.Geografia",
                c => new
                    {
                        GeografiaId = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false, maxLength: 100),
                        tipo = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.GeografiaId);
            
            CreateTable(
                "dbo.TurnoEmpleadoes",
                c => new
                    {
                        Turno_diaSemana = c.String(nullable: false, maxLength: 128),
                        Turno_turno = c.String(nullable: false, maxLength: 128),
                        Empleado_EmpleadoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Turno_diaSemana, t.Turno_turno, t.Empleado_EmpleadoId })
                .ForeignKey("dbo.Turno", t => new { t.Turno_diaSemana, t.Turno_turno }, cascadeDelete: true)
                .ForeignKey("dbo.Empleado", t => t.Empleado_EmpleadoId, cascadeDelete: true)
                .Index(t => new { t.Turno_diaSemana, t.Turno_turno })
                .Index(t => t.Empleado_EmpleadoId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DetalleCompra", new[] { "talla", "ArticuloId" }, "dbo.DetalleArticulo");
            DropForeignKey("dbo.Compra", "ProveedorId", "dbo.Proveedor");
            DropForeignKey("dbo.Articulo", "ProveedorId", "dbo.Proveedor");
            DropForeignKey("dbo.Venta", "EmpleadoId", "dbo.Empleado");
            DropForeignKey("dbo.DetalleVenta", "VentaId", "dbo.Venta");
            DropForeignKey("dbo.DetalleVenta", new[] { "talla", "ArticuloId" }, "dbo.DetalleArticulo");
            DropForeignKey("dbo.Venta", "ClienteId", "dbo.Cliente");
            DropForeignKey("dbo.TurnoEmpleadoes", "Empleado_EmpleadoId", "dbo.Empleado");
            DropForeignKey("dbo.TurnoEmpleadoes", new[] { "Turno_diaSemana", "Turno_turno" }, "dbo.Turno");
            DropForeignKey("dbo.Login", "EmpleadoId", "dbo.Empleado");
            DropForeignKey("dbo.Compra", "EmpleadoId", "dbo.Empleado");
            DropForeignKey("dbo.DetalleCompra", "CompraId", "dbo.Compra");
            DropForeignKey("dbo.DetalleArticulo", "ArticuloId", "dbo.Articulo");
            DropForeignKey("dbo.Articulo", "CategoriaId", "dbo.Categoria");
            DropIndex("dbo.TurnoEmpleadoes", new[] { "Empleado_EmpleadoId" });
            DropIndex("dbo.TurnoEmpleadoes", new[] { "Turno_diaSemana", "Turno_turno" });
            DropIndex("dbo.DetalleVenta", new[] { "talla", "ArticuloId" });
            DropIndex("dbo.DetalleVenta", new[] { "VentaId" });
            DropIndex("dbo.Venta", new[] { "ClienteId" });
            DropIndex("dbo.Venta", new[] { "EmpleadoId" });
            DropIndex("dbo.Login", new[] { "EmpleadoId" });
            DropIndex("dbo.Compra", new[] { "EmpleadoId" });
            DropIndex("dbo.Compra", new[] { "ProveedorId" });
            DropIndex("dbo.DetalleCompra", new[] { "talla", "ArticuloId" });
            DropIndex("dbo.DetalleCompra", new[] { "CompraId" });
            DropIndex("dbo.DetalleArticulo", new[] { "ArticuloId" });
            DropIndex("dbo.Articulo", new[] { "CategoriaId" });
            DropIndex("dbo.Articulo", new[] { "ProveedorId" });
            DropTable("dbo.TurnoEmpleadoes");
            DropTable("dbo.Geografia");
            DropTable("dbo.Configuracion");
            DropTable("dbo.Proveedor");
            DropTable("dbo.DetalleVenta");
            DropTable("dbo.Cliente");
            DropTable("dbo.Venta");
            DropTable("dbo.Turno");
            DropTable("dbo.Login");
            DropTable("dbo.Empleado");
            DropTable("dbo.Compra");
            DropTable("dbo.DetalleCompra");
            DropTable("dbo.DetalleArticulo");
            DropTable("dbo.Categoria");
            DropTable("dbo.Articulo");
        }
    }
}
