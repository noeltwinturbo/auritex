﻿#region USING

using Auritex.DAL;
using Auritex.Model;
using Auritex.Windows;
using Auritex.Windows.InfoWindows;
using Auritex.Windows.ShoppingWindows;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

#endregion

namespace Auritex
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        //***TRABAJO NOEL SEARA RODRIGUEZ DAM221***//

        #region VARIABLES

        private UnitOfWork uof;
        private Categoria categoria;
        private Articulo articulo;
        private Proveedor proveedor;
        private Compra compra;
        private HashSet<DetalleCompra> detallesCompra;
        private Cliente cliente;
        private Empleado empleado;
        private Venta venta;
        private HashSet<DetalleVenta> detallesVenta;
        private Login login;

        private string idCategoria;
        private int idDetalleVenta;
        private int idDetalleCompra;
        private int tabActual = 0;
        private bool privilegios = true;

        public static readonly string AUTOREABASTECIMIENTO = "AUTOREABASTECIMIENTO";
        public static readonly string MANTENIMIENTO = "MANTENIMIENTO";
        public static readonly string HABILITADO = "HABILITADO";
        public static readonly string DESHABILITADO = "DESHABILITADO";
        public static readonly string EMAIL_AURITEX = "auritex@gmx.es";

        #region HTML CORREOS

        public static readonly string HTML_ARTICULO_DEVUELTO =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">DEVOLUCIÓN</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Se ha devuelto el siguiente artículo:</h3>"
            + "<h3 style=\"padding-left: 60px;\">Artículo: {0}</h3>"
            + "<h3 style=\"padding-left: 60px;\">Talla: {1}</h3>"
            + "<h3 style=\"padding-left: 60px;\">Sexo: {2}</h3>"
            + "<h3 style=\"padding-left: 60px;\">Precio: {3}€</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_COMPRA_REALIZADA =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">COMPRA Nº{0}</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Se ha realizado con éxito una compra de {1} artículos, por un importe total de {2}€.</h3>"
            + "<h3 style=\"padding-left: 60px;\">Empleado: {3}</h3>"
            + "<h3 style=\"padding-left: 60px;\">ID empleado: {4}</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_COMPRA_CANCELADA =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">COMPRA Nº{0}</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Se ha cancelado con éxito una compra de {1} artículos, por un importe total de {2}€.</h3>"
            + "<h3 style=\"padding-left: 60px;\">Empleado: {3}</h3>"
            + "<h3 style=\"padding-left: 60px;\">ID empleado: {4}</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_COMPRA_AUTOREABASTECIMIENTO_REALIZADA =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">COMPRA POR AUTOREABASTECIMIENTO Nº{0}</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Se ha realizado con éxito una compra de {1} artículos, por un importe total de {2}€.</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_VENTA_REALIZADA =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">VENTA Nº{0}</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Se ha realizado con éxito una venta de {1} artículos, por un importe total de {2}€.</h3>"
            + "<h3 style=\"padding-left: 60px;\">Empleado: {3}</h3>"
            + "<h3 style=\"padding-left: 60px;\">ID empleado: {4}</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_CLIENTE_REGISTRADO =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">NUEVO USUARIO REGISTRADO</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Se ha registrado un nuevo cliente con ID: {0}.</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_CLIENTE_ELIMINADO =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">CUENTA DE USUARIO ELIMINADA</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Se ha eliminado el cliente con ID: {0}.</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_CLIENTE_REGISTRADO_CLIENTE =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">{0} BIENVENIDO A AURITEX</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Nos alegramos mucho de que formes parte de nosotros. Esperamos que nuestros artículos y promociones te satisfagan.</h3>"
            + "<h3 style=\"padding-left: 30px;\">Tu contraseña es: {1}, cuando inicies sesión te recomendamos que la cambies.</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"padding-left: 30px;\">Para cualquier consulta, por favor, contacta con nuestro servicio de atención al cliente.</p>"
            + "<p style=\"padding-left: 30px;\">Atentamente, equipo de Auritex.</p>"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        public static readonly string HTML_CLIENTE_ELIMINADO_CLIENTE =
            "<body>"
            + "<br />"
            + "<h1 style=\"text-align: center;\">CUENTA DE USUARIO ELIMINADA</h1>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<h3 style=\"padding-left: 30px;\">Hola {0}, lamentamos mucho que te tengas que ir, esperamos volver a verte muy pronto.</h3>"
            + "<br />"
            + "<hr />"
            + "<br />"
            + "<p style=\"padding-left: 30px;\">Para cualquier consulta, por favor, contacta con nuestro servicio de atención al cliente.</p>"
            + "<p style=\"padding-left: 30px;\">Atentamente, equipo de Auritex.</p>"
            + "<br />"
            + "<p style=\"text-align: center;\">Ubicación: Ourense, 32002</p>"
            + "<p style=\"text-align: center;\">Correo: auritex@gmx.es</p>"
            + "<br />"
            + "</body>";

        #endregion

        #endregion

        public MainWindow()
        {
            InitializeComponent();

            uof = new UnitOfWork();

            CargarElementos();
            CheckEstadoConfiguraciones();
        }

        #region METODOS INICIO

        private void CargarElementos()
        {
            cbCampoBusquedaArticulo.Text = "nombre";
            cbCampoBusquedaProveedor.Text = "nombre";
            cbCampoBusquedaCategoria.Text = "nombre";
            cbCampoBusquedaEmpleado.Text = "nombre";
            cbCampoBusquedaCliente.Text = "nombre";
            cbCampoBusquedaHistorialVentas.Text = "empleado";
            cbCampoBusquedaHistorialCompras.Text = "empleado";
            cbMetodoBusquedaArticulo.Text = ">";
            cbMetodoBusquedaHistorialVentas.Text = ">";
            calendarioVentas.SelectedDate = DateTime.Today;
            calendarioCompras.SelectedDate = DateTime.Today;

            cbPaisProveedor.ItemsSource = uof.GeografiaRepository.GetGeografia("PAIS");
            cbCiudadCliente.ItemsSource = uof.GeografiaRepository.GetGeografia("PROVINCIA");
        }

        private void CheckEstadoConfiguraciones()
        {
            Configuracion configuracion = uof.ConfiguracionRepository.MultiGet(c => c.nombre.Equals(AUTOREABASTECIMIENTO)).FirstOrDefault();
            if (configuracion.estado)
            {
                iconoAutoreabastecimiento.Source = new BitmapImage(new Uri(@"Icons\checkIcon.png", UriKind.Relative));
            }

            configuracion = uof.ConfiguracionRepository.MultiGet(c => c.nombre.Equals(MANTENIMIENTO)).FirstOrDefault();
            if (configuracion.estado)
            {
                iconoMantenimiento.Source = new BitmapImage(new Uri(@"Icons\checkIcon.png", UriKind.Relative));
            }
        }

        #endregion

        #region METODOS LOGIN/LOGOUT

        private void panelLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Login();
            }
        }

        private void btLogin_Click(object sender, RoutedEventArgs e)
        {
            Login();
        }

        private void panelInterior_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Logout();
            }
        }

        private void btCerrarSesion_Click(object sender, RoutedEventArgs e)
        {
            Logout();
        }

        private void Login()
        {
            if (!String.IsNullOrEmpty(tbUsuarioLogin.Text) && !String.IsNullOrEmpty(pbContraseñaLogin.Password))
            {
                ActualizarContexto();

                if (uof.EmpleadoRepository.MultiGet(c => c.usuario.Equals(tbUsuarioLogin.Text) && c.contraseña.Equals(pbContraseñaLogin.Password) && c.estado.Equals(HABILITADO)).Count() == 1)
                {
                    if (CheckTurno() || tbUsuarioLogin.Text.ToUpper().Equals("ADMIN"))
                    {
                        login = new Login(DateTime.Now, uof.EmpleadoRepository.MultiGet(c => c.usuario.Equals(tbUsuarioLogin.Text) && c.estado.Equals(HABILITADO)).FirstOrDefault().EmpleadoId);

                        CheckPrivilegios();
                        CheckTPV();
                        CheckProveedores();

                        panelLogin.Visibility = Visibility.Hidden;
                        panelInterior.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        MessageBox.Show("No puedes entrar porque no tienes este turno asignado o estás fuera del horario laboral", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Login incorrecto", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            else
            {
                MessageBox.Show("Faltan campos por rellenar", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void Logout()
        {
            var dialogResult = MessageBox.Show("¿Estás seguro de que quieres cerrar sesión?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                Logout(true);
            }
        }

        private bool CheckTurno()
        {
            CultureInfo ci = new CultureInfo("Es-ES");
            List<Turno> listaTurnos = uof.EmpleadoRepository.MultiGet(c => c.usuario.Equals(tbUsuarioLogin.Text) && c.estado.Equals(HABILITADO)).FirstOrDefault().turnos.ToList();
            foreach (Turno t in listaTurnos)
            {
                if (ci.DateTimeFormat.GetDayName(DateTime.Today.DayOfWeek).ToUpper().Equals(t.diaSemana.ToUpper()))
                {
                    if (DateTime.Now.Hour >= 8 && DateTime.Now.Hour < 15 && t.turno.ToUpper().Equals("MAÑANA") 
                        || DateTime.Now.Hour >= 15 && DateTime.Now.Hour < 22 && t.turno.ToUpper().Equals("TARDE"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void Logout(bool check)
        {
            login.fechaSalida = DateTime.Now;
            uof.LoginRepository.Add(login);

            if (check)
            {
                tbUsuarioLogin.Clear();
                pbContraseñaLogin.Clear();
                panelLogin.Visibility = Visibility.Visible;
                panelInterior.Visibility = Visibility.Hidden;
                tabContenedor.SelectedIndex = 0;
            }
        }

        #endregion

        #region BOTONES MENU

        private void btMantenimiento_Click(object sender, RoutedEventArgs e)
        {
            Configuracion configuracion = uof.ConfiguracionRepository.MultiGet(c => c.nombre.Equals(MANTENIMIENTO)).FirstOrDefault();
            if (!configuracion.estado)
            {
                configuracion.estado = true;
                iconoMantenimiento.Source = new BitmapImage(new Uri(@"Icons\checkIcon.png", UriKind.Relative));
            }
            else
            {
                configuracion.estado = false;
                iconoMantenimiento.Source = null;
            }
            uof.ConfiguracionRepository.Modify(configuracion);
        }

        private void btAutoreabastecimiento_Click(object sender, RoutedEventArgs e)
        {
            Configuracion configuracion = uof.ConfiguracionRepository.MultiGet(c => c.nombre.Equals(AUTOREABASTECIMIENTO)).FirstOrDefault();
            if (!configuracion.estado)
            {
                configuracion.estado = true;
                iconoAutoreabastecimiento.Source = new BitmapImage(new Uri(@"Icons\checkIcon.png", UriKind.Relative));
            }
            else
            {
                configuracion.estado = false;
                iconoAutoreabastecimiento.Source = null;
            }
            uof.ConfiguracionRepository.Modify(configuracion);
        }

        #endregion

        #region METODOS VARIOS

        private void CheckPrivilegios()
        {
            if (uof.EmpleadoRepository.MultiGet(c => c.usuario.Equals(tbUsuarioLogin.Text) && c.cargo.Equals("JEFE") && c.estado.Equals(HABILITADO)).Count() == 0)
            {
                EstadoMantenimientos(false);
                privilegios = false;
            }
            else
            {
                privilegios = true;
                EstadoMantenimientos(true);
            }
            CheckProveedores();
        }

        private void EstadoMantenimientos(bool estado)
        {
            if (privilegios)
            {
                panelMantenimientoCategorias.IsEnabled = estado;
                panelMantenimientoClientes.IsEnabled = estado;
                panelMantenimientoEmpleados.IsEnabled = estado;
                panelMantenimientoTurnos.IsEnabled = estado;
                panelMantenimientoArticulos.IsEnabled = estado;
                panelMantenimientoDetallesArticulo.IsEnabled = estado;
                panelMantenimientoProveedores.IsEnabled = estado;
                panelMantenimientoTurnos.IsEnabled = estado;
                btConfiguracion.Visibility = Visibility.Visible;
                if (tbUsuarioLogin.Text.ToUpper().Equals("ADMIN"))
                {
                    panelGuia.Columns = 6;
                }
                else
                {
                    panelGuia.Columns = 8;
                }
            }
            else
            {
                btConfiguracion.Visibility = Visibility.Collapsed;
                panelGuia.Columns = 7;
            }
        }

        private void CheckTPV()
        {
            if (tbUsuarioLogin.Text.ToUpper().Equals("ADMIN"))
            {
                tabTPV.Visibility = Visibility.Collapsed;
            }
            else
            {
                tabTPV.Visibility = Visibility.Visible;
                LimpiarVenta();
            }
        }

        private void CheckProveedores()
        {
            if (uof.ProveedorRepository.MultiGet(c => c.estado.Equals(HABILITADO)).Count() > 0 && privilegios && !tbUsuarioLogin.Text.ToUpper().Equals("ADMIN"))
            {
                tabCompras.Visibility = Visibility.Visible;
                LimpiarCompra();
            }
            else
            {
                tabCompras.Visibility = Visibility.Collapsed;
            }
        }

        public BitmapImage SeleccionarImagen()
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer);
            fileDialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            if (fileDialog.ShowDialog() == true)
            {
                return new BitmapImage(new Uri(fileDialog.FileName));
            }
            else
            {
                return null;
            }
        }

        public BitmapImage ConvertArrayByteToImage(byte[] byteArrayIn)
        {
            try
            {
                MemoryStream stream = new MemoryStream();
                stream.Write(byteArrayIn, 0, byteArrayIn.Length);
                stream.Position = 0;
                System.Drawing.Image img = System.Drawing.Image.FromStream(stream);

                BitmapImage returnImage = new BitmapImage();
                returnImage.BeginInit();

                MemoryStream ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                ms.Seek(0, SeekOrigin.Begin);
                returnImage.StreamSource = ms;

                returnImage.EndInit();

                return returnImage;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error al convertir un array de bytes a BitmapImage - {0}", ex.Message));
                return null;
            }

        }

        public byte[] ConvertImageToArrayByte(BitmapImage image)
        {
            try
            {
                MemoryStream memStream = new MemoryStream();
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();

                encoder.Frames.Add(BitmapFrame.Create(image));
                encoder.Save(memStream);

                return memStream.ToArray();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error al convertir un BitmapImage a un array de bytes - {0}", ex.Message));
                return null;
            }
        }

        public void SendEmail(string correoCliente, string html)
        {
            MailMessage msg = new MailMessage(EMAIL_AURITEX, correoCliente, ".:Auritex:.", html);
            msg.IsBodyHtml = true;
            SmtpClient sc = new SmtpClient("mail.gmx.es", 587);
            NetworkCredential cre = new NetworkCredential(EMAIL_AURITEX, "Auritex123.");
            sc.Credentials = cre;
            sc.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

            try
            {
                sc.Send(msg);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error al enviar el correo para \"{0}\" - {1}", correoCliente, ex.Message));
                MessageBox.Show("No se ha podido enviar el correo", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        public void ActualizarContexto()
        {
            Cursor = Cursors.Wait;
            try
            {
                foreach (var entity in uof.context.ChangeTracker.Entries())
                {
                    entity.Reload();
                }

                Debug.WriteLine("Contexto actualizado");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(String.Format("Error actualizando el contexto - {0}", ex.Message));
            }
            Cursor = Cursors.Arrow;
        }

        public Boolean Validar(Object obj)
        {
            ValidationContext validationContext = new ValidationContext(obj, null, null);
            List<System.ComponentModel.DataAnnotations.ValidationResult> errors = new List<System.ComponentModel.DataAnnotations.ValidationResult>();
            Validator.TryValidateObject(obj, validationContext, errors, true);
            if (errors.Count() > 0)
            {
                string mensageErrores = string.Empty;
                foreach (var error in errors)
                {
                    error.MemberNames.First();
                    mensageErrores += error.ErrorMessage + Environment.NewLine;
                }
                MessageBox.Show(mensageErrores, "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation); return false;
            }
            else
            {
                return true;
            }
        }

        private void tb_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int ascci = Convert.ToInt32(Convert.ToChar(e.Text));
            if (ascci >= 48 && ascci <= 57)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (login != null && login.fechaSalida == null)
            {
                Logout(false);
            }
        }

        #endregion

        #region LISTENER CAMBIAR PESTAÑA

        private void tabContenedor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tabArticulos.IsSelected && tabContenedor.SelectedIndex != tabActual)
            {
                ActualizarContexto();

                cbCategoriaArticulo.ItemsSource = uof.CategoriaRepository.MultiGet();
                dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet();
                dgDetallesArticulo.ItemsSource = null;

                LimpiarArticulo();

                btNuevoArticulo.Visibility = Visibility.Visible;
                btCancelarArticulo.Visibility = Visibility.Collapsed;
                btGuardarArticulo.IsEnabled = false;
            }
            else if (tabTPV.IsSelected && tabContenedor.SelectedIndex != tabActual && detallesVenta.Count == 0)
            {
                ActualizarContexto();

                tabActual = tabContenedor.SelectedIndex;

                LimpiarVenta();
                CargarCategorias();
            }
            else if (tabCompras.IsSelected && tabContenedor.SelectedIndex != tabActual && detallesCompra.Count == 0)
            {
                ActualizarContexto();

                tabActual = tabContenedor.SelectedIndex;
                
                LimpiarCompra();
            }
            else if (tabHistoriales.IsSelected && tabContenedor.SelectedIndex != tabActual)
            {
                ActualizarContexto();

                DateTime fecha = Convert.ToDateTime(calendarioVentas.SelectedDate);

                dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.fecha.Day == fecha.Day && c.fecha.Month == fecha.Month && c.fecha.Year == fecha.Year);
                dgHistorialDetallesVenta.ItemsSource = null;
                CalcularResumenVentas();

                dgHistorialCompras.ItemsSource = uof.CompraRepository.MultiGet(c => c.fecha.Day == fecha.Day && c.fecha.Month == fecha.Month && c.fecha.Year == fecha.Year);
                dgHistorialDetallesCompra.ItemsSource = null;
                CalcularResumenCompras();

            }
            else if (tabCategorias.IsSelected && tabContenedor.SelectedIndex != tabActual)
            {
                ActualizarContexto();

                dgCategoria.ItemsSource = uof.CategoriaRepository.MultiGet();

                LimpiarCategoria();

                btNuevaCategoria.Visibility = Visibility.Visible;
                btCancelarCategoria.Visibility = Visibility.Collapsed;
                btGuardarCategoria.IsEnabled = false;
            }
            else if (tabProveedores.IsSelected && tabContenedor.SelectedIndex != tabActual)
            {
                ActualizarContexto();

                dgProveedor.ItemsSource = uof.ProveedorRepository.MultiGet();

                LimpiarProveedor();

                btNuevoProveedor.Visibility = Visibility.Visible;
                btCancelarProveedor.Visibility = Visibility.Collapsed;
                btGuardarProveedor.IsEnabled = false;
            }
            else if (tabEmpleados.IsSelected && tabContenedor.SelectedIndex != tabActual)
            {
                ActualizarContexto();

                dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet();
                dgTurnosEmpleado.ItemsSource = null;

                LimpiarEmpleado();

                btNuevoEmpleado.Visibility = Visibility.Visible;
                btCancelarEmpleado.Visibility = Visibility.Collapsed;
                btGuardarEmpleado.IsEnabled = false;
            }
            else if (tabClientes.IsSelected && tabContenedor.SelectedIndex != tabActual)
            {
                ActualizarContexto();

                dgCliente.ItemsSource = uof.ClienteRepository.MultiGet();

                LimpiarCliente();

                btNuevoCliente.Visibility = Visibility.Visible;
                btCancelarCliente.Visibility = Visibility.Collapsed;
                btGuardarCliente.IsEnabled = false;
            }
            tabActual = tabContenedor.SelectedIndex;
        }

        #endregion

        #region PANEL ARTICULOS

        private void btSeleccionarImagenArticulo_Click(object sender, RoutedEventArgs e)
        {
            BitmapImage bitmap = SeleccionarImagen();
            if (bitmap != null)
            {
                imagenArticulo.Source = bitmap;
                if (btNuevoArticulo.IsVisible)
                {
                    articulo.imagen = ConvertImageToArrayByte((BitmapImage)imagenArticulo.Source);
                    uof.ArticuloRepository.Modify(articulo);
                }
            }
        }

        private void btQuitarImagenArticulo_Click(object sender, RoutedEventArgs e)
        {
            imagenArticulo.Source = null;
            if (btNuevoArticulo.IsVisible)
            {
                articulo.imagen = null;
                uof.ArticuloRepository.Modify(articulo);
            }
        }

        private void btNuevoArticulo_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            LimpiarArticulo();

            btCancelarArticulo.Visibility = Visibility.Visible;
            btNuevoArticulo.Visibility = Visibility.Collapsed;
            btGuardarArticulo.IsEnabled = true;
            panelDatosArticulo.IsEnabled = true;
            panelBotonesImagenesArticulo.IsEnabled = true;
            panelBusquedaArticulos.IsEnabled = false;
            panelDatagridsArticulos.IsEnabled = false;

            cbProveedorArticulo.ItemsSource = uof.ProveedorRepository.MultiGet(c => c.estado.Equals(HABILITADO));
        }

        private void btCancelarArticulo_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            LimpiarArticulo();

            btCancelarArticulo.Visibility = Visibility.Collapsed;
            btNuevoArticulo.Visibility = Visibility.Visible;
            btGuardarArticulo.IsEnabled = false;
        }

        private void btGuardarArticulo_Click(object sender, RoutedEventArgs e)
        {
            if (cbProveedorArticulo.SelectedIndex >= 0 && cbCategoriaArticulo.SelectedIndex >= 0)
            {
                if (Validar(articulo))
                {
                    if (articulo.precioCompra > 0.0)
                    {
                        if ((articulo.nivelNuevoPedido > 0 && articulo.nivelMaximoPedido > articulo.nivelNuevoPedido)
                            || articulo.nivelNuevoPedido == 0)
                        {
                            articulo.imagen = ConvertImageToArrayByte((BitmapImage)imagenArticulo.Source);
                            uof.ArticuloRepository.Add(articulo);

                            dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet();
                            MostrarArticulo();

                            btCancelarArticulo.Visibility = Visibility.Collapsed;
                            btNuevoArticulo.Visibility = Visibility.Visible;
                            btGuardarArticulo.IsEnabled = false;
                            panelBusquedaArticulos.IsEnabled = true;
                            panelDatagridsArticulos.IsEnabled = true;

                            MessageBox.Show("Artículo añadido", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("El nivel máximo pedido no puede ser igual o menor que el nivel nuevo pedido", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("El precio de compra del artículo no puede ser 0€", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            else
            {
                MessageBox.Show("Categoría y/o proveedor no asignados", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btModificarArticulo_Click(object sender, RoutedEventArgs e)
        {
            if (Validar(articulo))
            {
                if (articulo.precioCompra > 0.0)
                {
                    if ((articulo.nivelNuevoPedido > 0 && articulo.nivelMaximoPedido > articulo.nivelNuevoPedido)
                    || articulo.nivelNuevoPedido == 0)
                    {
                        articulo.imagen = ConvertImageToArrayByte((BitmapImage)imagenArticulo.Source);
                        uof.ArticuloRepository.Modify(articulo);

                        dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet();

                        MessageBox.Show("Artículo modificado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("El nivel máximo pedido no puede ser igual o menor que el nivel nuevo pedido", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("El precio de compra del artículo no puede ser 0€", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        private void btEliminarArticulo_Click(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show("¿Estás seguro de que quieres eliminar el artículo y sus tallas?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                int afectados = uof.ArticuloRepository.DisableArticulo(articulo);

                dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet();
                LimpiarArticulo();

                MessageBox.Show("Artículo eliminado\nTallas afectadas: " + afectados, "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void cbCampoBusquedaArticulo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCampoBusquedaArticulo.SelectedItem.Equals("precio compra") 
                || cbCampoBusquedaArticulo.SelectedItem.Equals("precio venta")
                || cbCampoBusquedaArticulo.SelectedItem.Equals("nivel nuevo pedido"))
            {
                cbMetodoBusquedaArticulo.Visibility = Visibility.Visible;
            }
            else
            {
                cbMetodoBusquedaArticulo.Visibility = Visibility.Collapsed;
            }
        }

        private void tbCampoBusquedaArticulo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BuscarArticulo();
            }
        }

        private void btBuscarArticulo_Click(object sender, RoutedEventArgs e)
        {
            BuscarArticulo();
        }

        private void btVerTodosArticulos_Click(object sender, RoutedEventArgs e)
        {
            VerTodosArticulos();
        }

        private void dgArticulo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgArticulo.SelectedIndex >= 0)
            {
                articulo = (Articulo)dgArticulo.SelectedItem;
                MostrarArticulo();
            }
        }

        private void dgArticulo_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgArticulo.SelectedIndex >= 0)
            {
                ActualizarContexto();

                VentanaMultiInfo vmi = new VentanaMultiInfo(this, uof, 5, null, null, null, articulo.proveedor, articulo.categoria);
                vmi.ShowDialog();
            }
        }

        private void dgDetallesArticulo_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgDetallesArticulo.SelectedIndex >= 0)
            {
                ActualizarContexto();

                DetalleArticulo da = ((DetalleArticulo)dgDetallesArticulo.SelectedItem);
                int index = 0;

                if (privilegios && da.estado.Equals(HABILITADO))
                {
                    index = 2;
                }

                VentanaDetallesArticulo vda = new VentanaDetallesArticulo(this, uof, index, articulo, da);
                vda.ShowDialog();
            }
        }

        private void btAñadirDetalleArticulo_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            VentanaDetallesArticulo vda = new VentanaDetallesArticulo(this, uof, 1, articulo, null);
            vda.ShowDialog();
        }

        private void btEliminarDetalleArticulo_Click(object sender, RoutedEventArgs e)
        {
            DetalleArticulo da = ((DetalleArticulo)dgDetallesArticulo.SelectedItem);

            if (privilegios && da.estado.Equals(HABILITADO))
            {
                var dialogResult = MessageBox.Show("¿Estás seguro de que quieres eliminar la talla?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    da.estado = DESHABILITADO;
                    uof.ArticuloRepository.Modify(articulo);

                    dgDetallesArticulo.Items.Refresh();

                    MessageBox.Show("Talla eliminada", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                MessageBox.Show("La talla ya está eliminada", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void BuscarArticulo()
        {
            if (!String.IsNullOrEmpty(tbCampoBusquedaArticulo.Text))
            {
                ActualizarContexto();

                if (!cbMetodoBusquedaArticulo.IsVisible)
                {
                    switch (cbCampoBusquedaArticulo.Text)
                    {
                        case "nombre": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.nombre.Contains(tbCampoBusquedaArticulo.Text)); break;
                        case "sexo": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.sexo.Contains(tbCampoBusquedaArticulo.Text)); break;
                        case "proveedor": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.proveedor.nombre.Contains(tbCampoBusquedaArticulo.Text)); break;
                        case "categoría": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.categoria.nombre.Contains(tbCampoBusquedaArticulo.Text)); break;
                        case "talla": dgArticulo.ItemsSource = uof.ArticuloRepository.GetArticulosTalla(tbCampoBusquedaArticulo.Text); break;
                        case "estado": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.estado.Contains(tbCampoBusquedaArticulo.Text)); break;
                    }
                }
                else
                {
                    float var = 0;
                    try { var = Convert.ToSingle(tbCampoBusquedaArticulo.Text, CultureInfo.InvariantCulture.NumberFormat); }
                    catch (Exception)
                    {
                        Debug.WriteLine(String.Format("Error al convertir \"{0}\" en un número", tbCampoBusquedaArticulo.Text));
                        MessageBox.Show("Solo valen números", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }

                    if (cbCampoBusquedaArticulo.Text.Equals("precio compra"))
                    {
                        switch (cbMetodoBusquedaArticulo.Text)
                        {
                            case ">": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.precioCompra > var); break;
                            case "<": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.precioCompra < var); break;
                            case ">=": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.precioCompra >= var); break;
                            case "<=": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.precioCompra <= var); break;
                            case "==": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.precioCompra == var); break;
                            case "!=": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.precioCompra != var); break;
                        }
                    }
                    else if (cbCampoBusquedaArticulo.Text.Equals("precio venta"))
                    {
                        switch (cbMetodoBusquedaArticulo.Text)
                        {
                            case ">": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.precioVenta > var); break;
                            case "<": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.precioVenta < var); break;
                            case ">=": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.precioVenta >= var); break;
                            case "<=": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.precioVenta <= var); break;
                            case "==": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.precioVenta == var); break;
                            case "!=": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.precioVenta != var); break;
                        }
                    }
                    else
                    {
                        switch (cbMetodoBusquedaArticulo.Text)
                        {
                            case ">": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.nivelNuevoPedido > var); break;
                            case "<": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.nivelNuevoPedido < var); break;
                            case ">=": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.nivelNuevoPedido >= var); break;
                            case "<=": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.nivelNuevoPedido <= var); break;
                            case "==": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.nivelNuevoPedido == var); break;
                            case "!=": dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet(c => c.nivelNuevoPedido != var); break;
                        }
                    }
                }

                LimpiarArticulo();
            }
            else
            {
                VerTodosArticulos();
            }
        }

        private void VerTodosArticulos()
        {
            ActualizarContexto();

            dgArticulo.ItemsSource = uof.ArticuloRepository.MultiGet();
            LimpiarArticulo();
        }

        private void MostrarArticulo()
        {
            panelMantenimientoArticulos.DataContext = articulo;
            imagenArticulo.Source = ConvertArrayByteToImage(articulo.imagen);
            if (articulo.estado.Equals(HABILITADO))
            {
                panelDatosArticulo.IsEnabled = true;
                panelBotonesImagenesArticulo.IsEnabled = true;
                btModificarArticulo.IsEnabled = true;
                btEliminarArticulo.IsEnabled = true;
                if (privilegios)
                {
                    btAñadirDetalleArticulo.IsEnabled = true;
                }
            }
            else
            {
                DeshabilitarBotonesArticulo();
            }
            dgDetallesArticulo.ItemsSource = articulo.detallesArticulo;
        }

        private void LimpiarArticulo()
        {
            articulo = new Articulo();

            cbProveedorArticulo.ItemsSource = uof.ProveedorRepository.MultiGet();
            panelMantenimientoArticulos.DataContext = articulo;
            imagenArticulo.Source = null;
            DeshabilitarBotonesArticulo();

            dgArticulo.SelectedItem = null;
            panelBusquedaArticulos.IsEnabled = true;
            panelDatagridsArticulos.IsEnabled = true;
        }

        private void DeshabilitarBotonesArticulo()
        {
            panelDatosArticulo.IsEnabled = false;
            panelBotonesImagenesArticulo.IsEnabled = false;
            btModificarArticulo.IsEnabled = false;
            btEliminarArticulo.IsEnabled = false;
            btAñadirDetalleArticulo.IsEnabled = false;
            dgDetallesArticulo.ItemsSource = null;
        }

        #endregion

        #region PANEL TPV

        private void btCategoriaVenta_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            idCategoria = ((Button)sender).Name.ToString().Split('_')[1];
            CargarArticulosVenta();
        }

        private void btArticuloVenta_Click(object sender, RoutedEventArgs e)
        {
            panelInfoDetalleArticuloVenta.DataContext = null;
            lbStockDetalleArticuloVenta.Content = null;
            wpBotonesDetallesArticuloVenta.Children.Clear();

            ActualizarContexto();

            string articuloId = ((Button)sender).Name.ToString().Split('_')[1];
            Articulo a = uof.ArticuloRepository.MultiGet(c => c.ArticuloId.ToString().Equals(articuloId)).FirstOrDefault();
            panelInfoArticuloVenta.DataContext = a;
            tbDescripcionArticuloVenta.Text = a.descripcion;

            foreach (DetalleArticulo da in uof.DetalleArticuloRepository.MultiGet(c => c.ArticuloId.ToString().Equals(articuloId) && c.estado.Equals(HABILITADO)).ToList())
            {
                Button bt = new Button();
                if (da.stock == 0)
                {
                    bt.IsEnabled = false;
                }
                bt.Name = "btTalla_" + da.talla + da.ArticuloId;
                bt.Content = da.talla;
                bt.Cursor = Cursors.Hand;
                bt.Width = 50;
                bt.Height = 50;
                bt.Margin = new Thickness(5);
                bt.Click += btDetalleArticuloVenta_Click;
                bt.MouseEnter += btDetalleArticuloVenta_Focus;

                wpBotonesDetallesArticuloVenta.Children.Add(bt);
            }
        }

        private void btDetalleArticuloVenta_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            string talla = ((Button)sender).Name.ToString().Split('_')[1];
            DetalleArticulo da = uof.DetalleArticuloRepository.MultiGet(c => (c.talla + c.ArticuloId).Equals(talla)).FirstOrDefault();

            if (uof.DetalleVentaRepository.CheckStockDetalleVentaDetalleArticuloCarrito(detallesVenta, da))
            {
                detallesVenta.Add(new DetalleVenta(idDetalleVenta++, da, venta));
                dgDetalleVenta.Items.Refresh();

                CalcularStockTmp(da);
                CalcularTotalVenta();
            }
            else
            {
                MessageBox.Show("No queda más stock para esta talla", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btDetalleArticuloVenta_Focus(object sender, RoutedEventArgs e)
        {
            string talla = ((Button)sender).Name.ToString().Split('_')[1];
            DetalleArticulo da = uof.DetalleArticuloRepository.MultiGet(c => (c.talla + c.ArticuloId).Equals(talla)).FirstOrDefault();

            CalcularStockTmp(da);

            panelInfoArticuloVenta.DataContext = da.articulo;
            tbDescripcionArticuloVenta.Text = da.articulo.descripcion;
            panelInfoDetalleArticuloVenta.DataContext = da;
        }

        private void btQuitarDetalleArticuloVenta_Click(object sender, RoutedEventArgs e)
        {
            DetalleVenta dv = (DetalleVenta)dgDetalleVenta.SelectedItem;
            DetalleArticulo da = dv.detalleArticulo;

            detallesVenta.Remove(dv);
            dgDetalleVenta.Items.Refresh();

            CalcularStockTmp(da);
            CalcularTotalVenta();

            if (detallesVenta.Count == 0)
            {
                idDetalleVenta = 1;
                detallesVenta = new HashSet<DetalleVenta>();
            }
        }

        private void btPagarVenta_Click(object sender, RoutedEventArgs e)
        {
            if (Validar(venta) && detallesVenta.Count > 0)
            {
                var dialogResult = MessageBox.Show("¿Estás seguro de que quieres finalizar la venta y pagar?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    ActualizarContexto();

                    if (CheckStockDetalleVentaDetalleArticulo())
                    {
                        venta.detallesVenta = detallesVenta;
                        venta.fecha = DateTime.Now;

                        uof.VentaRepository.AddVenta(venta);
                        Autoreabastecimiento();

                        Empleado empleado = uof.EmpleadoRepository.MultiGet(c => c.usuario.Equals(tbUsuarioLogin.Text)).FirstOrDefault();

                        SendEmail(EMAIL_AURITEX, String.Format(HTML_VENTA_REALIZADA, venta.VentaId, venta.totalUnidades, venta.totalPrecio, empleado.nombre + " " + empleado.apellidos, empleado.EmpleadoId));

                        VentanaTicket vt = new VentanaTicket(this, null, venta, null);
                        LimpiarVenta();
                        vt.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("No se ha podido finalizar la venta debido a que no queda stock para ninguno de los artículos del carrito", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        LimpiarVenta();
                    }
                }
            }
            else
            {
                MessageBox.Show("El carrito está vacío", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btCancelarVenta_Click(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show("¿Estás seguro de que quieres cancelar la venta?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                ActualizarContexto();

                LimpiarVenta();
            }
        }

        private void dgDetalleVenta_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgDetalleVenta.SelectedIndex >= 0)
            {
                DetalleVenta dv = (DetalleVenta)dgDetalleVenta.SelectedItem;

                CalcularStockTmp(dv.detalleArticulo);
                panelInfoArticuloVenta.DataContext = dv.detalleArticulo.articulo;
                tbDescripcionArticuloVenta.Text = dv.detalleArticulo.articulo.descripcion;
                panelInfoDetalleArticuloVenta.DataContext = dv.detalleArticulo;
            }
        }

        private void rbChico_Checked(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(idCategoria))
            {
                ActualizarContexto();

                CargarArticulosVenta();
            }
        }

        private void rbChica_Checked(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(idCategoria))
            {
                ActualizarContexto();

                CargarArticulosVenta();
            }
        }

        private void tbTodos_Checked(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(idCategoria))
            {
                ActualizarContexto();

                CargarArticulosVenta();
            }
        }

        private void CargarCategorias()
        {
            spBotonesCategoriasVenta.Children.Clear();

            foreach (Categoria c in uof.CategoriaRepository.MultiGet().ToList())
            {
                DockPanel dp = new DockPanel();
                dp.LastChildFill = true;

                Label lb = new Label();
                lb.HorizontalAlignment = HorizontalAlignment.Center;
                lb.Content = c.nombre;
                lb.FontSize = 12;
                lb.Margin = new Thickness(5);

                Image img = new Image();
                try
                {
                    img.Source = ConvertArrayByteToImage(c.imagen);
                    img.Margin = new Thickness(5);
                }
                catch (Exception ex) { Debug.WriteLine(String.Format("Error al cargar la imagen de la categoría - {0}", ex.Message)); }
                img.MaxWidth = 200;
                img.MaxHeight = 175;

                DockPanel.SetDock(lb, Dock.Bottom);
                dp.Children.Add(lb);
                dp.Children.Add(img);

                Button bt = new Button();
                bt.Name = "btCategoria_" + c.CategoriaId;
                bt.ToolTip = c.descripcion;
                bt.Content = dp;
                bt.Cursor = Cursors.Hand;
                bt.Width = 225;
                bt.Height = 200;
                bt.Margin = new Thickness(5);
                bt.Click += btCategoriaVenta_Click;

                spBotonesCategoriasVenta.Children.Add(bt);
            }
        }

        private void CargarArticulosVenta()
        {
            ResetPanelesVenta();

            Expression<Func<Articulo, bool>> lambda;
            if (rbChica.IsChecked == true)
            {
                lambda = c => c.CategoriaId.ToString().Equals(idCategoria) && c.estado.Equals(HABILITADO) && c.sexo.Equals("CHICA") && c.precioVenta > 0.0;
            }
            else if (rbChico.IsChecked == true)
            {
                lambda = c => c.CategoriaId.ToString().Equals(idCategoria) && c.estado.Equals(HABILITADO) && c.sexo.Equals("CHICO") && c.precioVenta > 0.0;
            }
            else
            {
                lambda = c => c.CategoriaId.ToString().Equals(idCategoria) && c.estado.Equals(HABILITADO) && c.precioVenta > 0.0;
            }

            foreach (Articulo a in uof.ArticuloRepository.MultiGet(lambda).ToList())
            {
                DockPanel dp = new DockPanel();
                dp.LastChildFill = true;

                Label lb = new Label();
                lb.HorizontalAlignment = HorizontalAlignment.Center;
                lb.Content = a.nombre;
                lb.FontSize = 12;

                Image img = new Image();
                try
                {
                    img.Source = ConvertArrayByteToImage(a.imagen);
                    img.Margin = new Thickness(5);
                }
                catch (Exception ex) { Debug.WriteLine(String.Format("Error al cargar la imagen del artíuclo - {0}", ex.Message)); }
                img.Width = 200;
                img.Height = 175;

                DockPanel.SetDock(lb, Dock.Bottom);
                dp.Children.Add(lb);
                dp.Children.Add(img);

                Button bt = new Button();
                if (a.stockTotal == 0)
                {
                    bt.IsEnabled = false;
                }
                bt.Name = "btArticulo_" + a.ArticuloId;
                bt.Content = dp;
                bt.Cursor = Cursors.Hand;
                bt.Width = 225;
                bt.Height = 200;
                bt.Margin = new Thickness(5);
                bt.Click += btArticuloVenta_Click;

                wpBotonesArticulosVenta.Children.Add(bt);
            }
        }

        private bool CheckStockDetalleVentaDetalleArticulo()
        {
            int stock, eliminados = 0;
            bool check = false;
            HashSet<DetalleArticulo> listaDetalleArticulos = new HashSet<DetalleArticulo>();

            foreach (DetalleVenta dv in detallesVenta)
            {
                listaDetalleArticulos.Add(dv.detalleArticulo);
            }

            foreach (DetalleArticulo da in listaDetalleArticulos)
            {
                stock = 0;

                foreach (DetalleVenta dv in detallesVenta)
                {
                    if ((dv.detalleArticulo.talla + dv.detalleArticulo.ArticuloId).Equals(da.talla + da.ArticuloId))
                    {
                        stock++;
                    }
                }
                if (stock > da.stock)
                {
                    stock = stock - da.stock;
                    eliminados += stock;
                    check = true;

                    foreach (DetalleVenta dv in detallesVenta)
                    {
                        if ((dv.detalleArticulo.talla + dv.detalleArticulo.ArticuloId).Equals(da.talla + da.ArticuloId))
                        {
                            detallesVenta.Remove(dv);

                            stock--;

                            if (stock == 0)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            if (check && detallesVenta.Count > 0)
            {
                MessageBox.Show(String.Format("Se han quitado {0} tallas debido a que no queda stock", eliminados), "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            return (detallesVenta.Count > 0) ? true : false;
        }

        private void Autoreabastecimiento()
        {
            if (uof.ConfiguracionRepository.MultiGet(c => c.nombre.Equals(AUTOREABASTECIMIENTO)).FirstOrDefault().estado)
            {
                List<Compra> listaCompras = new List<Compra>();
                HashSet<int> listaIds = new HashSet<int>();
                Compra c;

                foreach (DetalleVenta dv in venta.detallesVenta)
                {
                    if (dv.detalleArticulo.articulo.proveedor.estado.Equals(HABILITADO))
                    {
                        listaIds.Add(dv.detalleArticulo.articulo.ProveedorId);
                    }
                }

                foreach (int i in listaIds)
                {
                    c = new Compra(1, i);
                    c.fecha = DateTime.Now;

                    listaCompras.Add(c);
                }

                DetalleCompra dc;
                foreach (DetalleVenta dv in venta.detallesVenta)
                {
                    if (dv.detalleArticulo.articulo.nivelNuevoPedido > 0 && dv.detalleArticulo.stock < dv.detalleArticulo.articulo.nivelNuevoPedido && dv.detalleArticulo.unidadesEnPedido == 0)
                    {
                        foreach (Compra compra in listaCompras)
                        {
                            if (compra.ProveedorId == dv.detalleArticulo.articulo.ProveedorId)
                            {
                                dc = new DetalleCompra(compra.detallesCompra.Count + 1, dv.detalleArticulo, compra);
                                dc.unidades = dv.detalleArticulo.articulo.nivelMaximoPedido - dv.detalleArticulo.stock;

                                compra.detallesCompra.Add(dc);

                                dv.detalleArticulo.unidadesEnPedido = dc.unidades;

                                break;
                            }
                        }
                    }
                }

                foreach (Compra compra in listaCompras)
                {
                    if (compra.detallesCompra.Count > 0)
                    {
                        uof.CompraRepository.Add(compra);
                        SendEmail(EMAIL_AURITEX, String.Format(HTML_COMPRA_AUTOREABASTECIMIENTO_REALIZADA, compra.CompraId, compra.totalUnidades, compra.totalPrecio));
                    }
                }
            }
        }

        private void CalcularStockTmp(DetalleArticulo da)
        {
            int stock = da.stock;

            foreach (DetalleVenta dv in detallesVenta)
            {
                if (dv.detalleArticulo.ArticuloId == da.ArticuloId && dv.detalleArticulo.talla.Equals(da.talla))
                {
                    stock--;
                }
            }

            lbStockDetalleArticuloVenta.Content = stock;
        }

        private void CalcularTotalVenta()
        {
            float total = 0;

            foreach (DetalleVenta dv in detallesVenta)
            {
                total += dv.detalleArticulo.articulo.precioVenta;
            }

            lbTotalVenta.Content = total;

            if (total == 0.0)
            {
                tabCompras.IsEnabled = true;
                EstadoMantenimientos(true);
            }
            else
            {
                tabCompras.IsEnabled = false;
                EstadoMantenimientos(false);
            }
        }

        private void ResetPanelesVenta()
        {
            panelInfoArticuloVenta.DataContext = null;
            panelInfoDetalleArticuloVenta.DataContext = null;
            lbStockDetalleArticuloVenta.Content = null;
            tbDescripcionArticuloVenta.Text = "";
            wpBotonesArticulosVenta.Children.Clear();
            wpBotonesDetallesArticuloVenta.Children.Clear();
        }

        public void LimpiarVenta()
        {
            idDetalleVenta = 1;
            
            ResetPanelesVenta();

            venta = new Venta(uof.EmpleadoRepository.MultiGet(c => c.usuario.Equals(tbUsuarioLogin.Text) && c.estado.Equals(HABILITADO)).FirstOrDefault().EmpleadoId, 1);
            detallesVenta = new HashSet<DetalleVenta>();
            panelBotonesVenta.DataContext = venta;
            dgDetalleVenta.ItemsSource = detallesVenta;

            CalcularTotalVenta();
        }

        #endregion

        #region PANEL COMPRAS

        private void btArticuloCompra_Click(object sender, RoutedEventArgs e)
        {
            panelInfoDetalleArticuloCompra.DataContext = null;
            wpBotonesDetallesArticuloCompra.Children.Clear();

            ActualizarContexto();

            string articuloId = ((Button)sender).Name.ToString().Split('_')[1];
            Articulo a = uof.ArticuloRepository.MultiGet(c => c.ArticuloId.ToString().Equals(articuloId)).FirstOrDefault();
            panelInfoArticuloCompra.DataContext = a;
            tbDescripcionArticuloCompra.Text = a.descripcion;

            foreach (DetalleArticulo da in uof.DetalleArticuloRepository.MultiGet(c => c.ArticuloId.ToString().Equals(articuloId) && c.estado.Equals(HABILITADO)).ToList())
            {
                Button bt = new Button();
                bt.Name = "btTalla_" + da.talla + da.ArticuloId;
                bt.Content = da.talla;
                bt.Cursor = Cursors.Hand;
                bt.Width = 50;
                bt.Height = 50;
                bt.Margin = new Thickness(5);
                bt.Click += btDetalleArticuloCompra_Click;
                bt.MouseEnter += btDetalleArticuloCompra_Focus;

                wpBotonesDetallesArticuloCompra.Children.Add(bt);
            }
        }

        private void btDetalleArticuloCompra_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            bool check = false;
            string talla = ((Button)sender).Name.ToString().Split('_')[1];

            DetalleArticulo da = uof.DetalleArticuloRepository.MultiGet(c => (c.talla + c.ArticuloId).Equals(talla)).FirstOrDefault();

            dgDetalleCompra.SelectedItem = null;

            foreach (DetalleCompra dc in detallesCompra)
            {
                if ((dc.detalleArticulo.talla + dc.detalleArticulo.ArticuloId).Equals(talla))
                {
                    dc.unidades++;
                    check = true;
                }
            }
            if (!check)
            {
                detallesCompra.Add(new DetalleCompra(idDetalleCompra++, da, compra));
            }

            dgDetalleCompra.Items.Refresh();
            CalcularTotalCompra();
        }

        private void btDetalleArticuloCompra_Focus(object sender, RoutedEventArgs e)
        {
            string talla = ((Button)sender).Name.ToString().Split('_')[1];
            DetalleArticulo da = uof.DetalleArticuloRepository.MultiGet(c => (c.talla + c.ArticuloId).Equals(talla)).FirstOrDefault();

            panelInfoArticuloCompra.DataContext = da.articulo;
            tbDescripcionArticuloCompra.Text = da.articulo.descripcion;
            panelInfoDetalleArticuloCompra.DataContext = da;
        }

        private void btQuitarDetalleArticuloCompra_Click(object sender, RoutedEventArgs e)
        {
            DetalleCompra dc = (DetalleCompra)dgDetalleCompra.SelectedItem;

            detallesCompra.Remove(dc);
            dgDetalleCompra.Items.Refresh();
            CalcularTotalCompra();

            if (detallesCompra.Count == 0)
            {
                idDetalleCompra = 1;
                detallesCompra = new HashSet<DetalleCompra>();
            }
        }

        private void btFinalizarCompra_Click(object sender, RoutedEventArgs e)
        {
            if (Validar(compra) && detallesCompra.Count > 0)
            {
                var dialogResult = MessageBox.Show("¿Estás seguro de que quieres finalizar la compra?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
                if (dialogResult == MessageBoxResult.Yes)
                {
                    compra.detallesCompra = detallesCompra;
                    compra.fecha = DateTime.Now;
                    uof.CompraRepository.AddCompra(compra);

                    VentanaTicket vt = new VentanaTicket(this, null, null, compra);
                    LimpiarCompra();
                    vt.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("El carrito está vacío", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btCancelarCompra_Click(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show("¿Estás seguro de que quieres cancelar la compra?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                ActualizarContexto();

                LimpiarCompra();
            }
        }

        private void dgDetalleCompra_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgDetalleCompra.SelectedIndex >= 0)
            {
                DetalleCompra dc = (DetalleCompra)dgDetalleCompra.SelectedItem;

                panelInfoArticuloCompra.DataContext = dc.detalleArticulo.articulo;
                tbDescripcionArticuloCompra.Text = dc.detalleArticulo.articulo.descripcion;
                panelInfoDetalleArticuloCompra.DataContext = dc.detalleArticulo;
            }
        }

        private void cbProveedorCompra_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbProveedorCompra.SelectedIndex >= 0)
            {
                ResetPanelesCompra();

                idDetalleCompra = 1;
                compra = new Compra(uof.EmpleadoRepository.MultiGet(c => c.usuario.Equals(tbUsuarioLogin.Text) && c.estado.Equals(HABILITADO)).FirstOrDefault().EmpleadoId, ((Proveedor)cbProveedorCompra.SelectedItem).ProveedorId);
                detallesCompra = new HashSet<DetalleCompra>();
                dgDetalleCompra.ItemsSource = detallesCompra;
                panelBotonesCompra.DataContext = compra;

                CargarArticulosCompra();
                CalcularTotalCompra();
            }
        }

        private void tbUnidadesCompra_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (dgDetalleCompra.SelectedIndex >= 0)
            {
                try
                {
                    int cant = Convert.ToInt32(((TextBox)sender).Text);
                    if (cant != 0)
                    {
                        ((DetalleCompra)dgDetalleCompra.SelectedItem).unidades = cant;
                    }
                }
                catch (Exception) { Debug.WriteLine(String.Format("Error al convertir \"{0}\" en un número", ((TextBox)sender).Text)); }
            }
        }

        private void tbUnidadesCompra_LostFocus(object sender, RoutedEventArgs e)
        {
            dgDetalleCompra.SelectedItem = null;

            CalcularTotalCompra();
            dgDetalleCompra.Items.Refresh();
        }

        private void CargarArticulosCompra()
        {
            ResetPanelesCompra();

            foreach (Articulo a in uof.ArticuloRepository.MultiGet(c => c.ProveedorId == compra.ProveedorId && c.estado.Equals(HABILITADO)).ToList())
            {
                DockPanel dp = new DockPanel();
                dp.LastChildFill = true;

                Label lb = new Label();
                lb.HorizontalAlignment = HorizontalAlignment.Center;
                lb.Content = a.nombre;
                lb.FontSize = 12;

                Image img = new Image();
                try
                {
                    img.Source = ConvertArrayByteToImage(a.imagen);
                    img.Margin = new Thickness(5);
                }
                catch (Exception ex) { Debug.WriteLine(String.Format("Error al cargar la imagen del artíuclo - {0}", ex.Message)); }
                img.Width = 200;
                img.Height = 175;

                DockPanel.SetDock(lb, Dock.Bottom);
                dp.Children.Add(lb);
                dp.Children.Add(img);

                Button bt = new Button();
                bt.Name = "btArticulo_" + a.ArticuloId;
                bt.Content = dp;
                bt.Cursor = Cursors.Hand;
                bt.Width = 225;
                bt.Height = 200;
                bt.Margin = new Thickness(5);
                bt.Click += btArticuloCompra_Click;

                wpBotonesArticulosCompra.Children.Add(bt);
            }
        }

        private void CalcularTotalCompra()
        {
            float total = 0;

            foreach (DetalleCompra dc in detallesCompra)
            {
                total += dc.detalleArticulo.articulo.precioCompra * dc.unidades;
            }

            lbTotalCompra.Content = total;

            if (total == 0.0)
            {
                tabTPV.IsEnabled = true;
                EstadoMantenimientos(true);
            }
            else
            {
                tabTPV.IsEnabled = false;
                EstadoMantenimientos(false);
            }
        }

        private void ResetPanelesCompra()
        {
            panelInfoArticuloCompra.DataContext = null;
            panelInfoDetalleArticuloCompra.DataContext = null;
            tbDescripcionArticuloCompra.Text = "";
            wpBotonesArticulosCompra.Children.Clear();
            wpBotonesDetallesArticuloCompra.Children.Clear();
        }

        public void LimpiarCompra()
        {
            idDetalleCompra = 1;
            cbProveedorCompra.ItemsSource = uof.ProveedorRepository.MultiGet(c => c.estado.Equals(HABILITADO));

            ResetPanelesCompra();

            compra = new Compra(uof.EmpleadoRepository.MultiGet(c => c.usuario.Equals(tbUsuarioLogin.Text) && c.estado.Equals(HABILITADO)).FirstOrDefault().EmpleadoId, ((Proveedor)cbProveedorCompra.Items[0]).ProveedorId);
            detallesCompra = new HashSet<DetalleCompra>();
            dgDetalleCompra.ItemsSource = detallesCompra;
            panelBotonesCompra.DataContext = compra;

            CargarArticulosCompra();
            CalcularTotalCompra();
        }

        #endregion

        #region PANEL HISTORIALES

        //***HISTORIAL VENTAS***//

        private void dgHistorialVentas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgHistorialVentas.SelectedIndex >= 0)
            {
                dgHistorialDetallesVenta.ItemsSource = ((Venta)dgHistorialVentas.SelectedItem).detallesVenta;
            }
        }

        private void dgHistorialVentas_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgHistorialVentas.SelectedIndex >= 0)
            {
                ActualizarContexto();

                VentanaTicket vt = new VentanaTicket(this, null, (Venta)dgHistorialVentas.SelectedItem, null);
                vt.ShowDialog();
            }
        }

        private void btVerTodasVentas_Click(object sender, RoutedEventArgs e)
        {
            VerTodasVentas();
        }

        private void cbCampoBusquedaHistorialVentas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCampoBusquedaHistorialVentas.SelectedItem.Equals("descuento"))
            {
                cbMetodoBusquedaHistorialVentas.Visibility = Visibility.Visible;
            }
            else
            {
                cbMetodoBusquedaHistorialVentas.Visibility = Visibility.Collapsed;
            }
        }

        private void tbCampoBusquedaHistorialVentas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BuscarHistorialVentas();
            }
        }

        private void btBuscarVenta_Click(object sender, RoutedEventArgs e)
        {
            BuscarHistorialVentas();
        }

        private void calendarioVentas_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime fecha = Convert.ToDateTime(calendarioVentas.SelectedDate);

            ActualizarContexto();

            dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.fecha.Day == fecha.Day && c.fecha.Month == fecha.Month && c.fecha.Year == fecha.Year);
            dgHistorialDetallesVenta.ItemsSource = null;

            CalcularResumenVentas();
        }

        private void btDevolver_Click(object sender, RoutedEventArgs e)
        {
            DetalleVenta dv = (DetalleVenta)dgHistorialDetallesVenta.SelectedItem;

            var dialogResult = MessageBox.Show("¿Estás seguro de que quieres devolver el artículo?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                ActualizarContexto();

                if (uof.DetalleVentaRepository.MultiGet(c => c.DetalleVentaId == dv.DetalleVentaId && c.VentaId == dv.VentaId && c.estado.Equals("VENDIDO")).Count() == 1)
                {
                    if ((DateTime.Now - dv.venta.fecha).TotalDays <= 30)
                    {
                        dv.detalleArticulo.stock++;
                        dv.estado = "DEVUELTO";
                        uof.DetalleVentaRepository.Modify(dv);

                        SendEmail(EMAIL_AURITEX, String.Format(HTML_ARTICULO_DEVUELTO, dv.detalleArticulo.articulo.nombre, dv.detalleArticulo.talla, dv.detalleArticulo.articulo.sexo, dv.detalleArticulo.articulo.precioVenta));

                        dgHistorialDetallesVenta.Items.Refresh();

                        MessageBox.Show("Artículo devuelto", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("No se puede devolver el artículo porque pasaron más de 30 días desde que fue comprado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Este artículo ya ha sido devuelto", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        private void dgHistorialDetallesVenta_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgHistorialDetallesVenta.SelectedIndex >= 0)
            {
                ActualizarContexto();

                VentanaDetalleArticuloInfo vdai = new VentanaDetalleArticuloInfo(((DetalleVenta)dgHistorialDetallesVenta.SelectedItem).detalleArticulo, this, null);
                vdai.ShowDialog();
            }
        }

        private void BuscarHistorialVentas()
        {
            if (!String.IsNullOrEmpty(tbCampoBusquedaHistorialVentas.Text))
            {
                dgHistorialDetallesVenta.ItemsSource = null;

                ActualizarContexto();

                if (!cbMetodoBusquedaHistorialVentas.IsVisible)
                {
                    switch (cbCampoBusquedaHistorialVentas.Text)
                    {
                        case "empleado": dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.empleado.nombre.Contains(tbCampoBusquedaHistorialVentas.Text)); break;
                        case "cliente": dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.cliente.nombre.Contains(tbCampoBusquedaHistorialVentas.Text)); break;
                        case "ciudad": dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.ciudadEnvio.Contains(tbCampoBusquedaHistorialVentas.Text)); break;
                        case "código postal": dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.codPostalEnvio.Contains(tbCampoBusquedaHistorialVentas.Text)); break;
                        case "dirección": dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.direccionEnvio.Contains(tbCampoBusquedaHistorialVentas.Text)); break;
                        case "forma pago": dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.formaPago.Contains(tbCampoBusquedaHistorialVentas.Text)); break;
                        case "estado":
                            dgHistorialVentas.ItemsSource = null;
                            dgHistorialDetallesVenta.ItemsSource = uof.DetalleVentaRepository.MultiGet(c => c.estado.Contains(tbCampoBusquedaHistorialVentas.Text));
                            break;
                    }
                }
                else
                {
                    float descuentoHistorialVenta = 0;
                    try { descuentoHistorialVenta = Convert.ToSingle(tbCampoBusquedaHistorialVentas.Text, CultureInfo.InvariantCulture.NumberFormat); }
                    catch (Exception)
                    {
                        Debug.WriteLine(String.Format("Error al convertir \"{0}\" en un número", tbCampoBusquedaHistorialVentas.Text));
                        MessageBox.Show("Solo valen números", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }

                    switch (cbMetodoBusquedaHistorialVentas.Text)
                    {
                        case ">": dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.descuento > descuentoHistorialVenta); break;
                        case "<": dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.descuento < descuentoHistorialVenta); break;
                        case ">=": dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.descuento >= descuentoHistorialVenta); break;
                        case "<=": dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.descuento <= descuentoHistorialVenta); break;
                        case "==": dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.descuento == descuentoHistorialVenta); break;
                        case "!=": dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet(c => c.descuento != descuentoHistorialVenta); break;
                    }
                }

                CalcularResumenVentas();
            }
            else
            {
                VerTodasVentas();
            }
        }

        private void VerTodasVentas()
        {
            ActualizarContexto();

            dgHistorialVentas.ItemsSource = uof.VentaRepository.MultiGet();
            dgHistorialDetallesVenta.ItemsSource = null;

            CalcularResumenVentas();
        }

        private void CalcularResumenVentas()
        {
            int cantidadVentas = dgHistorialVentas.Items.Count;
            int chico = 0, chica = 0, unidades = 0;
            float total = 0;
            lbCantidadVentas.Content = cantidadVentas;

            dgHistorialVentas.SelectedItem = null;

            foreach (Venta v in dgHistorialVentas.Items)
            {
                foreach (DetalleVenta dv in v.detallesVenta)
                {
                    if (dv.estado.Equals("VENDIDO"))
                    {
                        if (dv.detalleArticulo.articulo.sexo.Equals("CHICO"))
                        {
                            chico++;
                        }
                        else
                        {
                            chica++;
                        }
                        unidades++;
                    }
                }
                total += v.totalPrecio;
            }

            lbSexoVentas.Content = (chico > chica) ? "chico, " + chico + " uds" : "chica, " + chica + " uds";
            lbGastoMedioVentas.Content = total / cantidadVentas;
            lbTotalUnidadesVentas.Content = unidades;
            lbTotalPrecioVentas.Content = total;
        }

        //***HISTORIAL COMPRAS***//

        private void dgHistorialCompras_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgHistorialCompras.SelectedIndex >= 0)
            {
                dgHistorialDetallesCompra.ItemsSource = ((Compra)dgHistorialCompras.SelectedItem).detallesCompra;
            }
        }

        private void dgHistorialCompras_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgHistorialCompras.SelectedIndex >= 0)
            {
                ActualizarContexto();

                VentanaTicket vt = new VentanaTicket(this, null, null, (Compra)dgHistorialCompras.SelectedItem);
                vt.ShowDialog();
            }
        }

        private void btVerTodasCompras_Click(object sender, RoutedEventArgs e)
        {
            VerTodasCompras();
        }

        private void tbCampoBusquedaHistorialCompras_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BuscarHistorialCompras();
            }
        }

        private void btBuscarCompra_Click(object sender, RoutedEventArgs e)
        {
            BuscarHistorialCompras();
        }

        private void calendarioCompras_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime fecha = Convert.ToDateTime(calendarioCompras.SelectedDate);

            ActualizarContexto();

            dgHistorialCompras.ItemsSource = uof.CompraRepository.MultiGet(c => c.fecha.Day == fecha.Day && c.fecha.Month == fecha.Month && c.fecha.Year == fecha.Year);
            dgHistorialDetallesCompra.ItemsSource = null;

            CalcularResumenCompras();
        }

        private void btModificarEstadoCompra_Click(object sender, RoutedEventArgs e)
        {
            Compra c = (Compra)dgHistorialCompras.SelectedItem;

            ActualizarContexto();

            if (c.estado.Equals("PENDIENTE"))
            {
                VentanaModificarEstadoCompra vmec = new VentanaModificarEstadoCompra(this, null, uof, c);
                vmec.ShowDialog();
            }
            else
            {
                MessageBox.Show("Este pedido ya ha sido finalizado o cancelado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void dgHistorialDetallesCompra_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgHistorialDetallesCompra.SelectedIndex >= 0)
            {
                ActualizarContexto();

                VentanaDetalleArticuloInfo vdai = new VentanaDetalleArticuloInfo(((DetalleCompra)dgHistorialDetallesCompra.SelectedItem).detalleArticulo, this, null);
                vdai.ShowDialog();
            }
        }

        private void BuscarHistorialCompras()
        {
            if (!String.IsNullOrEmpty(tbCampoBusquedaHistorialCompras.Text))
            {
                dgHistorialDetallesCompra.ItemsSource = null;

                ActualizarContexto();

                switch (cbCampoBusquedaHistorialCompras.Text)
                {
                    case "empleado": dgHistorialCompras.ItemsSource = uof.CompraRepository.MultiGet(c => c.empleado.nombre.Contains(tbCampoBusquedaHistorialCompras.Text)); break;
                    case "proveedor": dgHistorialCompras.ItemsSource = uof.CompraRepository.MultiGet(c => c.proveedor.nombre.Contains(tbCampoBusquedaHistorialCompras.Text)); break;
                    case "estado": dgHistorialCompras.ItemsSource = uof.CompraRepository.MultiGet(c => c.estado.Contains(tbCampoBusquedaHistorialCompras.Text)); break;
                }
            }
            else
            {
                VerTodasCompras();
            }
        }

        private void VerTodasCompras()
        {
            ActualizarContexto();

            dgHistorialCompras.ItemsSource = uof.CompraRepository.MultiGet();
            dgHistorialDetallesCompra.ItemsSource = null;

            CalcularResumenCompras();
        }

        private void CalcularResumenCompras()
        {
            dgHistorialCompras.SelectedItem = null;
            int chico = 0, chica = 0, unidades = 0, cantidadCompras = 0;
            float total = 0;

            foreach (Compra c in dgHistorialCompras.Items)
            {
                if (c.estado.Equals("FINALIZADA"))
                {
                    foreach (DetalleCompra dc in c.detallesCompra)
                    {
                        if (dc.detalleArticulo.articulo.sexo.Equals("CHICO"))
                        {
                            chico++;
                        }
                        else
                        {
                            chica++;
                        }
                        unidades++;
                    }
                    cantidadCompras++;
                    total += c.totalPrecio;
                }
            }

            lbCantidadCompras.Content = cantidadCompras;
            lbSexoCompras.Content = (chico > chica) ? "chico, " + chico + " uds" : "chica, " + chica + " uds";
            lbGastoMedioCompras.Content = total / cantidadCompras;
            lbTotalUnidadesCompras.Content = unidades;
            lbTotalPrecioCompras.Content = total;
        }

        #endregion

        #region PANEL CATEGORIAS

        private void btSeleccionarImagenCategoria_Click(object sender, RoutedEventArgs e)
        {
            BitmapImage bitmap = SeleccionarImagen();
            if (bitmap != null)
            {
                imagenCategoria.Source = bitmap;
                if (btNuevaCategoria.IsVisible)
                {
                    categoria.imagen = ConvertImageToArrayByte((BitmapImage)imagenCategoria.Source);
                    uof.CategoriaRepository.Modify(categoria);
                }
            }
        }

        private void btQuitarImagenCategoria_Click(object sender, RoutedEventArgs e)
        {
            imagenCategoria.Source = null;
            if (btNuevaCategoria.IsVisible)
            {
                categoria.imagen = null;
                uof.CategoriaRepository.Modify(categoria);
            }
        }

        private void btNuevaCategoria_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            LimpiarCategoria();

            btCancelarCategoria.Visibility = Visibility.Visible;
            btNuevaCategoria.Visibility = Visibility.Collapsed;
            btGuardarCategoria.IsEnabled = true;
            panelBotonesImagenesCategoria.IsEnabled = true;
            panelDatosCategorias.IsEnabled = true;
            panelBusquedaCategorias.IsEnabled = false;
            dgCategoria.IsEnabled = false;
        }

        private void btCancelarCategoria_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            LimpiarCategoria();

            btCancelarCategoria.Visibility = Visibility.Collapsed;
            btNuevaCategoria.Visibility = Visibility.Visible;
            btGuardarCategoria.IsEnabled = false;
        }

        private void btGuardarCategoria_Click(object sender, RoutedEventArgs e)
        {
            if (Validar(categoria))
            {
                categoria.imagen = ConvertImageToArrayByte((BitmapImage)imagenCategoria.Source);
                uof.CategoriaRepository.Add(categoria);

                dgCategoria.ItemsSource = uof.CategoriaRepository.MultiGet();
                MostrarCategoria();

                btCancelarCategoria.Visibility = Visibility.Collapsed;
                btNuevaCategoria.Visibility = Visibility.Visible;
                btGuardarCategoria.IsEnabled = false;
                panelBusquedaCategorias.IsEnabled = true;
                dgCategoria.IsEnabled = true;

                MessageBox.Show("Categoría añadida", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btModificarCategoria_Click(object sender, RoutedEventArgs e)
        {
            if (Validar(categoria))
            {
                categoria.imagen = ConvertImageToArrayByte((BitmapImage)imagenCategoria.Source);
                uof.CategoriaRepository.Modify(categoria);

                dgCategoria.ItemsSource = uof.CategoriaRepository.MultiGet();

                MessageBox.Show("Categoría modificada", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btEliminarCategoria_Click(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show("¿Estás seguro de que quieres eliminar la categoría?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                int afectados = uof.CategoriaRepository.RemoveCategoria(categoria);

                dgCategoria.ItemsSource = uof.CategoriaRepository.MultiGet();
                LimpiarCategoria();

                MessageBox.Show("Categoría eliminada\nArtículos afectados: " + afectados, "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void tbCampoBusquedaCategoria_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BuscarCategoria();
            }
        }

        private void btBuscarCategoria_Click(object sender, RoutedEventArgs e)
        {
            BuscarCategoria();
        }

        private void btVerTodasCategorias_Click(object sender, RoutedEventArgs e)
        {
            VerTodasCategorias();   
        }

        private void dgCategoria_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgCategoria.SelectedIndex >= 0)
            {
                categoria = (Categoria)dgCategoria.SelectedItem;
                MostrarCategoria();
            }
        }

        private void dgCategoria_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgCategoria.SelectedIndex >= 0)
            {
                ActualizarContexto();

                VentanaMultiInfo vmi = new VentanaMultiInfo(this, uof, 4, null, null, categoria.articulos.ToList(), null, null);
                vmi.ShowDialog();
            }
        }

        private void BuscarCategoria()
        {
            if (!String.IsNullOrEmpty(tbCampoBusquedaCategoria.Text))
            {
                ActualizarContexto();

                switch (cbCampoBusquedaCategoria.Text)
                {
                    case "nombre": dgCategoria.ItemsSource = uof.CategoriaRepository.MultiGet(c => c.nombre.Contains(tbCampoBusquedaCategoria.Text)); break;
                    case "descripción": dgCategoria.ItemsSource = uof.CategoriaRepository.MultiGet(c => c.descripcion.Contains(tbCampoBusquedaCategoria.Text)); break;
                }

                LimpiarCategoria();
            }
            else
            {
                VerTodasCategorias();
            }
        }

        private void VerTodasCategorias()
        {
            ActualizarContexto();

            dgCategoria.ItemsSource = uof.CategoriaRepository.MultiGet();
            LimpiarCategoria();
        }

        private void MostrarCategoria()
        {
            panelMantenimientoCategorias.DataContext = categoria;
            imagenCategoria.Source = ConvertArrayByteToImage(categoria.imagen);
            if (!categoria.nombre.Equals("DEFAULT"))
            {
                panelDatosCategorias.IsEnabled = true;
                panelBotonesImagenesCategoria.IsEnabled = true;
                btModificarCategoria.IsEnabled = true;
                btEliminarCategoria.IsEnabled = true;
            }
            else
            {
                DeshabilitarBotonesCategoria();
            }
        }

        private void LimpiarCategoria()
        {
            categoria = new Categoria();

            panelMantenimientoCategorias.DataContext = categoria;
            imagenCategoria.Source = null;

            DeshabilitarBotonesCategoria();

            dgCategoria.SelectedItem = null;
            panelBusquedaCategorias.IsEnabled = true;
            dgCategoria.IsEnabled = true;
        }

        private void DeshabilitarBotonesCategoria()
        {
            panelDatosCategorias.IsEnabled = false;
            panelBotonesImagenesCategoria.IsEnabled = false;
            btModificarCategoria.IsEnabled = false;
            btEliminarCategoria.IsEnabled = false;
        }

        #endregion

        #region PANEL PROVEEDORES

        private void btSeleccionarImagenProveedor_Click(object sender, RoutedEventArgs e)
        {
            BitmapImage bitmap = SeleccionarImagen();
            if (bitmap != null)
            {
                imagenProveedor.Source = bitmap;
                if (btNuevoProveedor.IsVisible)
                {
                    proveedor.imagen = ConvertImageToArrayByte((BitmapImage)imagenProveedor.Source);
                    uof.ProveedorRepository.Modify(proveedor);
                }
            }
        }

        private void btQuitarImagenProveedor_Click(object sender, RoutedEventArgs e)
        {
            imagenProveedor.Source = null;
            if (btNuevoProveedor.IsVisible)
            {
                proveedor.imagen = null;
                uof.ProveedorRepository.Modify(proveedor);
            }
        }

        private void btNuevoProveedor_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            LimpiarProveedor();

            btCancelarProveedor.Visibility = Visibility.Visible;
            btNuevoProveedor.Visibility = Visibility.Collapsed;
            btGuardarProveedor.IsEnabled = true;
            panelBotonesImagenesProveedor.IsEnabled = true;
            panelDatosProveedor.IsEnabled = true;
            dgProveedor.IsEnabled = false;
            panelBusquedaProveedores.IsEnabled = false;
        }

        private void btCancelarProveedor_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            LimpiarProveedor();

            btCancelarProveedor.Visibility = Visibility.Collapsed;
            btNuevoProveedor.Visibility = Visibility.Visible;
            btGuardarProveedor.IsEnabled = false;
        }

        private void btGuardarProveedor_Click(object sender, RoutedEventArgs e)
        {
            if (Validar(proveedor))
            {
                proveedor.imagen = ConvertImageToArrayByte((BitmapImage)imagenProveedor.Source);
                uof.ProveedorRepository.Add(proveedor);

                dgProveedor.ItemsSource = uof.ProveedorRepository.MultiGet();
                MostrarProveedor();

                btCancelarProveedor.Visibility = Visibility.Collapsed;
                btNuevoProveedor.Visibility = Visibility.Visible;
                btGuardarProveedor.IsEnabled = false;
                dgProveedor.IsEnabled = true;
                panelBusquedaProveedores.IsEnabled = true;

                CheckProveedores();

                MessageBox.Show("Proveedor añadido", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btModificarProveedor_Click(object sender, RoutedEventArgs e)
        {
            if (Validar(proveedor))
            {
                proveedor.imagen = ConvertImageToArrayByte((BitmapImage)imagenProveedor.Source);
                uof.ProveedorRepository.Modify(proveedor);

                dgProveedor.ItemsSource = uof.ProveedorRepository.MultiGet();

                MessageBox.Show("Proveedor modificado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btEliminarProveedor_Click(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show("¿Estás seguro de que quieres eliminar el proveedor?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                proveedor.estado = DESHABILITADO;
                uof.ProveedorRepository.Modify(proveedor);

                dgProveedor.ItemsSource = uof.ProveedorRepository.MultiGet();
                LimpiarProveedor();
                CheckProveedores();

                MessageBox.Show("Proveedor eliminado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void tbCampoBusquedaProveedor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BuscarProveedor();
            }
        }

        private void btBuscarProveedor_Click(object sender, RoutedEventArgs e)
        {
            BuscarProveedor();
        }

        private void btVerTodosProveedores_Click(object sender, RoutedEventArgs e)
        {
            VerTodosProveedores();   
        }

        private void dgProveedor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgProveedor.SelectedIndex >= 0)
            {
                proveedor = (Proveedor)dgProveedor.SelectedItem;
                MostrarProveedor();
            }
        }

        private void dgProveedor_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgProveedor.SelectedIndex >= 0)
            {
                ActualizarContexto();

                VentanaMultiInfo vmi = new VentanaMultiInfo(this, uof, 3, null, proveedor.compras.ToList(), proveedor.articulos.ToList(), null, null);
                vmi.ShowDialog();
            }
        }

        private void BuscarProveedor()
        {
            if (!String.IsNullOrEmpty(tbCampoBusquedaProveedor.Text))
            {
                ActualizarContexto();

                switch (cbCampoBusquedaProveedor.Text)
                {
                    case "nombre": dgProveedor.ItemsSource = uof.ProveedorRepository.MultiGet(c => c.nombre.Contains(tbCampoBusquedaProveedor.Text)); break;
                    case "teléfono": dgProveedor.ItemsSource = uof.ProveedorRepository.MultiGet(c => c.telefono.Contains(tbCampoBusquedaProveedor.Text)); break;
                    case "email": dgProveedor.ItemsSource = uof.ProveedorRepository.MultiGet(c => c.email.Contains(tbCampoBusquedaProveedor.Text)); break;
                    case "país": dgProveedor.ItemsSource = uof.ProveedorRepository.MultiGet(c => c.pais.Contains(tbCampoBusquedaProveedor.Text)); break;
                    case "ciudad": dgProveedor.ItemsSource = uof.ProveedorRepository.MultiGet(c => c.ciudad.Contains(tbCampoBusquedaProveedor.Text)); break;
                    case "código postal": dgProveedor.ItemsSource = uof.ProveedorRepository.MultiGet(c => c.codPostal.Contains(tbCampoBusquedaProveedor.Text)); break;
                    case "dirección": dgProveedor.ItemsSource = uof.ProveedorRepository.MultiGet(c => c.direccion.Contains(tbCampoBusquedaProveedor.Text)); break;
                    case "estado": dgProveedor.ItemsSource = uof.ProveedorRepository.MultiGet(c => c.estado.Contains(tbCampoBusquedaProveedor.Text)); break;
                }

                LimpiarProveedor();
            }
            else
            {
                VerTodosProveedores();
            }
        }

        private void VerTodosProveedores()
        {
            ActualizarContexto();

            dgProveedor.ItemsSource = uof.ProveedorRepository.MultiGet();
            LimpiarProveedor();
        }

        private void MostrarProveedor()
        {
            panelMantenimientoProveedores.DataContext = proveedor;
            imagenProveedor.Source = ConvertArrayByteToImage(proveedor.imagen);
            if (proveedor.estado.Equals(HABILITADO))
            {
                panelDatosProveedor.IsEnabled = true;
                panelBotonesImagenesProveedor.IsEnabled = true;
                btModificarProveedor.IsEnabled = true;
                btEliminarProveedor.IsEnabled = true;
            }
            else
            {
                DeshabilitarBotonesProveedor();
            }
        }

        private void LimpiarProveedor()
        {
            proveedor = new Proveedor();

            panelMantenimientoProveedores.DataContext = proveedor;
            imagenProveedor.Source = null;
            DeshabilitarBotonesProveedor();

            dgProveedor.SelectedItem = null;
            dgProveedor.IsEnabled = true;
            panelBusquedaProveedores.IsEnabled = true;
        }

        private void DeshabilitarBotonesProveedor()
        {
            panelDatosProveedor.IsEnabled = false;
            panelBotonesImagenesProveedor.IsEnabled = false;
            btModificarProveedor.IsEnabled = false;
            btEliminarProveedor.IsEnabled = false;
        }

        #endregion

        #region PANEL EMPLEADOS

        private void btSeleccionarImagenEmpleado_Click(object sender, RoutedEventArgs e)
        {
            BitmapImage bitmap = SeleccionarImagen();
            if (bitmap != null)
            {
                imagenEmpleado.Source = bitmap;
                if (btNuevoEmpleado.IsVisible)
                {
                    empleado.imagen = ConvertImageToArrayByte((BitmapImage)imagenEmpleado.Source);
                    uof.EmpleadoRepository.Modify(empleado);
                }
            }
        }

        private void btQuitarImagenEmpleado_Click(object sender, RoutedEventArgs e)
        {
            imagenEmpleado.Source = null;
            if (btNuevoEmpleado.IsVisible)
            {
                empleado.imagen = null;
                uof.EmpleadoRepository.Modify(empleado);
            }
        }

        private void btNuevoEmpleado_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            LimpiarEmpleado();

            btCancelarEmpleado.Visibility = Visibility.Visible;
            btNuevoEmpleado.Visibility = Visibility.Collapsed;
            btGuardarEmpleado.IsEnabled = true;
            panelBotonesImagenesEmpleado.IsEnabled = true;
            panelDatosEmpleado.IsEnabled = true;
            panelBusquedaEmpleados.IsEnabled = false;
            panelDatagridsEmpleados.IsEnabled = false;
        }

        private void btCancelarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            LimpiarEmpleado();

            btCancelarEmpleado.Visibility = Visibility.Collapsed;
            btNuevoEmpleado.Visibility = Visibility.Visible;
            btGuardarEmpleado.IsEnabled = false;
        }

        private void btGuardarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            empleado.contraseña = pbContraseñaEmpleado.Password;
            if (Validar(empleado))
            {
                if (empleado.fechaNacimiento < empleado.fechaContratacion)
                {
                    if (uof.EmpleadoRepository.MultiGet(c => c.usuario.Equals(empleado.usuario) && c.EmpleadoId != empleado.EmpleadoId && c.estado.Equals(HABILITADO)).Count() == 0)
                    {
                        if (uof.EmpleadoRepository.MultiGet(c => c.dni.Equals(empleado.dni) && c.EmpleadoId != empleado.EmpleadoId && c.estado.Equals(HABILITADO)).Count() == 0)
                        {
                            empleado.imagen = ConvertImageToArrayByte((BitmapImage)imagenEmpleado.Source);
                            uof.EmpleadoRepository.Add(empleado);

                            dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet();
                            MostrarEmpleado();

                            btCancelarEmpleado.Visibility = Visibility.Collapsed;
                            btNuevoEmpleado.Visibility = Visibility.Visible;
                            btGuardarEmpleado.IsEnabled = false;
                            panelBusquedaEmpleados.IsEnabled = true;
                            panelDatagridsEmpleados.IsEnabled = true;

                            MessageBox.Show("Empleado añadido", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("Ya existe un empleado registrado con este DNI", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Ya existe un empleado registrado con este usuario", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("La fecha de contratación no puede ser igual o posterior a la de nacimiento", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        private void btModificarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            empleado.contraseña = pbContraseñaEmpleado.Password;
            if (Validar(empleado))
            {
                if (empleado.fechaNacimiento < empleado.fechaContratacion)
                {
                    if (uof.EmpleadoRepository.MultiGet(c => c.usuario.Equals(empleado.usuario) && c.EmpleadoId != empleado.EmpleadoId && c.estado.Equals(HABILITADO)).Count() == 0)
                    {
                        if (uof.EmpleadoRepository.MultiGet(c => c.dni.Equals(empleado.dni) && c.EmpleadoId != empleado.EmpleadoId && c.estado.Equals(HABILITADO)).Count() == 0)
                        {
                            empleado.imagen = ConvertImageToArrayByte((BitmapImage)imagenEmpleado.Source);
                            uof.EmpleadoRepository.Modify(empleado);

                            dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet();

                            MessageBox.Show("Empleado modificado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("Ya existe un empleado registrado con este DNI", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Ya existe un empleado registrado con este usuario", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("La fecha de contratación no puede ser igual o posterior a la de nacimiento", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        private void btEliminarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show("¿Estás seguro de que quieres eliminar el empleado?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                empleado.estado = DESHABILITADO;
                uof.EmpleadoRepository.Modify(empleado);

                if (empleado.usuario.ToUpper().Equals(tbUsuarioLogin.Text.ToUpper()))
                {
                    Logout(true);
                }

                dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet();
                LimpiarEmpleado();

                MessageBox.Show("Empleado eliminado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void cbCampoBusquedaEmpleado_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCampoBusquedaEmpleado.SelectedItem.Equals("fecha nacimiento") || cbCampoBusquedaEmpleado.SelectedItem.Equals("fecha contratación"))
            {
                dpBusquedaEmpleado.Visibility = Visibility.Visible;
                tbCampoBusquedaEmpleado.Visibility = Visibility.Collapsed;
            }
            else
            {
                dpBusquedaEmpleado.Visibility = Visibility.Collapsed;
                tbCampoBusquedaEmpleado.Visibility = Visibility.Visible;
            }
        }

        private void tbCampoBusquedaEmpleado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BuscarEmpleado();
            }
        }

        private void dpBusquedaEmpleado_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            BuscarEmpleado();
        }

        private void btBuscarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            BuscarEmpleado();   
        }

        private void btVerTodosEmpleados_Click(object sender, RoutedEventArgs e)
        {
            VerTodosEmpleados();
        }

        private void btVerHistorialLogins_Click(object sender, RoutedEventArgs e)
        {
            VentanaLoginInfo vli;

            ActualizarContexto();

            if (privilegios)
            {
                vli = new VentanaLoginInfo(this, uof, null);
            }
            else
            {
                vli = new VentanaLoginInfo(this, null, uof.EmpleadoRepository.MultiGet(c => c.usuario.Equals(tbUsuarioLogin.Text) && c.estado.Equals(HABILITADO)).FirstOrDefault().logins.ToList());
            }

            vli.ShowDialog();
        }

        private void dgEmpleado_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgEmpleado.SelectedIndex >= 0)
            {
                empleado = (Empleado)dgEmpleado.SelectedItem;
                MostrarEmpleado();
            }
        }

        private void dgEmpleado_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgEmpleado.SelectedIndex >= 0)
            {
                ActualizarContexto();

                VentanaMultiInfo vmi = new VentanaMultiInfo(this, uof, 1, empleado.ventas.ToList(), empleado.compras.ToList(), null, null, null);
                vmi.ShowDialog();
            }
        }

        private void btAñadirTurno_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            VentanaAsignarTurnos vat = new VentanaAsignarTurnos(this, uof, empleado);
            vat.ShowDialog();
        }

        private void btQuitarTurno_Click(object sender, RoutedEventArgs e)
        {
            empleado.turnos.Remove((Turno)dgTurnosEmpleado.SelectedItem);
            uof.EmpleadoRepository.Modify(empleado);

            dgTurnosEmpleado.Items.Refresh();
        }

        private void BuscarEmpleado()
        {
            if (!String.IsNullOrEmpty(tbCampoBusquedaEmpleado.Text) && tbCampoBusquedaEmpleado.IsVisible
                || dpBusquedaEmpleado.SelectedDate != null && dpBusquedaEmpleado.IsVisible)
            {
                ActualizarContexto();

                switch (cbCampoBusquedaEmpleado.Text)
                {
                    case "nombre": dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet(c => c.nombre.Contains(tbCampoBusquedaEmpleado.Text)); break;
                    case "apellidos": dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet(c => c.apellidos.Contains(tbCampoBusquedaEmpleado.Text)); break;
                    case "dni": dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet(c => c.dni.Contains(tbCampoBusquedaEmpleado.Text)); break;
                    case "teléfono": dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet(c => c.telefono.Contains(tbCampoBusquedaEmpleado.Text)); break;
                    case "email": dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet(c => c.email.Contains(tbCampoBusquedaEmpleado.Text)); break;
                    case "dirección": dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet(c => c.direccion.Contains(tbCampoBusquedaEmpleado.Text)); break;
                    case "usuario": dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet(c => c.usuario.Contains(tbCampoBusquedaEmpleado.Text)); break;
                    case "cargo": dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet(c => c.cargo.Contains(tbCampoBusquedaEmpleado.Text)); break;
                    case "fecha nacimiento":
                        DateTime fechaN = Convert.ToDateTime(dpBusquedaEmpleado.SelectedDate);
                        dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet(c => c.fechaNacimiento == fechaN);
                        break;
                    case "fecha contratación":
                        DateTime fechaC = Convert.ToDateTime(dpBusquedaEmpleado.SelectedDate);
                        dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet(c => c.fechaContratacion == fechaC);
                        break;
                    case "día semana": dgEmpleado.ItemsSource = uof.EmpleadoRepository.GetEmpleadosTurnos(true, tbCampoBusquedaEmpleado.Text); break;
                    case "turno": dgEmpleado.ItemsSource = uof.EmpleadoRepository.GetEmpleadosTurnos(false, tbCampoBusquedaEmpleado.Text); break;
                }

                LimpiarEmpleado();
            }
            else
            {
                VerTodosEmpleados();
            }
        }

        private void VerTodosEmpleados()
        {
            ActualizarContexto();

            dgEmpleado.ItemsSource = uof.EmpleadoRepository.MultiGet();
            LimpiarEmpleado();
        }

        private void MostrarEmpleado()
        {
            panelMantenimientoEmpleados.DataContext = empleado;
            pbContraseñaEmpleado.Password = empleado.contraseña;
            imagenEmpleado.Source = ConvertArrayByteToImage(empleado.imagen);
            if (!empleado.nombre.Equals("ADMIN") && empleado.estado.Equals(HABILITADO))
            {
                panelDatosEmpleado.IsEnabled = true;
                panelBotonesImagenesEmpleado.IsEnabled = true;
                btModificarEmpleado.IsEnabled = true;
                btEliminarEmpleado.IsEnabled = true;
                if (privilegios)
                {
                    panelMantenimientoTurnos.IsEnabled = true;
                }
                dgTurnosEmpleado.ItemsSource = empleado.turnos;
            }
            else
            {
                DeshabilitarBotonesEmpleado();
            }
        }

        private void LimpiarEmpleado()
        {
            empleado = new Empleado();

            panelMantenimientoEmpleados.DataContext = empleado;
            pbContraseñaEmpleado.Password = "";
            imagenEmpleado.Source = null;
            DeshabilitarBotonesEmpleado();

            dgEmpleado.SelectedItem = null;
            panelBusquedaEmpleados.IsEnabled = true;
            panelDatagridsEmpleados.IsEnabled = true;
        }

        private void DeshabilitarBotonesEmpleado()
        {
            panelDatosEmpleado.IsEnabled = false;
            panelBotonesImagenesEmpleado.IsEnabled = false;
            btModificarEmpleado.IsEnabled = false;
            btEliminarEmpleado.IsEnabled = false;
            panelMantenimientoTurnos.IsEnabled = false;
            dgTurnosEmpleado.ItemsSource = null;
        }

        #endregion

        #region PANEL CLIENTES

        private void btNuevoCliente_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            LimpiarCliente();

            btCancelarCliente.Visibility = Visibility.Visible;
            btNuevoCliente.Visibility = Visibility.Collapsed;
            btGuardarCliente.IsEnabled = true;
            panelDatosCliente.IsEnabled = true;
            dgCliente.IsEnabled = false;
            panelBusquedaClientes.IsEnabled = false;
        }

        private void btCancelarCliente_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContexto();

            LimpiarCliente();

            btCancelarCliente.Visibility = Visibility.Collapsed;
            btNuevoCliente.Visibility = Visibility.Visible;
            btGuardarCliente.IsEnabled = false;
        }

        private void btGuardarCliente_Click(object sender, RoutedEventArgs e)
        {
            if (Validar(cliente))
            {
                ActualizarContexto();

                if (uof.ClienteRepository.MultiGet(c => c.email.Equals(cliente.email) && c.estado.Equals(HABILITADO)).Count() == 0)
                {
                    Random rd = new Random();
                    string letras = "Aa.Bb_Cc.Dd_Ee.Ff_Gg.Hh_Ii.Jj_Kk.Ll_Mm.Nn_Oo.Pp_Qq.Rr_Ss.Tt_IUu.Vv_Ww.Xx_Yy.Zz";
                    string password = "";

                    for (int i = 0; i < 5; i++)
                    {
                        password += letras.ElementAt(rd.Next(letras.Length)).ToString() + rd.Next(10);
                    }

                    cliente.contraseña = password;
                    uof.ClienteRepository.Add(cliente);

                    SendEmail(cliente.email, String.Format(HTML_CLIENTE_REGISTRADO_CLIENTE, cliente.nombre.ToUpper(), password));
                    SendEmail(cliente.email, String.Format(HTML_CLIENTE_REGISTRADO, cliente.ClienteId));

                    dgCliente.ItemsSource = uof.ClienteRepository.MultiGet();
                    MostrarCliente();

                    btCancelarCliente.Visibility = Visibility.Collapsed;
                    btNuevoCliente.Visibility = Visibility.Visible;
                    btGuardarCliente.IsEnabled = false;
                    dgCliente.IsEnabled = true;
                    panelBusquedaClientes.IsEnabled = true;

                    MessageBox.Show("Cliente añadido", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Ya existe un cliente registrado con este email", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        private void btModificarCliente_Click(object sender, RoutedEventArgs e)
        {
            if (Validar(cliente))
            {
                if (uof.ClienteRepository.MultiGet(c => c.email.Equals(cliente.email) && c.ClienteId != cliente.ClienteId && c.estado.Equals(HABILITADO)).Count() == 0)
                {
                    uof.ClienteRepository.Modify(cliente);

                    dgCliente.ItemsSource = uof.ClienteRepository.MultiGet();

                    MessageBox.Show("Cliente modificado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Ya hay un cliente registrado con este email", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        private void btEliminarCliente_Click(object sender, RoutedEventArgs e)
        {
            var dialogResult = MessageBox.Show("¿Estás seguro de que quieres eliminar el cliente?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                cliente.estado = DESHABILITADO;
                uof.ClienteRepository.Modify(cliente);

                SendEmail(cliente.email, String.Format(HTML_CLIENTE_ELIMINADO_CLIENTE, cliente.nombre.ToUpper()));
                SendEmail(cliente.email, String.Format(HTML_CLIENTE_ELIMINADO, cliente.ClienteId));

                dgCliente.ItemsSource = uof.ClienteRepository.MultiGet();
                LimpiarCliente();

                MessageBox.Show("Cliente eliminado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void cbCampoBusquedaCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCampoBusquedaCliente.SelectedItem.Equals("fecha nacimiento"))
            {
                dpBusquedaCliente.Visibility = Visibility.Visible;
                tbCampoBusquedaCliente.Visibility = Visibility.Collapsed;
            }
            else
            {
                dpBusquedaCliente.Visibility = Visibility.Collapsed;
                tbCampoBusquedaCliente.Visibility = Visibility.Visible;
            }
        }

        private void tbCampoBusquedaCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BuscarCliente();
            }
        }

        private void dpBusquedaCliente_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            BuscarCliente();
        }

        private void btBuscarCliente_Click(object sender, RoutedEventArgs e)
        {
            BuscarCliente();   
        }

        private void btVerTodosClientes_Click(object sender, RoutedEventArgs e)
        {
            VerTodosClientes();
        }

        private void dgCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgCliente.SelectedIndex >= 0)
            {
                cliente = (Cliente)dgCliente.SelectedItem;
                MostrarCliente();
            }
        }

        private void dgCliente_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if ((dgCliente.SelectedIndex >= 0))
            {
                ActualizarContexto();

                VentanaMultiInfo vmi = new VentanaMultiInfo(this, uof, 2, cliente.ventas.ToList(), null, null, null, null);
                vmi.ShowDialog();
            }
        }

        private void BuscarCliente()
        {
            if ((!String.IsNullOrEmpty(tbCampoBusquedaCliente.Text) && tbCampoBusquedaCliente.IsVisible)
                || (dpBusquedaCliente.SelectedDate != null && dpBusquedaCliente.IsVisible))
            {
                ActualizarContexto();

                switch (cbCampoBusquedaCliente.Text)
                {
                    case "nombre": dgCliente.ItemsSource = uof.ClienteRepository.MultiGet(c => c.nombre.Contains(tbCampoBusquedaCliente.Text)); break;
                    case "apellidos": dgCliente.ItemsSource = uof.ClienteRepository.MultiGet(c => c.apellidos.Contains(tbCampoBusquedaCliente.Text)); break;
                    case "fecha nacimiento":
                        DateTime fecha = Convert.ToDateTime(dpBusquedaCliente.SelectedDate);
                        dgCliente.ItemsSource = uof.ClienteRepository.MultiGet(c => c.fechaNacimiento == fecha);
                        break;
                    case "dni": dgCliente.ItemsSource = uof.ClienteRepository.MultiGet(c => c.dni.Contains(tbCampoBusquedaCliente.Text)); break;
                    case "teléfono": dgCliente.ItemsSource = uof.ClienteRepository.MultiGet(c => c.telefono.Contains(tbCampoBusquedaCliente.Text)); break;
                    case "email": dgCliente.ItemsSource = uof.ClienteRepository.MultiGet(c => c.email.Contains(tbCampoBusquedaCliente.Text)); break;
                    case "ciudad": dgCliente.ItemsSource = uof.ClienteRepository.MultiGet(c => c.ciudad.Contains(tbCampoBusquedaCliente.Text)); break;
                    case "código postal": dgCliente.ItemsSource = uof.ClienteRepository.MultiGet(c => c.codPostal.Contains(tbCampoBusquedaCliente.Text)); break;
                    case "dirección": dgCliente.ItemsSource = uof.ClienteRepository.MultiGet(c => c.direccion.Contains(tbCampoBusquedaCliente.Text)); break;
                }

                LimpiarCliente();
            }
            else
            {
                VerTodosClientes();
            }
        }

        private void VerTodosClientes()
        {
            ActualizarContexto();

            dgCliente.ItemsSource = uof.ClienteRepository.MultiGet();
            LimpiarCliente();
        }

        private void MostrarCliente()
        {
            panelMantenimientoClientes.DataContext = cliente;
            if (!cliente.nombre.Equals("CAJA") && cliente.estado.Equals(HABILITADO))
            {
                panelDatosCliente.IsEnabled = true;
                btModificarCliente.IsEnabled = true;
                btEliminarCliente.IsEnabled = true;
            }
            else
            {
                DeshabilitarBotonesCliente();
            }
        }

        private void LimpiarCliente()
        {
            cliente = new Cliente();

            panelMantenimientoClientes.DataContext = cliente;
            DeshabilitarBotonesCliente();

            dgCliente.SelectedItem = null;
            dgCliente.IsEnabled = true;
            panelBusquedaClientes.IsEnabled = true;
        }

        private void DeshabilitarBotonesCliente()
        {
            panelDatosCliente.IsEnabled = false;
            btModificarCliente.IsEnabled = false;
            btEliminarCliente.IsEnabled = false;
        }

        #endregion

    }
}
