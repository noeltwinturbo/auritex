﻿#pragma checksum "..\..\VentanaDetallesArticulo.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "A5DA8491708D1CAAAD5FDBB1698B2215CFE1ED77"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using Auritex;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Auritex {
    
    
    /// <summary>
    /// DetallesArticulo
    /// </summary>
    public partial class DetallesArticulo : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 11 "..\..\VentanaDetallesArticulo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.WrapPanel panelBotonesMantenimientoDetalleArticulo;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\VentanaDetallesArticulo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btGuardarDetalleArticulo;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\VentanaDetallesArticulo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btModificarDetalleArticulo;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\VentanaDetallesArticulo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btCancelarDetalleArticulo;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\VentanaDetallesArticulo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid panelMantenimientoDetalleArticulo;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\VentanaDetallesArticulo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.WrapPanel panelBotonesImagenesDetalleArticulo;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\VentanaDetallesArticulo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btSeleccionarImagenDetalleArticulo;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\VentanaDetallesArticulo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btQuitarImagenDetalleArticulo;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\VentanaDetallesArticulo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imagenDetalleArticulo;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\VentanaDetallesArticulo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbTallaDetalleArticulo;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\VentanaDetallesArticulo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbStockDetalleArticulo;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\VentanaDetallesArticulo.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbEstadoDetalleArticulo;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Auritex;component/ventanadetallesarticulo.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\VentanaDetallesArticulo.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.panelBotonesMantenimientoDetalleArticulo = ((System.Windows.Controls.WrapPanel)(target));
            return;
            case 2:
            this.btGuardarDetalleArticulo = ((System.Windows.Controls.Button)(target));
            
            #line 12 "..\..\VentanaDetallesArticulo.xaml"
            this.btGuardarDetalleArticulo.Click += new System.Windows.RoutedEventHandler(this.btGuardarDetalleArticulo_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btModificarDetalleArticulo = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\VentanaDetallesArticulo.xaml"
            this.btModificarDetalleArticulo.Click += new System.Windows.RoutedEventHandler(this.btModificarDetalleArticulo_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btCancelarDetalleArticulo = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\VentanaDetallesArticulo.xaml"
            this.btCancelarDetalleArticulo.Click += new System.Windows.RoutedEventHandler(this.btCancelarDetalleArticulo_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.panelMantenimientoDetalleArticulo = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.panelBotonesImagenesDetalleArticulo = ((System.Windows.Controls.WrapPanel)(target));
            return;
            case 7:
            this.btSeleccionarImagenDetalleArticulo = ((System.Windows.Controls.Button)(target));
            
            #line 32 "..\..\VentanaDetallesArticulo.xaml"
            this.btSeleccionarImagenDetalleArticulo.Click += new System.Windows.RoutedEventHandler(this.btSeleccionarImagenDetalleArticulo_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btQuitarImagenDetalleArticulo = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\VentanaDetallesArticulo.xaml"
            this.btQuitarImagenDetalleArticulo.Click += new System.Windows.RoutedEventHandler(this.btQuitarImagenDetalleArticulo_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.imagenDetalleArticulo = ((System.Windows.Controls.Image)(target));
            return;
            case 10:
            this.tbTallaDetalleArticulo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.tbStockDetalleArticulo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.tbEstadoDetalleArticulo = ((System.Windows.Controls.TextBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

