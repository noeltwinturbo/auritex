﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auritex.Model
{
    [Table("DetalleCompra")]
    public class DetalleCompra
    {

        public DetalleCompra() { }

        public DetalleCompra(int DetalleCompraId, DetalleArticulo detalleArticulo, Compra compra)
        {
            this.DetalleCompraId = DetalleCompraId;
            unidades = 1;
            precio = detalleArticulo.articulo.precioCompra;
            this.detalleArticulo = detalleArticulo;
            this.compra = compra;
        }

        [Key, Column(Order = 0)]
        public int DetalleCompraId { get; set; }

        [Required(ErrorMessage = "Unidades requeridas")]
        [Range(0, 500, ErrorMessage = "Máximo 499 unidades")]
        public int unidades { get; set; }

        [Required(ErrorMessage = "Precio requerido")]
        [Range(0f, 999f, ErrorMessage = "Máximo 999€ precio")]
        public float precio { get; set; }

        public float subTotalPrecio
        {
            get
            {
                return unidades * precio;
            }
        }

        [Key, Column(Order = 1)]
        public int CompraId { get; set; }
        public string talla { get; set; }
        public int ArticuloId { get; set; }

        public virtual Compra compra { get; set; }
        public virtual DetalleArticulo detalleArticulo { get; set; }

    }
}
