﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auritex.Model
{
    [Table("Configuracion")]
    public class Configuracion
    {

        public Configuracion () { }

        public Configuracion(string nombre, bool estado)
        {
            this.nombre = nombre;
            this.estado = estado;
        }

        [Key]
        [Required(ErrorMessage = "Nombre de la configuración requerido")]
        [DataType(DataType.Text, ErrorMessage = "Nombre no válido")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Mínimo 3 y máximo 100 caracteres para el nombre de la configuración")]
        public string nombre { get; set; }

        [Required(ErrorMessage = "Estado de la confguración requerido")]
        public bool estado { get; set; }

    }
}
