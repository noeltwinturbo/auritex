﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Auritex.Model
{
    [Table("DetalleArticulo")]
    public class DetalleArticulo
    {

        public DetalleArticulo()
        {
            estado = "HABILITADO";
            detalleVentas = new HashSet<DetalleVenta>();
            detalleCompras = new HashSet<DetalleCompra>();
        }

        public DetalleArticulo(int ArticuloId)
        {
            this.ArticuloId = ArticuloId;
            estado = "HABILITADO";
            detalleVentas = new HashSet<DetalleVenta>();
            detalleCompras = new HashSet<DetalleCompra>();
        }

        [Key, Column(Order = 0)]
        [Required(ErrorMessage = "Talla requerida")]
        [DataType(DataType.Text, ErrorMessage = "Talla no válida")]
        [StringLength(10, MinimumLength = 1, ErrorMessage = "Mínimo 1 y máximo 10 caracteres para la talla")]
        public string talla { get; set; }

        [Required(ErrorMessage = "Stock requerido")]
        [Range(0, 1000, ErrorMessage = "Máximo 999 unidades de stock")]
        public int stock { get; set; }

        [Required(ErrorMessage = "Unidades en pedido requerido")]
        [Range(0, 1000, ErrorMessage = "Máximo 999 unidades de unidades en pedido")]
        public int unidadesEnPedido { get; set; }

        [Required(ErrorMessage = "Estado requerido")]
        [RegularExpression("HABILITADO|DESHABILITADO", ErrorMessage = "Estado no válido")]
        public string estado { get; set; }

        [Key, Column(Order = 1)]
        public int ArticuloId { get; set; }

        public virtual Articulo articulo { get; set; }

        public virtual ICollection<DetalleVenta> detalleVentas { get; set; }
        public virtual ICollection<DetalleCompra> detalleCompras { get; set; }

    }
}
