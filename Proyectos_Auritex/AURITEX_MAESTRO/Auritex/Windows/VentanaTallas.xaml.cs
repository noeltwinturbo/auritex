﻿using Auritex.DAL;
using Auritex.Model;
using System;
using System.Windows;
using System.Windows.Input;

namespace Auritex
{
    /// <summary>
    /// Lógica de interacción para DetallesArticulo.xaml
    /// </summary>
    public partial class VentanaDetallesArticulo : Window
    {

        private MainWindow mw;
        private UnitOfWork uof;
        private DetalleArticulo detalleArticulo;
        private Articulo articulo;
        private int index;

        public VentanaDetallesArticulo(MainWindow mw, UnitOfWork uof, int index, Articulo articulo, DetalleArticulo detalleArticulo)
        {
            InitializeComponent();

            Owner = mw;
            this.mw = mw;
            this.uof = uof;
            this.index = index;
            this.detalleArticulo = detalleArticulo;
            this.articulo = articulo;
            
            Iniciar(index);
        }

        private void Iniciar(int index)
        {
            if (index == 0)
            {
                panelMantenimientoDetalleArticulo.DataContext = detalleArticulo;
            }
            else
            {
                panelBotonesMantenimientoDetalleArticulo.Visibility = Visibility.Visible;
                tbStockDetalleArticulo.IsReadOnly = false;
                if (index == 1)
                {
                    detalleArticulo = new DetalleArticulo(articulo.ArticuloId);
                    tbTallaDetalleArticulo.IsReadOnly = false;

                }
                if (index == 2)
                {
                    btGuardarDetalleArticulo.Visibility = Visibility.Collapsed;
                    btModificarDetalleArticulo.Visibility = Visibility.Visible;
                }
                panelMantenimientoDetalleArticulo.DataContext = detalleArticulo;
            }
        }

        private void btGuardarDetalleArticulo_Click(object sender, RoutedEventArgs e)
        {
            GuardarDetalleArticulo();
        }

        private void btModificarDetalleArticulo_Click(object sender, RoutedEventArgs e)
        {
            ModificarDetalleArticulo();
        }

        private void btCancelarDetalleArticulo_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (index == 1)
                {
                    GuardarDetalleArticulo();
                }
            }
            else if (e.Key == Key.Escape)
            {
                Close();
            }
        }

        private void GuardarDetalleArticulo()
        {
            mw.ActualizarContexto();

            if (uof.DetalleArticuloRepository.CheckExistsDetalleArticulo(tbTallaDetalleArticulo.Text, articulo))
            {
                detalleArticulo.talla = tbTallaDetalleArticulo.Text;
                if (mw.Validar(detalleArticulo))
                {
                    articulo.detallesArticulo.Add(detalleArticulo);
                    uof.ArticuloRepository.Modify(articulo);

                    mw.dgArticulo.Items.Refresh();
                    mw.dgDetallesArticulo.Items.Refresh();

                    Iniciar(1);

                    MessageBox.Show("Talla creada", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            else
            {
                MessageBox.Show("Ya existe esta talla", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void ModificarDetalleArticulo()
        {
            if (mw.Validar(detalleArticulo))
            {
                uof.DetalleArticuloRepository.Modify(detalleArticulo);

                mw.dgArticulo.Items.Refresh();
                mw.dgDetallesArticulo.Items.Refresh();

                MessageBox.Show("Talla modificada", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);

                Close();
            }
        }

        private void tb_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int ascci = Convert.ToInt32(Convert.ToChar(e.Text));
            if (ascci >= 48 && ascci <= 57)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

    }
}
