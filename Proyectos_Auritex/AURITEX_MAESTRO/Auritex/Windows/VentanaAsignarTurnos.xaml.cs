﻿using Auritex.DAL;
using Auritex.Model;
using System.Windows;
using System.Windows.Input;

namespace Auritex
{
    /// <summary>
    /// Lógica de interacción para Turnos.xaml
    /// </summary>
    public partial class VentanaAsignarTurnos : Window
    {

        private MainWindow mw;
        private UnitOfWork uof;
        private Empleado empleado;

        public VentanaAsignarTurnos(MainWindow mw, UnitOfWork uof, Empleado empleado)
        {
            InitializeComponent();

            Owner = mw;
            this.mw = mw;
            this.uof = uof;
            this.empleado = empleado;
            dgTurno.ItemsSource = uof.TurnoRepository.MultiGet();
        }

        private void dgTurno_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgTurno.SelectedIndex >= 0)
            {
                Turno t = (Turno)dgTurno.SelectedItem;

                if (!empleado.turnos.Contains(t))
                {
                    empleado.turnos.Add(t);
                    uof.EmpleadoRepository.Modify(empleado);

                    mw.dgTurnosEmpleado.Items.Refresh();
                }
                else
                {
                    MessageBox.Show("Ya tiene este turno asignado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }          
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
        }
    }
}
