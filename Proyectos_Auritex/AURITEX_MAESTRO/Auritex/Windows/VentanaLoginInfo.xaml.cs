﻿using Auritex.DAL;
using Auritex.Model;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Auritex.Windows
{
    /// <summary>
    /// Lógica de interacción para VentanaLoginInfo.xaml
    /// </summary>
    public partial class VentanaLoginInfo : Window
    {

        private UnitOfWork uof;
        private MainWindow mw;
        private List<Login> logins;
        private bool enable = false;

        public VentanaLoginInfo(MainWindow mw, UnitOfWork uof, List<Login> logins)
        {
            InitializeComponent();

            Owner = mw;
            this.mw = mw;
            this.uof = uof;
            if (logins != null)
            {
                dgLoginInfo.ItemsSource = logins;
                this.logins = logins;
            }
            else
            {
                dgLoginInfo.ItemsSource = uof.LoginRepository.MultiGet();
                enable = true;
            }
        }

        private void calendarioLogins_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime fecha = Convert.ToDateTime(calendarioLogins.SelectedDate);

            mw.ActualizarContexto();

            if (enable)
            {
                dgLoginInfo.ItemsSource = uof.LoginRepository.MultiGet(c => c.fechaEntrada.Day == fecha.Day && c.fechaEntrada.Month == fecha.Month && c.fechaEntrada.Year == fecha.Year);
            }
            else
            {
                List<Login> listaLogins = new List<Login>();

                foreach (Login l in logins)
                {
                    if (l.fechaEntrada.Day == fecha.Day && l.fechaEntrada.Month == fecha.Month && l.fechaEntrada.Year == fecha.Year)
                    {
                        listaLogins.Add(l);
                    }
                }
                dgLoginInfo.ItemsSource = listaLogins;
            }
        }

        private void btVerTodosLogins_Click(object sender, RoutedEventArgs e)
        {
            if (enable)
            {
                mw.ActualizarContexto();

                dgLoginInfo.ItemsSource = uof.LoginRepository.MultiGet();
            }
            else
            {
                dgLoginInfo.ItemsSource = logins;
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
        }
    }
}
