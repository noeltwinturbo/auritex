﻿using Auritex.DAL;
using Auritex.Model;
using Auritex.Windows.InfoWindows;
using System;
using System.Windows;
using System.Windows.Input;

namespace Auritex.Windows
{
    /// <summary>
    /// Lógica de interacción para VentanaModificarEstadoCompra.xaml
    /// </summary>
    public partial class VentanaModificarEstadoCompra : Window
    {

        private MainWindow mw;
        private VentanaMultiInfo vmi;
        private UnitOfWork uof;
        private Compra compra;
        private bool check = false;

        public VentanaModificarEstadoCompra(MainWindow mw, VentanaMultiInfo vmi, UnitOfWork uof, Compra compra)
        {
            InitializeComponent();

            if (mw != null)
            {
                Owner = mw;
                this.mw = mw;
            }
            else
            {
                Owner = vmi;
                this.mw = vmi.mw;
                this.vmi = vmi;
            }

            this.uof = uof;
            this.compra = compra;
            panelEstadoCompra.DataContext = compra;
            this.compra.estado = "FINALIZADA";
        }

        private void btModificarEstadoCompra_Click(object sender, RoutedEventArgs e)
        {
            ModificarEstadoCompra();
        }

        private void ModificarEstadoCompra()
        {
            var dialogResult = MessageBox.Show("¿Estás seguro de que quieres modificar el estado de la compra?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                check = true;

                mw.ActualizarContexto();

                compra.estado = cbEstadoCompra.Text;

                foreach (DetalleCompra dc in compra.detallesCompra)
                {
                    dc.detalleArticulo.unidadesEnPedido -= dc.unidades;
                    if (compra.estado.Equals("FINALIZADA"))
                    {
                        dc.detalleArticulo.stock += dc.unidades;
                    }
                }

                uof.CompraRepository.Modify(compra);

                if (vmi != null)
                {
                    vmi.dgHistorialCompras.Items.Refresh();
                }
                mw.dgHistorialCompras.Items.Refresh();

                mw.SendEmail(MainWindow.EMAIL_AURITEX, String.Format((compra.estado.Equals("FINALIZADA") ? MainWindow.HTML_COMPRA_REALIZADA : MainWindow.HTML_COMPRA_CANCELADA), compra.CompraId, compra.totalUnidades, compra.totalPrecio, compra.empleado.nombre + " " + compra.empleado.apellidos, compra.EmpleadoId));

                MessageBox.Show((compra.estado.Equals("FINALIZADA") ? "Compra finalizada y artículos recibidos" : "Compra cancelada"), "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);

                Close();
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
            else if (e.Key == Key.Enter)
            {
                ModificarEstadoCompra();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!compra.estado.Equals("PENDIENTE") && !check)
            {
                cbEstadoCompra.Text = "PENDIENTE";
            }
        }
    }
}
