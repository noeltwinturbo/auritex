﻿using Auritex.DAL;
using Auritex.Model;
using Auritex.Windows.ShoppingWindows;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Auritex.Windows.InfoWindows
{
    /// <summary>
    /// Lógica de interacción para VentanaMultiInfo.xaml
    /// </summary>
    public partial class VentanaMultiInfo : Window
    {

        public MainWindow mw { get; }
        private UnitOfWork uof;
        private List<Articulo> listaArticulos;
        private List<Venta> listaVentas;
        private List<Compra> listaCompras;

        public VentanaMultiInfo(MainWindow mw, UnitOfWork uof, int index, List<Venta> listaVentas, List<Compra> listaCompras, List<Articulo> listaArticulos, Proveedor proveedor, Categoria categoria)
        {
            InitializeComponent();

            Owner = mw;
            this.mw = mw;
            this.uof = uof;

            BlackOutDays();
            CargarElementos();

            switch (index)
            {
                case 1:
                    this.listaVentas = listaVentas;
                    this.listaCompras = listaCompras;
                    dgHistorialVentas.ItemsSource = this.listaVentas;
                    dgHistorialCompras.ItemsSource = this.listaCompras;

                    CalcularResumenVentas();
                    CalcularResumenCompras();

                    tabVentas.Visibility = Visibility.Visible;
                    tabCompras.Visibility = Visibility.Visible;
                    tabVentas.IsSelected = true;
                    break;
                case 2:
                    this.listaVentas = listaVentas;
                    dgHistorialVentas.ItemsSource = this.listaVentas;

                    CalcularResumenVentas();

                    tabVentas.Visibility = Visibility.Visible;
                    tabVentas.IsSelected = true;
                    break;
                case 3:
                    this.listaArticulos = listaArticulos;
                    this.listaCompras = listaCompras;
                    dgArticulo.ItemsSource = this.listaArticulos;
                    dgHistorialCompras.ItemsSource = this.listaCompras;

                    CalcularResumenCompras();

                    tabArticulos.Visibility = Visibility.Visible;
                    tabCompras.Visibility = Visibility.Visible;
                    tabCompras.IsSelected = true;
                    break;
                case 4:
                    this.listaArticulos = listaArticulos;
                    dgArticulo.ItemsSource = this.listaArticulos;

                    tabArticulos.Visibility = Visibility.Visible;
                    tabArticulos.IsSelected = true;
                    break;
                case 5:
                    panelDatosProveedor.DataContext = proveedor;
                    imagenProveedor.Source = mw.ConvertArrayByteToImage(proveedor.imagen);
                    panelDatosCategorias.DataContext = categoria;
                    imagenCategoria.Source = mw.ConvertArrayByteToImage(categoria.imagen);

                    tabProveedores.Visibility = Visibility.Visible;
                    tabCategorias.Visibility = Visibility.Visible;
                    tabCategorias.IsSelected = true;
                    break;
            }
        }

        #region METODOS INICIO

        private void CargarElementos()
        {
            cbCampoBusquedaArticulo.Text = "nombre";
            cbCampoBusquedaHistorialVentas.Text = "empleado";
            cbCampoBusquedaHistorialCompras.Text = "empleado";
            cbMetodoBusquedaArticulo.Text = ">";
            cbMetodoBusquedaHistorialVentas.Text = ">";
        }

        private void BlackOutDays()
        {
            for (DateTime i = calendarioVentas.DisplayDateStart.Value; i < calendarioVentas.DisplayDateEnd.Value; i = i.AddDays(7))
            {
                calendarioVentas.BlackoutDates.Add(new CalendarDateRange(i));
                calendarioCompras.BlackoutDates.Add(new CalendarDateRange(i));
            }
        }

        #endregion

        #region PANEL ARTICULOS

        private void cbCampoBusquedaArticulo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCampoBusquedaArticulo.SelectedItem.Equals("precio compra") 
                || cbCampoBusquedaArticulo.SelectedItem.Equals("precio venta")
                || cbCampoBusquedaArticulo.SelectedItem.Equals("nivel nuevo pedido"))
            {
                cbMetodoBusquedaArticulo.Visibility = Visibility.Visible;
            }
            else
            {
                cbMetodoBusquedaArticulo.Visibility = Visibility.Collapsed;
            }
        }

        private void tbCampoBusquedaArticulo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BuscarArticulos();
            }
        }

        private void btBuscarArticulo_Click(object sender, RoutedEventArgs e)
        {
            BuscarArticulos();
        }

        private void btVerTodosArticulos_Click(object sender, RoutedEventArgs e)
        {
            VerTodosArticulos();
        }

        private void dgArticulo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgArticulo.SelectedIndex >= 0)
            {
                Articulo articulo = (Articulo)dgArticulo.SelectedItem;

                panelDatosArticulo.DataContext = articulo;
                imagenArticulo.Source = mw.ConvertArrayByteToImage(articulo.imagen);
                dgDetallesArticulo.ItemsSource = ((Articulo)dgArticulo.SelectedItem).detallesArticulo;
            }
        }

        private void BuscarArticulos()
        {
            if (!String.IsNullOrEmpty(tbCampoBusquedaArticulo.Text))
            {
                panelDatosArticulo.DataContext = null;
                imagenArticulo.Source = null;
                dgArticulo.SelectedItem = null;

                List<Articulo> listaArt = new List<Articulo>();

                if (!cbMetodoBusquedaArticulo.IsVisible)
                {
                    switch (cbCampoBusquedaArticulo.Text)
                    {
                        case "nombre":
                            foreach (Articulo a in listaArticulos)
                            {
                                if (a.nombre.Contains(tbCampoBusquedaArticulo.Text))
                                {
                                    listaArt.Add(a);
                                }
                            }
                            break;
                        case "sexo":
                            foreach (Articulo a in listaArticulos)
                            {
                                if (a.sexo.Contains(tbCampoBusquedaArticulo.Text))
                                {
                                    listaArt.Add(a);
                                }
                            }
                            break;
                        case "proveedor":
                            foreach (Articulo a in listaArticulos)
                            {
                                if (a.proveedor.nombre.Contains(tbCampoBusquedaArticulo.Text))
                                {
                                    listaArt.Add(a);
                                }
                            }
                            break;
                        case "categoría":
                            foreach (Articulo a in listaArticulos)
                            {
                                if (a.categoria.nombre.Contains(tbCampoBusquedaArticulo.Text))
                                {
                                    listaArt.Add(a);
                                }
                            }
                            break;
                        case "talla":
                            foreach (Articulo a in listaArticulos)
                            {
                                foreach (DetalleArticulo da in a.detallesArticulo)
                                {
                                    if (da.talla.Equals(tbCampoBusquedaArticulo.Text))
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                            }
                            break;
                        case "estado":
                            foreach (Articulo a in listaArticulos)
                            {
                                if (a.estado.Contains(tbCampoBusquedaArticulo.Text))
                                {
                                    listaArt.Add(a);
                                }
                            }
                            break;
                    }
                }
                else
                {
                    float var = 0;
                    try { var = Convert.ToSingle(tbCampoBusquedaArticulo.Text, CultureInfo.InvariantCulture.NumberFormat); }
                    catch (Exception)
                    {
                        Debug.WriteLine(String.Format("Error al convertir \"{0}\" en un número", tbCampoBusquedaArticulo.Text));
                        MessageBox.Show("Solo valen números", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }

                    if (cbCampoBusquedaArticulo.Text.Equals("precio compra"))
                    {
                        switch (cbMetodoBusquedaArticulo.Text)
                        {
                            case ">":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.precioCompra > var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case "<":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.precioCompra < var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case ">=":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.precioCompra >= var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case "<=":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.precioCompra <= var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case "==":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.precioCompra == var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case "!=":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.precioCompra != var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                        }
                    }
                    else if (cbMetodoBusquedaArticulo.Text.Equals("precio venta"))
                    {
                        switch (cbMetodoBusquedaArticulo.Text)
                        {
                            case ">":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.precioVenta > var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case "<":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.precioVenta < var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case ">=":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.precioVenta >= var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case "<=":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.precioVenta <= var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case "==":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.precioVenta == var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case "!=":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.precioVenta != var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                        }
                    }
                    else
                    {
                        switch (cbMetodoBusquedaArticulo.Text)
                        {
                            case ">":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.nivelNuevoPedido > var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case "<":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.nivelNuevoPedido < var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case ">=":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.nivelNuevoPedido >= var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case "<=":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.nivelNuevoPedido <= var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case "==":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.nivelNuevoPedido == var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                            case "!=":
                                foreach (Articulo a in listaArticulos)
                                {
                                    if (a.nivelNuevoPedido != var)
                                    {
                                        listaArt.Add(a);
                                    }
                                }
                                break;
                        }
                    }
                }

                dgArticulo.ItemsSource = listaArt;
            }
            else
            {
                VerTodosArticulos();
            }
        }

        private void VerTodosArticulos()
        {
            dgArticulo.ItemsSource = listaArticulos;
            panelDatosArticulo.DataContext = null;
            imagenArticulo.Source = null;
            dgArticulo.SelectedItem = null;
        }

        #endregion

        #region PANEL HISTORIAL VENTAS

        private void calendarioVentas_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime fecha = Convert.ToDateTime(calendarioVentas.SelectedDate);
            dgHistorialVentas.ItemsSource = null;

            foreach (Venta v in listaVentas)
            {
                if (v.fecha.Day == fecha.Day && v.fecha.Month == fecha.Month && v.fecha.Year == fecha.Year)
                {
                    dgHistorialVentas.Items.Add(v);
                }
            }

            dgHistorialDetallesVenta.ItemsSource = null;

            CalcularResumenVentas();
        }

        private void cbCampoBusquedaHistorialVentas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbCampoBusquedaHistorialVentas.SelectedItem.Equals("descuento"))
            {
                cbMetodoBusquedaHistorialVentas.Visibility = Visibility.Visible;
            }
            else
            {
                cbMetodoBusquedaHistorialVentas.Visibility = Visibility.Collapsed;
            }
        }

        private void tbCampoBusquedaHistorialVentas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BuscarHistorialVentas();
            }
        }

        private void btBuscarVenta_Click(object sender, RoutedEventArgs e)
        {
            BuscarHistorialVentas();   
        }

        private void btVerTodasVentas_Click(object sender, RoutedEventArgs e)
        {
            VerTodasVentas();
        }

        private void dgHistorialVentas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgHistorialVentas.SelectedIndex >= 0)
            {
                dgHistorialDetallesVenta.ItemsSource = ((Venta)dgHistorialVentas.SelectedItem).detallesVenta;
            }
        }

        private void dgHistorialVentas_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgHistorialVentas.SelectedIndex >= 0)
            {
                VentanaTicket vt = new VentanaTicket(null, this, (Venta)dgHistorialVentas.SelectedItem, null);
                vt.ShowDialog();
            }
        }

        private void dgHistorialDetallesVenta_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgHistorialDetallesVenta.SelectedIndex >= 0)
            {
                VentanaDetalleArticuloInfo vdai = new VentanaDetalleArticuloInfo(((DetalleVenta)dgHistorialDetallesVenta.SelectedItem).detalleArticulo, null, this);
                vdai.ShowDialog();
            }
        }

        private void btDevolver_Click(object sender, RoutedEventArgs e)
        {
            DetalleVenta dv = (DetalleVenta)dgHistorialDetallesVenta.SelectedItem;

            var dialogResult = MessageBox.Show("¿Estás seguro de que quieres devolver el detalle de artículo?", "Auritex - Aviso", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (dialogResult == MessageBoxResult.Yes)
            {
                mw.ActualizarContexto();

                if (uof.DetalleVentaRepository.MultiGet(c => c.DetalleVentaId == dv.DetalleVentaId && c.VentaId == dv.VentaId && c.estado.Equals("VENDIDO")).Count() == 1)
                {
                    if ((DateTime.Now - dv.venta.fecha).TotalDays <= 30)
                    {
                        dv.detalleArticulo.stock++;
                        dv.estado = "DEVUELTO";
                        uof.DetalleVentaRepository.Modify(dv);

                        mw.SendEmail(MainWindow.EMAIL_AURITEX, String.Format(MainWindow.HTML_ARTICULO_DEVUELTO, dv.detalleArticulo.articulo.nombre, dv.detalleArticulo.talla, dv.detalleArticulo.articulo.sexo, dv.detalleArticulo.articulo.precioVenta));

                        dgHistorialDetallesVenta.Items.Refresh();
                        mw.dgHistorialDetallesVenta.Items.Refresh();

                        MessageBox.Show("Detalle de artículo devuelto", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("No se puede devolver el detalle de artículo porque pasaron más de 30 días desde que fue comprado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Este detalle de artículo ya ha sido devuelto", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        private void BuscarHistorialVentas()
        {
            if (!String.IsNullOrEmpty(tbCampoBusquedaHistorialVentas.Text))
            {
                dgHistorialVentas.SelectedItem = null;
                dgHistorialDetallesVenta.ItemsSource = null;
                List<Venta> listaVen = new List<Venta>();

                if (!cbMetodoBusquedaHistorialVentas.IsVisible)
                {
                    switch (cbCampoBusquedaHistorialVentas.Text)
                    {
                        case "empleado":
                            foreach (Venta v in listaVentas)
                            {
                                if (v.empleado.nombre.Contains(tbCampoBusquedaHistorialVentas.Text))
                                {
                                    listaVen.Add(v);
                                }
                            }
                            break;
                        case "cliente":
                            foreach (Venta v in listaVentas)
                            {
                                if (v.cliente.nombre.Contains(tbCampoBusquedaHistorialVentas.Text))
                                {
                                    listaVen.Add(v);
                                }
                            }
                            break;
                        case "ciudad":
                            foreach (Venta v in listaVentas)
                            {
                                if (v.ciudadEnvio.Contains(tbCampoBusquedaHistorialVentas.Text))
                                {
                                    listaVen.Add(v);
                                }
                            }
                            break;
                        case "código postal":
                            foreach (Venta v in listaVentas)
                            {
                                if (v.codPostalEnvio.Contains(tbCampoBusquedaHistorialVentas.Text))
                                {
                                    listaVen.Add(v);
                                }
                            }
                            break;
                        case "dirección":
                            foreach (Venta v in listaVentas)
                            {
                                if (v.direccionEnvio.Contains(tbCampoBusquedaHistorialVentas.Text))
                                {
                                    listaVen.Add(v);
                                }
                            }
                            break;
                        case "forma pago":
                            foreach (Venta v in listaVentas)
                            {
                                if (v.formaPago.Contains(tbCampoBusquedaHistorialVentas.Text))
                                {
                                    listaVen.Add(v);
                                }
                            }
                            break;
                        case "estado":
                            dgHistorialVentas.ItemsSource = null;
                            foreach (Venta v in listaVentas)
                            {
                                foreach (DetalleVenta dv in v.detallesVenta)
                                {
                                    if (dv.estado.Contains(tbCampoBusquedaHistorialVentas.Text))
                                    {
                                        listaVen.Add(v);
                                    }
                                }
                            }
                            break;
                    }
                }
                else
                {
                    float descuentoHistorialVenta = 0;
                    try { descuentoHistorialVenta = Convert.ToSingle(tbCampoBusquedaHistorialVentas.Text, CultureInfo.InvariantCulture.NumberFormat); }
                    catch (Exception)
                    {
                        Debug.WriteLine(String.Format("Error al convertir \"{0}\" en un número", tbCampoBusquedaHistorialVentas.Text));
                        MessageBox.Show("Solo valen números", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }

                    switch (cbMetodoBusquedaHistorialVentas.Text)
                    {
                        case ">":
                            foreach (Venta v in listaVentas)
                            {
                                if (v.descuento > descuentoHistorialVenta)
                                {
                                    listaVen.Add(v);
                                }
                            }
                            break;
                        case "<":
                            foreach (Venta v in listaVentas)
                            {
                                if (v.descuento < descuentoHistorialVenta)
                                {
                                    listaVen.Add(v);
                                }
                            }
                            break;
                        case ">=":
                            foreach (Venta v in listaVentas)
                            {
                                if (v.descuento >= descuentoHistorialVenta)
                                {
                                    listaVen.Add(v);
                                }
                            }
                            break;
                        case "<=":
                            foreach (Venta v in listaVentas)
                            {
                                if (v.descuento <= descuentoHistorialVenta)
                                {
                                    listaVen.Add(v);
                                }
                            }
                            break;
                        case "==":
                            foreach (Venta v in listaVentas)
                            {
                                if (v.descuento == descuentoHistorialVenta)
                                {
                                    listaVen.Add(v);
                                }
                            }
                            break;
                        case "!=":
                            foreach (Venta v in listaVentas)
                            {
                                if (v.descuento != descuentoHistorialVenta)
                                {
                                    listaVen.Add(v);
                                }
                            }
                            break;
                    }
                }

                dgHistorialVentas.ItemsSource = listaVen;

                CalcularResumenVentas();
            }
            else
            {
                VerTodasVentas();
            }
        }

        private void VerTodasVentas()
        {
            dgHistorialVentas.ItemsSource = listaVentas;
            dgHistorialVentas.SelectedItem = null;
            dgHistorialDetallesVenta.ItemsSource = null;

            CalcularResumenVentas();
        }

        private void CalcularResumenVentas()
        {
            int cantidadVentas = dgHistorialVentas.Items.Count;
            int chico = 0, chica = 0, unidades = 0;
            float total = 0;
            lbCantidadVentas.Content = cantidadVentas;

            dgHistorialVentas.SelectedItem = null;

            foreach (Venta v in dgHistorialVentas.Items)
            {
                foreach (DetalleVenta dv in v.detallesVenta)
                {
                    if (dv.estado.Equals("VENDIDO"))
                    {
                        if (dv.detalleArticulo.articulo.sexo.Equals("CHICO"))
                        {
                            chico++;
                        }
                        else
                        {
                            chica++;
                        }
                        unidades++;
                    }
                }
                total += v.totalPrecio;
            }

            lbSexoVentas.Content = (chico > chica) ? "chico, " + chico + " uds" : "chica, " + chica + " uds";
            lbGastoMedioVentas.Content = total / cantidadVentas;
            lbTotalUnidadesVentas.Content = unidades;
            lbTotalPrecioVentas.Content = total;
        }

        #endregion

        #region PANEL HISTORIAL COMPRAS

        private void calendarioCompras_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            DateTime fecha = Convert.ToDateTime(calendarioCompras.SelectedDate);
            dgHistorialCompras.ItemsSource = null;

            foreach (Compra c in listaCompras)
            {
                if (c.fecha.Day == fecha.Day && c.fecha.Month == fecha.Month && c.fecha.Year == fecha.Year)
                {
                    dgHistorialCompras.Items.Add(c);
                }
            }

            dgHistorialDetallesCompra.ItemsSource = null;

            CalcularResumenCompras();
        }

        private void tbCampoBusquedaHistorialCompras_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BuscarHistorialCompras();
            }
        }

        private void btBuscarCompra_Click(object sender, RoutedEventArgs e)
        {
            BuscarHistorialCompras();
        }

        private void btVerTodasCompras_Click(object sender, RoutedEventArgs e)
        {
            VerTodasCompras();
        }

        private void dgHistorialCompras_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgHistorialCompras.SelectedIndex >= 0)
            {
                dgHistorialDetallesCompra.ItemsSource = ((Compra)dgHistorialCompras.SelectedItem).detallesCompra;
            }
        }

        private void dgHistorialCompras_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgHistorialCompras.SelectedIndex >= 0)
            {
                VentanaTicket vt = new VentanaTicket(null, this, null, (Compra)dgHistorialCompras.SelectedItem);
                vt.ShowDialog();
            }
        }

        private void btModificarEstadoCompra_Click(object sender, RoutedEventArgs e)
        {
            Compra c = (Compra)dgHistorialCompras.SelectedItem;

            mw.ActualizarContexto();

            if (c.estado.Equals("PENDIENTE"))
            {
                VentanaModificarEstadoCompra vmec = new VentanaModificarEstadoCompra(null, this, uof, c);
                vmec.ShowDialog();
            }
            else
            {
                MessageBox.Show("Este pedido ya ha sido finalizado o cancelado", "Auritex - Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void dgHistorialDetallesCompra_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (dgHistorialDetallesCompra.SelectedIndex >= 0)
            {
                VentanaDetalleArticuloInfo vdai = new VentanaDetalleArticuloInfo(((DetalleCompra)dgHistorialDetallesCompra.SelectedItem).detalleArticulo, null, this);
                vdai.ShowDialog();
            }
        }

        private void BuscarHistorialCompras()
        {
            if (!String.IsNullOrEmpty(tbCampoBusquedaHistorialCompras.Text))
            {
                List<Compra> listaCom = new List<Compra>();
                dgHistorialCompras.SelectedItem = null;
                dgHistorialDetallesCompra.ItemsSource = null;

                switch (cbCampoBusquedaHistorialCompras.Text)
                {
                    case "empleado":
                        foreach (Compra c in listaCompras)
                        {
                            if (c.empleado.nombre.Contains(tbCampoBusquedaHistorialCompras.Text))
                            {
                                listaCom.Add(c);
                            }
                        }
                        break;
                    case "proveedor":
                        foreach (Compra c in listaCompras)
                        {
                            if (c.proveedor.nombre.Contains(tbCampoBusquedaHistorialCompras.Text))
                            {
                                listaCom.Add(c);
                            }
                        }
                        break;
                    case "estado":
                        foreach (Compra c in listaCompras)
                        {
                            if (c.estado.Contains(tbCampoBusquedaHistorialCompras.Text))
                            {
                                listaCom.Add(c);
                            }
                        }
                        break;
                }

                dgHistorialCompras.ItemsSource = listaCom;
            }
            else
            {
                VerTodasCompras();
            }
        }

        private void VerTodasCompras()
        {
            dgHistorialCompras.ItemsSource = listaCompras;
            dgHistorialCompras.SelectedItem = null;
            dgHistorialDetallesCompra.ItemsSource = null;

            CalcularResumenCompras();
        }

        private void CalcularResumenCompras()
        {
            int chico = 0, chica = 0, unidades = 0, cantidadCompras = 0;
            float total = 0;
           
            dgHistorialCompras.SelectedItem = null;

            foreach (Compra c in dgHistorialCompras.Items)
            {
                if (c.estado.Equals("FINALIZADA"))
                {
                    foreach (DetalleCompra dc in c.detallesCompra)
                    {
                        if (dc.detalleArticulo.articulo.sexo.Equals("CHICO"))
                        {
                            chico++;
                        }
                        else
                        {
                            chica++;
                        }
                        unidades++;
                    }
                    cantidadCompras++;
                    total += c.totalPrecio;
                }
            }

            lbCantidadCompras.Content = cantidadCompras;
            lbSexoCompras.Content = (chico > chica) ? "chico, " + chico + " uds" : "chica, " + chica + " uds";
            lbGastoMedioCompras.Content = total / cantidadCompras;
            lbTotalUnidadesCompras.Content = unidades;
            lbTotalPrecioCompras.Content = total;
        }

        #endregion

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
        }

    }
}
