﻿using Auritex.Model;
using Auritex.Windows.InfoWindows;
using System.Windows;
using System.Windows.Input;

namespace Auritex.Windows
{
    /// <summary>
    /// Lógica de interacción para VentanaDetalleArticuloInfo.xaml
    /// </summary>
    public partial class VentanaDetalleArticuloInfo : Window
    {
        public VentanaDetalleArticuloInfo(DetalleArticulo da, MainWindow mw, VentanaMultiInfo vmi)
        {
            InitializeComponent();

            if (mw != null)
            {
                Owner = mw;
                imagenArticulo.Source = mw.ConvertArrayByteToImage(da.articulo.imagen);
            }
            else
            {
                Owner = vmi;
                imagenArticulo.Source = vmi.mw.ConvertArrayByteToImage(da.articulo.imagen);
            }

            panelDatosArticulo.DataContext = da;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
        }
    }
}
